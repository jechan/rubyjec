require 'FileUtils'
require 'C:/Shipserv Backup/RUBY WORKSHOP/STD/STD scripts/mymethods_v02.rb'

@@v_int_type='LAB'
@@v_ext='.XML'
@@v_dir_template='C:/Shipserv Backup/RUBY WORKSHOP/STD/STD MTML TEMPLATE/'
Dir.chdir(@@v_dir_template)
@@v_pref='JC'
@@v_doc_type=1

def get_doc_seq(doc_type)
	@v_seq_file=@@v_int_type+'_seq.txt'
	cur_dt=Time.new
	@v_dt_seq=cur_dt.strftime('%y%m%d')
	@w_seq=File.read(@v_seq_file).split(',')
	v_seq=@w_seq[doc_type.to_i]
	if @w_seq[0] == @v_dt_seq then
		@w_seq[doc_type.to_i]=@w_seq[doc_type.to_i].to_i+1
	else
		#w_seq=[v_dt_seq,'0','0','0','0','0']
		@w_seq[0]=@v_dt_seq
		for i in 1..5
			if doc_type.to_i==i then
				@w_seq[i]=1 
			else
				@w_seq[i]=0
			end
		end
	end
	return @w_seq
end


def gen_uc_ref_no(drefseq)
	cur_dt=Time.new
	v_dt_seq=cur_dt.strftime('%y%m%d')
	@seq=@@v_pref+get_doc_type(@@v_doc_type.to_i)+v_dt_seq+add_zero_prefix(3,drefseq[@@v_doc_type.to_i])
end

def write_seq_refno(w_seq)
	v_seq_file=@@v_int_type+'_seq.txt'
	cur_dt=Time.new
	v_dt_seq=cur_dt.strftime('%y%m%d')
	File.open(v_seq_file,'w') do|fp|
		#fp.print(v_dt_seq+','+w_seq[1].to_s+','+w_seq[2].to_s+','+w_seq[3].to_s+','+w_seq[4].to_s+','+w_seq[5].to_s+','+w_seq[6].to_s+','+w_seq[7].to_s)
		fp.print(w_seq.join(','))
	end
end



doc_ref_seq=get_doc_seq(@@v_doc_type)
@@v_unique_ref=gen_uc_ref_no(doc_ref_seq)
if @@v_doc_type.to_i == 1 or @@v_doc_type.to_i==2 then
	doc_ref_seq[@@v_doc_type.to_i+5] = @@v_unique_ref
end
write_file= write_seq_refno(doc_ref_seq)
