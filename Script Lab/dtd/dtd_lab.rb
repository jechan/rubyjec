require 'nokogiri'

test=%Q{<?xml version="1.0"?>
<note>
<to>Tove</to>
<from>Jani</from>
<heading>Reminder</heading>
<body>Don't forget me this weekend</body>
</note>}

xml=Nokogiri::XML(test)
#puts xml.class
node=xml.at_xpath('//note')
#puts node.class
dtd=xml.create_internal_subset('note',nil,'note.dtd')
puts dtd.each {|e| puts e}
