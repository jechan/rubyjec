require 'nokogiri'

builder = Nokogiri::XML::Builder.new do |xml|
  xml.doc.create_internal_subset('test',nil,"test.dtd")
  xml.root do
    #xml.foo(:test=>'attr', :anorhet=>'content')
    xml.foo('text content', :what=>'try', :test=>'attr', :anorhet=>'content')
    xml.foo('text content2', :what=>'try2', :test=>'attr2', :anorhet=>'content2')
    xml.sroo('sibling')
  end
end
 
xml=Nokogiri::XML(builder.to_xml)
foo = xml.at_xpath("//foo")
puts foo
puts foo.next_element
foo.remove_attribute('test')
puts foo
puts"================"
nodeset=xml.xpath("//foo")
#puts nodeset.length
puts nodeset
puts "+++++++++++++++"
puts nodeset.shift
puts nodeset
puts xml
