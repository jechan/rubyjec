require 'FileUtils'
require 'C:/Shipserv Backup/RUBY WORKSHOP/STD/STD scripts/mymethods_v03.rb'

#Global Variable Declaration
@@v_int_type='LAB'
@@v_ext='.XML'
@@v_dir_template='C:/Shipserv Backup/RUBY WORKSHOP/STD/LAB/'
Dir.chdir(@@v_dir_template)
@@v_pref='JC'

#Replaced Variables in documents later to be XML
@@v_buyer='10414'
@@v_supplier='59758'

@@v_doc_type=4

def get_attr_val(attr)
	attr_pattern = /#{attr}="(\S+)"/
	return @@v_sample.scan(attr_pattern).first
end

@@v_sample=%Q{<MTML>
	<Interchange ControlReference="RequestForQuote:4361767" Identifier="UNOC" PreparationDate="2012-Apr-30" PreparationTime="18:27" Recipient="59758" RecipientCodeQualifier="ZEX" Sender="10414" SenderCodeQualifier="ZEX" VersionNumber="2">
		<RequestForQuote AssociationAssignedCode="MARL10" ControllingAgency="UN" CurrencyCode="USD" DeliveryTermsCode="EXW" FunctionCode="_9" LineItemCount="1" MessageNumber="90001489166" MessageReferenceNumber="RequestForQuote:4361767" Priority="High" ReleaseNumber="96A" TaxStatus="Exempt" TransportModeCode="_2" VersionNumber="D">
			<DateTimePeriod FormatQualifier="_203" Qualifier="_132" Value="200803240000"/>
			<DateTimePeriod FormatQualifier="_203" Qualifier="_133" Value="200803280000"/>}

puts get_attr_val("ControlReference")
puts get_attr_val("MessageNumber")