require 'rubygems'
require 'savon'

client=Savon::Client.new('http://dev.shipserv.com/SSMTML/MTMLLink.asmx?wsdl')
puts client.wsdl.namespace
puts client.wsdl.endpoint
puts client.wsdl.soap_actions

file_contents=File.read('C:\MTML\10414\OUT\NS51RFQ120717004.XML')

response = client.request :send_document, :xmlns=>"urn:shipserv.mtml" do
	soap.body=%Q{
	<strTradeNetID>10414</strTradeNetID>
      <strUserID>b_10414_1</strUserID>
      <strPassword>shipserv</strPassword>
      <strIntegration>NS5</strIntegration>
      <strFileContents>#{file_contents}</strFileContents>
      <byteFileContentsAsBytes>base64Binary</byteFileContentsAsBytes>
      <strClientFileName>test.src</strClientFileName>
      <strDocumentType>string</strDocumentType>

	}
	end