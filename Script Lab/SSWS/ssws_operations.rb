require 'rubygems'
require 'savon'
require 'base64'
require 'nokogiri'
require 'htmlentities'

module Tradenet
  class SoapConnection
    def setup(host)
      @@s_client = Savon::Client.new do
        wsdl.endpoint = "#{host}/MTMLLink.asmx"
        wsdl.namespace = "urn:shipserv.mtml"
      end
    end

    def acknowledgedocument(params={})
      @@s_client.request :AcknowledgeDocument do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/AcknowledgeDocument'
        soap.input = ["AcknowledgeDocument", {xmlns: "urn:shipserv.mtml"}]
        soap.body = {strTradeNetID: params[:tnid],
                     strUserID: params[:username],
                     strPassword: params[:password],
                     strIntegration: params[:integrations],
                     strDocType: params[:doctype],
                     strAckID: params[:ackid].to_s}
      end
    end

    def checkencodingtype(params={})
      @@s_client.request :CheckEncodingType do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/CheckEncodingType'
        soap.input = ["CheckEncodingType", {xmlns: "urn:shipserv.mtml"}]
        soap.body = {EncodingTypeCode: params[:encodingtypecode]}
      end
    end

    def getcountremaining(params={})
      @@s_client.request :GetCountRemaining do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/GetCountRemaining'
        soap.input = ["GetCountRemaining", {xmlns: "urn:shipserv.mtml"}]
        soap.body = {strTradeNetID: params[:tnid],
                     strUserID: params[:username],
                     strPassword: params[:password],
                     strIntegration: params[:integrations]}
      end
    end

    def getdocumenttypes
      @@s_client.request :GetDocumentTypes do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/GetDocumentTypes'
      end
    end

    def getencodingtypecodes
      @@s_client.request :GetEncodingTypeCodes do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/GetEncodingTypeCodes'
      end
    end

    def getexchangerates(params={})
      @@s_client.request :GetExchangeRates do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/GetExchangeRates'
        soap.input = ["GetExchangeRates", {xmlns: "urn:shipserv.mtml"}]
        soap.body = {dUpdatedOnOrAfter: params[:datetime]}
      end
    end

    def getintegrationcodes
      @@s_client.request :GetIntegrationCodes do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/GetIntegrationCodes'
      end
    end

    def getintegrationsettings(params={})
      @@s_client.request :GetIntegrationSettings do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/GetIntegrationSettings'
        soap.input = ["GetIntegrationSettings", {xmlns: "urn:shipserv.mtml"}]
        soap.body = {strIntegration: params[:integration]}
      end
    end

    def ping
      @@s_client.request :Ping do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/Ping'
      end
    end

    def searchsuppliersbyname(params={})
      @@s_client.request :SearchSuppliersByName do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/SearchSuppliersByName'
        soap.input = ["SearchSuppliersByName", {xmlns: "urn:shipserv.mtml"}]
        soap.body = {sName: params[:sName], sCallersTNID: params[:sCallersTNID]}
      end
    end

    def getcustomerdoc(params={})
      @@s_client.request :GetCustomerDoc do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/GetCustomerDoc'
        soap.input = ["GetCustomerDoc", {xmlns: "urn:shipserv.mtml"}]
        soap.body = {strTradeNetID: params[:tnid],
                     strUserID: params[:username],
                     strPassword: params[:password],
                     strIntegration: params[:integrations]}
      end
    end

    def getcustomerdocbytes(params={})
      @@s_client.request :GetCustomerDocBytes do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/GetCustomerDocBytes'
        soap.input = ["GetCustomerDocBytes", {xmlns: "urn:shipserv.mtml"}]
        soap.body = {strTradeNetID: params[:tnid],
                     strUserID: params[:username],
                     strPassword: params[:password],
                     strIntegration: params[:integrations]}
      end
    end

    def sendcustomerdocument(params={})
      @@s_client.request :SendCustomerDoc do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/SendCustomerDoc'
        soap.input = ["SendCustomerDoc", {xmlns: "urn:shipserv.mtml"}]
        soap.body = {strTradeNetID: params[:tnid],
                     strUserID: params[:username],
                     strPassword: params[:password],
                     strIntegration: params[:integrations],
                     strFileContents: params[:filecontents],
                     byteFileContents: Base64.encode64(params[:filecontents])}
      end
    end

    def getdocument(params={})
      @@s_client.request :GetDocument do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/GetDocument'
        soap.input = ["GetDocument", {xmlns: "urn:shipserv.mtml"}]
        soap.body = {strTradeNetID: params[:tnid],
                     strUserID: params[:username],
                     strPassword: params[:password],
                     strIntegration: params[:integrations]}
      end
    end

    def senddocument(params={})
      @@s_client.request :SendDocument do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/SendDocument'
        soap.input = ["SendDocument", {xmlns: "urn:shipserv.mtml"}]
        soap.body = {strTradeNetID: params[:tnid],
                     strUserID: params[:username],
                     strPassword: params[:password],
                     strIntegration: params[:integrations],
                     #strFileContents: params[:filecontents],
                     byteFileContentsAsBytes: Base64.encode64(params[:filecontents]),
                     strClientFileName: params[:clientfname],
                     strDocumentType: params[:doctype]}
      end
    end

    def getencodeddocument(params={})
      @@s_client.request :GetEncodedDocument do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/GetEncodedDocument'
        soap.input = ["GetEncodedDocument", {xmlns: "urn:shipserv.mtml"}]
        soap.body = {"UserIntegrationDoc" => {"AppDetails" => {"Name" => params[:name],
                                                              "Version" => params[:version]},
                                             "IntegrationDetails" => {"TradeNetID" => params[:tnid],
                                                                      "UserID" => params[:username],
                                                                      "UserPassword" => params[:password],
                                                                      "IntegrationCode" => params[:integrations]},
                                             "DocumentDetails" => {"EncodingTypeCode" => params[:encodingtypecode]}}
        }
      end
    end

    def sendencodeddocument(params={})
      @@s_client.request :SendEncodedDocument do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/SendEncodedDocument'
        soap.input = ["SendEncodedDocument", {xmlns: "urn:shipserv.mtml"}]
        soap.body = {"UserIntegrationDoc" => {"AppDetails" => {"Name" => params[:name] || "Fitnesse",
                                                               "Version" => params[:version] || "1.0.0"},
                                              "IntegrationDetails" => {"TradeNetID" => params[:tnid],
                                                                       "UserID" => params[:username],
                                                                       "UserPassword" => params[:password],
                                                                       "IntegrationCode" => params[:integrations]},
                                              "DocumentDetails" => {"DocumentType" => params[:doctype],
                                                                    "ClientFileName" => params[:clientfname] || "Shipserv Auto",
                                                                    "FileContentsAsBytes" => Base64.encode64(params[:filecontents]).gsub("\n", ''),
                                                                    "EncodingTypeCode" => params[:encodingtypecode] || "UTF-8"}}
                                              #"DocumentAttachments" => {"DocumentAttachment" => {"FileName" => params[:filename],
                                              #                                                   "FileContentsAsBytes" => file_content_as_bytes(filename: params[:filename])}}}
        }
      end
    end

    def getmtmldoc(params={})
      @@s_client.request :GetMTMLDoc do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/GetMTMLDoc'
        soap.input = ["GetMTMLDoc", {xmlns: "urn:shipserv.mtml"}]
        soap.body = {strTradeNetID: params[:tnid],
                     strUserID: params[:username],
                     strPassword: params[:password],
                     strIntegration: params[:integrations]}
      end
    end

    def sendmtmldoc(params={})
      @@s_client.request :SendMTMLDoc do
        @@s_client.http.headers["SOAPAction"]= 'urn:shipserv.mtml/SendMTMLDoc'
        soap.input = ["SendMTMLDoc", {xmlns: "urn:shipserv.mtml"}]
        soap.body = {strTradeNetID: params[:tnid],
                     strUserID: params[:username],
                     strPassword: params[:password],
                     strMTML: params[:mtml_server],
                     strIntegration: params[:integrations]}
      end
    end

    def file_content_as_bytes(params={})
      path = File.join(File.dirname(__FILE__), "../integrations/data/attachment", "#{params[:path]}", params[:filename])
      puts "File to open :: #{path}"
      File.open(path, 'r') do |image_file|
        Base64.encode64(image_file.read)
      end
    end
  end
end