require 'rubygems'
require 'oci8'

module Utils
  class Database
    attr_accessor :env, :browser

    def execute_statement(env, sql_statement)
      if env == 'LIVE'
        tnsnames = '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1531)) (CONNECT_DATA = (SID = SSPROD04)))'
        conn = OCI8.new('READONLY', 'iship', tnsnames)
      elsif env == 'UAT' || env == 'TN2TEST'
        tnsnames = '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1530)) (CONNECT_DATA = (SID = SSTEST05)))'
        conn = OCI8.new('SSERVDBA', 'robin', tnsnames)
      else #dev
        tnsnames = '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = zeus.myshipserv.com)(PORT = 1521)) (CONNECT_DATA = (SID = SSDEV06)))'
        conn = OCI8.new('SSERVDBA', 'devmla', tnsnames)
      end

      puts "SQL STATEMENT :: #{sql_statement}"
      if sql_statement.downcase.include? 'select'
        temp = conn.exec(sql_statement) do |r|
          @source = r.to_a
        end
        puts temp.to_s + ' rows were processed.'
      elsif sql_statement.downcase.include? 'update'
        temp = conn.exec(sql_statement)
        puts temp.to_s + ' rows were updated.'
        conn.commit
        puts 'Commit Successful.'
        @source = "Update Success"
      elsif sql_statement.downcase.include? 'insert'
        temp = conn.exec(sql_statement)
        puts temp.to_s + ' rows inserted.'
        conn.commit
        puts 'Commit Successful.'
        @source = "Insert Success"
      end
      conn.logoff
      puts "SQL :: #{@source}\n"
      return @source
    end
  end
end