require 'rubygems'
require 'win32ole'

@@fx_property=%Q{MTMLLink FX Client, Version 5.2.2.35027}


def wait_edit_button
	au3 = WIN32OLE.new('AutoItX3.Control') #will remove this once naayos ko ung class
	begin
		edit_btn=au3.ControlCommand("MTMLLink FX Client, Version 5.2.2.35027", "","[NAME:btnEdit]","IsEnabled", "")
		au3.sleep(1000)
	end until edit_btn=="1"
end

def def_wait_time
	au3 = WIN32OLE.new('AutoItX3.Control') #will remove this once naayos ko ung class
	begin
		server_url=au3.ControlGetText("MTMLLink FX Client, Version 5.2.2.35027", "","[NAME:txtServer]")
		exch_btn=au3.ControlCommand("MTMLLink FX Client, Version 5.2.2.35027", "","[NAME:btnForce]","IsEnabled", "")
		au3.sleep(1000)
	end until server_url=="http://test.shipserv.com/SSMTML"  and exch_btn=="1"
end

def win_exist
	au3 = WIN32OLE.new('AutoItX3.Control') #will remove this once naayos ko ung class
	var=au3.WinExists("[TITLE:MTMLLink FX Client, Version 5.2.2.35027; CLASS:WindowsForms10.Window.8.app.0.378734a]","")
	return var.to_s
end

au3 = WIN32OLE.new('AutoItX3.Control')
au3.Run("C:/Program Files/ShipServ/MTMLlink-FX/Client/MTMLlinkFXClient.exe") if win_exist == '0'
#au3.WinWaitActive("[TITLE:MTMLLink FX Client, Version 5.2.2.35027; CLASS:WindowsForms10.Window.8.app.0.378734a]","")
au3.sleep(5000)
au3.ControlFocus("#@@fx_property","", "[NAME:cbServer]")
au3.sleep(500)
au3.ControlSend("MTMLLink FX Client, Version 5.2.2.35027", "", "[NAME:cbServer]", "SHIP-IT-JCHAN{DOWN}")
wait_edit_button
au3.ControlFocus("MTMLLink FX Client, Version 5.2.2.35027", "", "[NAME:cbInstance]")
au3.sleep(500)
au3.ControlSend("MTMLLink FX Client, Version 5.2.2.35027", "", "[NAME:cbInstance]", "10414{DOWN}")
def_wait_time
au3.ControlClick("MTMLLink FX Client, Version 5.2.2.35027", "","[Name:btnForce; TEXT:Exchange Now!;]")
au3.sleep(1000)
#au3.WinClose("[TITLE:MTMLLink FX Client, Version 5.2.2.35027; CLASS:WindowsForms10.Window.8.app.0.378734a]","")