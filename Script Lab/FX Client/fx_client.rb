#require 'mtmllink/helpers'
require 'fit/column_fixture'
require 'ssws/transaction_docs'
require 'helpers/default'
require 'helpers/rally_update'
require 'win32ole'
require 'net/http'
require 'uri'

module Mtmllink
  class FxClient < Fit::ColumnFixture
    BUYER_ID = '10141'
    SUPPLIER_ID = '70455'

    attr_accessor :server, :instance
    attr_accessor :doctype, :integration
    #@@timestamp = Time.now.strftime("%m%d%y_%H%M%S")
    attr_accessor :env, :browser, :username, :password, :test_case
    include Rally

    def initialize
      $app_title = "MTMLLink FX Client, Version 5.2.2.35027"
      @log_results = RallyUpdate.new
    end

    def load_fx_client
      @au3 = WIN32OLE.new "AutoItX3.Control"
      $pid = @au3.Run('C:\Program Files\ShipServ\MTMLlink-FX\Client\MTMLLinkFXClient.exe', "", @SW_MAXIMIZE)
      @au3.WinActivate($app_title)
      app_handle = @au3.WinGetHandle($app_title, "")
      @au3.WinActivate(app_handle)
      @au3.Sleep(3000)
      ret_val = @au3.ProcessExists($pid)
      result = ret_val != 0 ? 'pass' : 'fail'
    end

    def load_server
      @au3.ControlFocus($app_title, "", "[Name:btnRescan; TEXT:Rescan;]")
      @au3.ControlClick($app_title, "", "[Name:btnRescan; TEXT:Rescan;]")
      @au3.Sleep(3000)
      @au3.ControlSend($app_title, "", "[NAME:cbServer]", "{DOWN}")
      @au3.ControlCommand($app_title, "", "[NAME:cbServer]", "ShowDropDown", "")
      @au3.ControlCommand($app_title, "", "[NAME:cbServer]", "HideDropDown", "")
      @au3.Sleep(2000)
      @au3.ControlSend($app_title, "", "[NAME:cbServer]", "#{@server}{DOWN}")
      @au3.Sleep(2000)

      @au3.ControlCommand($app_title, "", "[NAME:cbInstance]", "ShowDropDown", "")
      @au3.ControlCommand($app_title, "", "[NAME:cbInstance]", "HideDropDown", "")
      @au3.Sleep(2000)
      @au3.ControlSend($app_title, "", "[NAME:cbInstance]", "#{@instance}{DOWN}")
      @au3.Sleep(5000)

      server = @au3.ControlGetText($app_title, "", "[NAME:txtServer]")
      @inbox_url = @au3.ControlGetText($app_title, "", "[NAME:txtInbox]")
      @outbox_url = @au3.ControlGetText($app_title, "", "[NAME:txtOutBox]")

      result = server != "" ? 'ok' : 'fail'
    end

    def check_logs
      @au3.ControlCommand($app_title, "", "[NAME:TabControl1]", "TabRight", "") # Purchasing Plugin
      @au3.Sleep(1000)

      @au3.ControlCommand($app_title, "", "[NAME:TabControl1]", "TabRight", "") # Logistics Plugin
      @au3.Sleep(1000)

      @au3.ControlCommand($app_title, "", "[NAME:TabControl1]", "TabRight", "") # Log
      @au3.Sleep(3000)

      @au3.ControlClick($app_title, "", "[Name:btnRefresh]") #Refresh Logs
      @au3.Sleep(3000)
      @au3.ControlCommand($app_title, "", "[NAME:ListBox1]", "GetCurrentSelection", "")
      @au3.Sleep(1000)
      log_entry = @au3.ControlGetText($app_title, "", "[NAME:txtItm]")
      puts "LOG ENTRY :: #{log_entry}"
      return log_entry
    end

    #[NAME:btnRefresh]
    #[NAME:cmdClear]

    def edit_account_info
      @au3.ControlSetText($app_title, "", "[NAME:txtServer]", 'http://dev.shipserv.com/SSMTML')
      @au3.ControlSetText($app_title, "", "[NAME:txtUID]", 'b_king')
      @au3.ControlSetText($app_title, "", "[NAME:txtPwd]", 'SHIPSERV')
    end

    def edit_folders
      @au3.ControlSetText($app_title, "", "[NAME:txtInbox]", 'C:\10141\MTML\IN')
      @au3.ControlSetText($app_title, "", "[NAME:txtInCopy]", 'C:\10141\MTML\IN\COPY')
      @au3.ControlSetText($app_title, "", "[NAME:txtOutBox]", 'C:\10141\MTML\OUT')
      @au3.ControlSetText($app_title, "", "[NAME:txtOutboxCopy]", 'C:\10141\MTML\OUT\COPY')
      @au3.ControlSetText($app_title, "", "[NAME:txtErr]", 'C:\10141\MTML\ERR')
      @au3.ControlSetText($app_title, "", "[NAME:txtTemp]", 'C:\10141\MTML\TEMP')
      @au3.ControlSetText($app_title, "", "[NAME:txtICR]", 'C:\10141\MTML\OUT\R')
    end

    def enable_edit
      @au3.ControlClick($app_title, "", "[NAME:btnEdit;]")
      @au3.WinWaitActive("Enter Access Code", "")
      @au3.ControlSend("Enter Access Code", "", "[NAME:txtPwd]", "SHIPSERV")
      @au3.ControlClick("Enter Access Code", "", "[NAME:btnOk;]")
      @au3.WinWaitActive($app_title, "")
    end

    def edit_integration_setting #[NAME:cbInt] STD
      @au3.ControlCommand($app_title, "", "[NAME:cbInt]", "ShowDropDown", "")
      @au3.ControlCommand($app_title, "", "[NAME:cbInt]", "HideDropDown", "")
      @au3.Sleep(2000)
      @au3.ControlSend($app_title, "", "[NAME:cbInt]", "STD{DOWN}")
      @au3.Sleep(2000)
    end

    def edit_interval_setting
      @au3.ControlSetText($app_title, "", "[NAME:txtInterval]", '300')
    end

    def edit_mode_setting #[NAME:cbMode] STD
      @au3.ControlCommand($app_title, "", "[NAME:cbMode]", "ShowDropDown", "")
      @au3.ControlCommand($app_title, "", "[NAME:cbMode]", "HideDropDown", "")
      @au3.Sleep(2000)
      @au3.ControlSend($app_title, "", "[NAME:cbMode]", "Send/Receive{DOWN}")
      @au3.Sleep(2000)
    end

    def exchange_now
      @au3.ControlClick($app_title, "", "[Name:btnForce; TEXT:Exchange Now!;]")
      @au3.Sleep(15000)
      return 'ok'
    end

    def ping_server
      @au3.ControlClick($app_title, "", "[Name:btnPing; TEXT:Ping Server;]")
      @au3.Sleep(2000)
      @au3.WinWaitActive("MTMLLinkFXClient", "")
      output = @au3.ControlGetText("MTMLLinkFXClient", "", "[CLASS:Static; INSTANCE:1]")
      @au3.ControlClick("MTMLLinkFXClient", "", "[CLASS:Button; INSTANCE:1]")
      @au3.Sleep(2000)
      $app_handle = @au3.WinGetHandle($app_title, "")
      @au3.WinActivate($app_handle, "")
      return output
    end

    def destroy
      ret_val = @au3.ProcessClose($pid)
      result = ret_val == 1 ? 'pass' : 'fail'
      return result
    end

    def create_temp_file
      filecontents = Transaction.create_document(
          doctype: @doctype,
          integration: @integration,
          ##ref_ctrlref: control_ref,
          ##ref_msgnum: @rfq_messagenumber,
          supplier: SUPPLIER_ID,
          buyer: BUYER_ID
      )
      #create_qot_file(rfq_refnum: @rfq_controlrefnum, msg_num: doctype.downcase)
      #create_po_file(qot_refnum: @qot_controlrefnum, msg_num: @qot_messagenumber)
      #create_por_file(po_refnum: @po_controlrefnum, msg_num: @po_messagenumber)
      #create_poc_file(po_refnum: @po_controlrefnum, msg_num: @po_messagenumber)
      doc_fname = "#{@doctype.upcase}#{Time.now.strftime("%m%d%y%H%M%L")}.xml"
      write_file(filename: doc_fname, path: "../mtmllink/temp", content: filecontents)

      #puts "Created #{@doc_fname} document" if @doc_fname != "Error creating document"
      return doc_fname
    end

    def move_file_to_folders
      doc_file = newest_file(File.join(File.dirname(__FILE__), 'temp'))
      move_file(filename: doc_file, path: '../mtmllink/temp', destination: @outbox_url)
      return 'ok'
    end

    def check_in_folder
      doc_file = newest_file(@inbox_url)
    end

    def set_integrations

    end

    def get_latest_rfq
      doc_file = newest_file(@inbox_url)
      path = File.join(@inbox_url, doc_file)
      rfq_doc_file = File.open(path, 'r') { |handle| handle.read }
      @rfq_messagenumber = rfq_doc_file.scan(/MessageNumber="\d*"/).to_s.gsub(/\D/, "")
      puts "RFQ Message Number : " + @rfq_messagenumber
      @rfq_controlrefnum = rfq_doc_file.scan(/RequestForQuote:\d*/).first.to_s.gsub(/\D/, "")
      puts "RFQ Control Ref Number : " + @rfq_controlrefnum

      return doc_file
    end

    def get_latest_qot
      doc_file = newest_file(@inbox_url)
      path = File.join(@inbox_url, doc_file)
      qot_doc_file = File.open(path, 'r') { |handle| handle.read }
      @qot_messagenumber = qot_doc_file.body.scan(/MessageNumber="\d*"/).to_s.gsub(/\D/, "")
      puts "QOT Message Number : " + @qot_messagenumber
      @qot_controlrefnum = qot_doc_file.body.scan(/Quote:\d*/)[0].to_s.gsub(/\D/, "")
      puts "QOT Control Ref Number : " + @qot_controlrefnum

      return doc_file
    end

    def get_latest_po
      doc_file = newest_file(@inbox_url)
      path = File.join(@inbox_url, doc_file)
      po_doc_file = File.open(path, 'r') { |handle| handle.read }
      @po_messagenumber = po_doc_file.scan(/MessageNumber="\d*"/).to_s.gsub(/\D/, "")
      puts "PO Message Number : " + @po_messagenumber
      @po_controlrefnum = po_doc_file.scan(/Order:\d*/)[0].to_s.gsub(/\D/, "")
      puts "PO Control Ref Number : " + @po_controlrefnum

      return doc_file
    end

    def get_latest_inv
      doc_file = newest_file(@inbox_url)
      path = File.join(@inbox_url, doc_file)
      inv_doc_file = File.open(path, 'r') { |handle| handle.read }
      @inv_messagenumber = inv_doc_file.body.scan(/MessageNumber="\d*"/).to_s.gsub(/\D/, "")
      puts "INV Message Number : " + @inv_messagenumber
      @inv_controlrefnum = inv_doc_file.body.scan(/Invoice:\d*/)[0].to_s.gsub(/\D/, "")
      puts "INV Control Ref Number : " + @inv_controlrefnum

      return doc_file
    end
  end
end