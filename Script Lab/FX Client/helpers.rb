require 'fit/column_fixture'
require 'helpers/rally_update'
require 'helpers/default'
require 'win32ole'
require 'net/http'
require 'uri'


class Helpers < Fit::ColumnFixture
  BUYER_ID = '10141'
  SUPPLIER_ID = '70455'
  @timestamp = Time.now.strftime("%m%d%y_%H%M%S")
  attr_accessor :env, :browser, :username, :password, :test_case
  include Rally

  $app_title = "MTMLLink FX Client, Version 5.2.2.35027"

  def create_rfq_file(params ={})
    control_ref = "RFQ" + @timestamp # XXX090611_1234
    data = Transaction.create_document(
        doctype: 'RFQ',
        integration: params[:integration],
        ref_ctrlref: control_ref,
        ref_msgnum: params[:msg_num] || control_ref,
        supplier: params[:supplier_id] || SUPPLIER_ID,
        buyer: params[:buyer_id] || BUYER_ID
    )
    write_file(filename: "#{control_ref}.xml", path: "../mtmllink/temp", content: data)
    return "#{control_ref}.xml"
  end

  def create_qot_file(params ={})
    control_ref = "QOT" + @timestamp # XXX090611_1234
    data = Transaction.create_document(
        doctype: 'QOT',
        integration: params[:integration],
        ref_ctrlref: control_ref,
        ref_msgnum: params[:msg_num] || control_ref,
        supplier: params[:supplier_id] || SUPPLIER_ID,
        buyer: params[:buyer_id] || BUYER_ID,
        rfqctrlref: params[:rfq_refnum]
    )
    write_file(filename: "#{control_ref}.xml", path: "../mtmllink/temp", content: data)
    return "#{control_ref}.xml"
  end

  def create_po_file(params ={})
    control_ref = "PO" + @timestamp # XXX090611_1234
    data = open_file(path: params[:integration] || 'STD', filename: 'po.xml')
    data = data.gsub("[REF_CtrlRef]", params[:ctrl_ref] || control_ref)
    data = data.gsub("[REF_SenderID]", params[:buyer_id] || BUYER_ID)
    data = data.gsub("[REF_RecptID]", params[:supplier_id] || SUPPLIER_ID)
    data = data.gsub("[REF_MsgRefNum]", params[:msg_ref_num] || control_ref)
    data = data.gsub("[REF_MsgNum]", params[:msg_num] || control_ref)
    data = data.gsub("[QOTCtrlRef]", params[:qot_refnum])
    write_file(filename: "#{control_ref}.xml", path: "../mtmllink/temp", content: data)
    return "#{control_ref}.xml"
  end

  def create_po_direct_file(params ={})
    control_ref = "POD" + @timestamp # XXX090611_1234
    data = open_file(path: params[:integration] || 'STD', filename: 'po_direct.xml')
    data = data.gsub("[REF_CtrlRef]", params[:ctrl_ref] || control_ref)
    data = data.gsub("[REF_SenderID]", params[:buyer_id] || BUYER_ID)
    data = data.gsub("[REF_RecptID]", params[:supplier_id] || SUPPLIER_ID)
    data = data.gsub("[REF_MsgRefNum]", params[:msg_ref_num] || control_ref)
    data = data.gsub("[REF_MsgNum]", params[:msg_num] || control_ref)
    write_file(filename: "#{control_ref}.xml", path: "../mtmllink/temp", content: data)
    return "#{control_ref}.xml"
  end

  def create_por_file(params ={})
    control_ref = "POR" + @timestamp # XXX090611_1234
    data = open_file(path: params[:integration] || 'STD', filename: 'por.xml')
    data = data.gsub("[REF_CtrlRef]", params[:ctrl_ref] || control_ref)
    data = data.gsub("[REF_RecptID]", params[:buyer_id] || BUYER_ID)
    data = data.gsub("[REF_SenderID]", params[:supplier_id] || SUPPLIER_ID)
    data = data.gsub("[REF_MsgRefNum]", params[:msg_ref_num] || control_ref)
    data = data.gsub("[REF_MsgNum]", params[:msg_num] || control_ref)
    data = data.gsub("[POCtrlRef]", params[:po_refnum])
    write_file(filename: "#{control_ref}.xml", path: "../mtmllink/temp", content: data)
    return "#{control_ref}.xml"
  end

  def create_poc_file(params ={})
    control_ref = "POC" + @timestamp # XXX090611_1234
    data = open_file(path: params[:integration] || 'STD', filename: 'poc.xml')
    data = data.gsub("[REF_CtrlRef]", params[:ctrl_ref] || control_ref)
    data = data.gsub("[REF_RecptID]", params[:buyer_id] || BUYER_ID)
    data = data.gsub("[REF_SenderID]", params[:supplier_id] || SUPPLIER_ID)
    data = data.gsub("[REF_MsgRefNum]", params[:msg_ref_num] || control_ref)
    data = data.gsub("[REF_MsgNum]", params[:msg_num] || control_ref)
    data = data.gsub("[POCtrlRef]", params[:po_refnum])
    write_file(filename: "#{control_ref}.xml", path: "../mtmllink/temp", content: data)
    return "#{control_ref}.xml"
  end

  def create_inv_file(params ={})
    control_ref = "INV" + @timestamp # XXX090611_1234
    data = open_file(path: params[:integration] || 'STD', filename: 'inv.xml')
    data = data.gsub("[REF_CtrlRef]", params[:ctrl_ref] || control_ref)
    data = data.gsub("[REF_RecptID]", params[:buyer_id].to_s || BUYER_ID)
    data = data.gsub("[REF_SenderID]", params[:supplier_id].to_s || SUPPLIER_ID)
    data = data.gsub("[REF_MsgRefNum]", params[:msg_ref_num] || control_ref)
    data = data.gsub("[REF_MsgNum]", params[:msg_num] || control_ref)
    write_file(filename: "#{control_ref}.xml", path: "../mtmllink/temp", content: data)
    return "#{control_ref}.xml"
  end
end