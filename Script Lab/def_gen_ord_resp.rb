require 'FileUtils'
require 'C:/Shipserv Backup/RUBY WORKSHOP/STD/STD scripts/mymethods_v03.rb'

#Global Variable Declaration
@@v_int_type='LAB'
@@v_ext='.XML'
@@v_dir_template='C:/Shipserv Backup/RUBY WORKSHOP/STD/LAB/'
Dir.chdir(@@v_dir_template)
@@v_pref='JC'

#Replaced Variables in documents later to be XML
@@v_buyer='10414'
@@v_supplier='59758'

@@v_doc_type=4

class Intfile
	def gen_ord_resp
		w_content=File.read(@@v_int_type+'_'+'POR'+@@v_ext)
		w_content.gsub!("v_buyer", @@v_buyer)
		w_content.gsub!("v_supplier", @@v_supplier)
		w_content.gsub!("v_unique_ref", @@v_unique_ref)
		w_content.gsub!("v_ord_dat_for_poac", @@v_ord_dat_for_poac)
		
		v_file_name=@@v_int_type+'_'+get_doc_type(6)+'_'+@@v_unique_ref+@@v_ext
		File.open(v_file_name, 'w') do |fp|
			fp.puts(@@w_content)
		end
		
		FileUtils.mv(v_file_name, get_out_folder(@@v_doc_type))
	end
end