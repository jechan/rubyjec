

class RFBasicNumber
	def specify_a_number(num1)
		@n1=num1.to_i
	end
	
	def specify_another_number(num2)
		@n2=num2.to_i
	end
	
	def add_the_two
		@n1+@n2
	end
	
	def multiply_the_two
		@n1*@n2
	end

end

if __FILE__ == $0
  require 'rubygems'
  require "robot_remote_server"
  RobotRemoteServer.new(RFBasicNumber.new, *ARGV)
end
