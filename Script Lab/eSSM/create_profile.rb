require 'essm/helpers'

module Essm
  class CreateProfile < Helpers
    attr_accessor :suppliertype, :doctype, :status, :suppliername, :vessel, :reference_no
    attr_accessor :email1, :email2, :timezone, :tz

    def goto_create_supplier_profile
      login_user_via_netadmin(@username, @password)
      @engine.link(:href => /tradenetprofile\/createNewSupplierProfile\.do/).send_keys :enter
      @engine.wait

      until @engine.title == "Supplier Branch Edit" do
        sleep 1
      end
      return "ok"
    end

    def goto_create_buyer_profile
      login_user_via_netadmin(@username, @password)
      @engine.link(:href => /tradenetprofile\/createNewBuyerProfile\.do/).send_keys :enter
      @engine.wait

      until @engine.title == "Buyer Branch Edit" do
        sleep 1
      end
      return "ok"
    end

    def create_buyer_profile
      fill_in_buyer_fields(
          :is_org => "new",
          :org_name => "King Ship Management (Demo Only)",
          :branch_name => "King Ship Management_#{Time.now.strftime("%H%M")} (For Demos Only)",
          :account_status => "Active",
          :test_account => "yes",
          :account_in_use => "yes",
          :internet_availability => "yes"
      )
      @engine.button(:name, 'save').send_keys :enter
      sleep 5

      return "ok"
    end

    def create_supplier_profile
      fill_in_supplier_fields(
          :is_org => "new",
          :org_name => "King Ship Management (Demo Only)",
          :test_account => "yes",
          :branch_name => "King Kong Supplier_#{Time.now.strftime("%H%M")} (For Demos Only)"
      )
      @engine.button(:name, 'save').send_keys :enter
      sleep 5

      return "ok"
    end

    def get_buyer_tnid
      buyer_tnid = @engine.link(:href, /shipservadmin\/tradenetprofile\/editProfile\.do\?customerType=buyer/).text
    end

    def get_supplier_tnid
      supplier_tnid = @engine.link(:href, /shipservadmin\/tradenetprofile\/editProfile\.do\?customerType=supplier/).text
    end

    def fill_in_buyer_fields(params = {})
      @engine.radio(:name => /wlw-radio_button_group_key:{actionForm.is_new_org}/, :value => "new").send_keys :space if params[:is_org] == 'new'
      @engine.radio(:name => /wlw-radio_button_group_key:{actionForm.is_new_org}/, :value => "exist").send_keys :space if params[:is_org] == 'exist'
      @engine.text_field(:name => /{actionForm.byb_byo_org_name}/).set(params[:org_name] || 'Hollesen Shipping (FOR DEMOS ONLY)')
      #wlw-radio_button_group_key:{actionForm.byo_is_trans_history_search}
      @engine.text_field(:name => /{actionForm.byb_name}/).set(params[:branch_name] || 'King Ship Management (For Demos Only)')
      #wlw-select_key:{actionForm.byb_contract_type}
      @engine.text_field(:name => /{actionForm.byb_address_1}/).set(params[:branch_address] || 'ShipServ Demonstration Account')
      @engine.text_field(:name => /{actionForm.byb_city}/).set(params[:branch_city] || '-')
      @engine.text_field(:name => /{actionForm.byb_zip_code}/).set(params[:branch_zip] || '-')
      @engine.select_list(:name, "wlw-select_key:{actionForm.byb_country}").select_value("PH")
      @engine.text_field(:name => /{actionForm.byb_email_address}/).set(params[:branch_email] || 'aking@shipserv.com')
      @engine.text_field(:name => /{actionForm.byb_phone_no_1}/).set(params[:branch_phone] || '45 33323120')
      #Americas EMEA APAC
      @engine.select_list(:name, "wlw-select_key:{actionForm.byb_account_region}").select_value("APAC")

      #Integrated Buyer?
      #wlw-radio_button_group_key:{actionForm.byb_mtml_buyer}

      @engine.text_field(:name => /{actionForm.byb_group_fleet}/).set(params[:group_fleet] || '1')
      @engine.text_field(:name => /{actionForm.byb_no_of_registered_ships}/).set(params[:num_ships] || '0')
      @engine.text_field(:name => /{actionForm.byb_est_annual_po_rate}/).set(params[:annual_po_rate] || '200')

      @engine.text_field(:name => /{actionForm.byb_registrant_first_name}/).set(params[:contact_first_name] || 'Anne Kristine')
      @engine.text_field(:name => /{actionForm.byb_registrant_last_name}/).set(params[:contact_last_name] || 'King')
      @engine.text_field(:name => /{actionForm.byb_registrant_email_address}/).set(params[:contact_email] || 'aking@shipserv.com')
      @engine.text_field(:name => /{actionForm.byb_registrant_designation}/).set(params[:contact_designation] || 'Purchase Manager')
      @engine.text_field(:name => /{actionForm.byb_registrant_phone_no}/).set(params[:contact_phone_no] || '45 33323120')

      #Service Profile
      @engine.radio(:name => "wlw-radio_button_group_key:{actionForm.byb_sts}", :value =>'ACT').send_keys :space if params[:account_status] == 'Active'
      @engine.radio(:name => "wlw-radio_button_group_key:{actionForm.byb_sts}", :value =>'INA').send_keys :space if params[:account_status] == 'Inactive'
      @engine.radio(:name => "wlw-radio_button_group_key:{actionForm.byb_test_account}", :value =>'Y').send_keys :space if params[:test_account] == 'yes'
      @engine.radio(:name => "wlw-radio_button_group_key:{actionForm.byb_test_account}", :value =>'N').send_keys :space if params[:test_account] == 'no'
      @engine.radio(:name => "wlw-radio_button_group_key:{actionForm.byb_list_in_buyer_directory}", :value =>'Y').send_keys :space if params[:account_in_use] == 'yes'
      @engine.radio(:name => "wlw-radio_button_group_key:{actionForm.byb_list_in_buyer_directory}", :value =>'N').send_keys :space if params[:account_in_use] == 'no'
      @engine.radio(:name => "wlw-radio_button_group_key:{actionForm.byb_internet_access_available}", :value =>'Y').send_keys :space if params[:internet_availability] == 'yes'
      @engine.radio(:name => "wlw-radio_button_group_key:{actionForm.byb_internet_access_available}", :value =>'N').send_keys :space if params[:internet_availability] == 'no'
    end

    def fill_in_supplier_fields(params = {})
      @engine.radio(:name => /wlw-radio_button_group_key:{actionForm.is_new_org}/, :value => "new").send_keys :space if params[:is_org] == 'new'
      @engine.radio(:name => /wlw-radio_button_group_key:{actionForm.is_new_org}/, :value => "exist").send_keys :space if params[:is_org] == 'exist'
      @engine.text_field(:name => /{actionForm.sup_supplier_name}/).set(params[:org_name] || 'Baker Bass Parent (for demos only)')
      @engine.text_field(:name => /{actionForm.spb_name}/).set(params[:branch_name] || 'King Kong Supplier (For Demos Only) ')
      @engine.text_field(:name => /{actionForm.spb_branch_address_1}/).set(params[:branch_address] || 'International House')
      @engine.text_field(:name => /{actionForm.spb_phone_no_1}/).set(params[:branch_phone] || '+44 2077 661 521')
      @engine.text_field(:name => /{actionForm.spb_city}/).set(params[:branch_city] || 'London')
      @engine.text_field(:name => /{actionForm.spb_zip_code}/).set(params[:branch_zip] || 'EC2A')
      @engine.select_list(:name, "wlw-select_key:{actionForm.spb_country}").select_value("GB")
      @engine.text_field(:name => /{actionForm.spb_email}/).set(params[:trading_contact_email] || 'aking@shipserv.com')
      @engine.text_field(:name => /{actionForm.spb_contact_name}/).set(params[:trading_contact_name] || 'Anne Kristine King')
      @engine.text_field(:name => /{actionForm.public_contact_email}/).set(params[:directory_public_email] || 'aking@friendster.com')

      #Service Profile
      #Americas EMEA APAC
      @engine.select_list(:name, "wlw-select_key:{actionForm.spb_account_region}").select_value("EMEA")

      @engine.radio(:name => "wlw-radio_button_group_key:{actionForm.spb_test_account}", :value =>'N').send_keys :space if params[:test_account] == 'no'
      @engine.radio(:name => "wlw-radio_button_group_key:{actionForm.spb_test_account}", :value =>'Y').send_keys :space if params[:test_account] == 'yes'

      #Other Details
      @engine.select_list(:name, "wlw-select_key:{actionForm.directory_entry_status}").select_value("HIDDEN")
    end
  end
end