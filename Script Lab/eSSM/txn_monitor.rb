require 'essm/helpers'

module Essm
  class TxnMonitor < Helpers
    attr_accessor :suppliertype, :doctype, :status, :suppliername, :vessel, :reference_no
    attr_accessor :email1, :email2, :timezone, :tz

    def txn_monitor
      login_user(@username, @password)
      @engine.link(:href => /buyertxnmonitor/).send_keys :enter

      until @engine.title == 'Transaction Monitor' do
        sleep 1
      end
      sleep(2)
      return @page_status = 'ok' if @engine.title == 'Transaction Monitor'
    end

    def transaction_monitor
      txn_monitor()
      x = (@engine.title == 'Transaction Monitor') ? true : false
      output = log_results(:action => 'SINGLE', :result => x, :notes_fail => "Page title - #{@engine.title}")
      return output
    end

    def txn_monitor_via_admin
      login_user(@username, @password)
      @engine.link(:href => /buyertxnmonitor/).send_keys :enter
      @engine.wait

      until @engine.title == 'Transaction Monitor' do
        sleep 1
      end
      x = (@engine.title == 'Transaction Monitor') ? true : false
      output = log_results(:action => 'SINGLE', :result => x, :notes_fail => "Page title - #{@engine.title}")
      return output
    end

    def txn_monitor_via_netadmin
      login_user_via_netadmin(@username, @password)
      @engine.link(:href => /tradenetprofile\/other\.do/).send_keys :enter
      @engine.wait
      @engine.link(:text => 'Buyer Transaction Monitor').send_keys :enter
      sleep(5)
      @engine.window(:title, 'Transaction Monitor').use
      x = (@engine.title == 'Transaction Monitor') ? true : false
      output = log_results(:action => 'SINGLE', :result => x, :notes_fail => "Page title - #{@engine.title}")
      return output
    end

    def search_query
      if @status.nil? && @suppliertype.nil? && @doctype.nil? then
        @engine.button(:name => /showTxnMonitorReport/).send_keys :enter
        sleep(3)
      else
        status = check_status(@status) if !@status.nil?
        suppliertype = check_supplier_type(@suppliertype) if !@suppliertype.nil?
        doc_type = check_doc_type(@doctype) if !@doctype.nil?

        fill_in_txn_monitor(:supplier_type => suppliertype,
                            :doc_type => doc_type,
                            :status => status,
                            :spb_branch_code => @suppliername,
                            :vessel => @vessel,
                            :reference_no => @reference_no)
        @engine.button(:name => /showTxnMonitorReport/).send_keys :enter
        sleep(5)
      end
      if @engine.url =~ /buyertxnmonitor/ then
        result = @engine.text.include?('No Data Found') ? "No Data Found" : "Search OK"
      elsif @engine.url =~ /buyertxnhistory/ then
        result = "Search OK" if !@engine.text.include?("click the 'Search' button")
      end
      return result
    end

    def get_base_time
      @engine.select_list(:name => /timeZoneId/).select_value('Africa/Abidjan')
      @engine.wait
      x = @engine.span(:id, 'rowDate').text
      time_utc = DateTime.strptime(x, "%d %b %Y %I:%M %p").to_time.utc
      return time_utc
    end

    # Checks if 'tz' variable is same as the select list initial selected value
    def verify_tz_value
      tz = @engine.select_list(:name => /timeZoneId/).value
      val = check_timezone(tz)
      x = (@tz == val) ? true : false
      output = log_results(:action => 'SINGLE', :result => x, :notes_fail => "") #TODO: Fill up notes when test fail
      return output
    end

    #Checks for the timezone select list
    def verify_timezone_list
      count = @engine.select_list(:name => /timeZoneId/).options.count
      x = (@engine.select_list(:name => /timeZoneId/).exist?) ? true : false
      output = log_results(:action => 'SINGLE', :result => x, :notes_pass => "Automation Update - #{count} total option count", :notes_fail => "Timezone drop down list doesn't exist'")
      return output
    end

    #Change the timezone by selecting a value in the select list
    def change_timezone
      puts "==> Selecting #{@timezone} as my NEW timezone"
      timezone = check_timezone(@timezone)
      @engine.select_list(:name => /timeZoneId/).select_value(timezone)
      @engine.wait
      puts '==> Timezone Change Successfully'
      output = check_timezone(@engine.select_list(:name => /timeZoneId/).value)
      return output
    end

    def verify_timezone
      timezone = check_timezone(@timezone)
      x = @engine.span(:id, 'rowDate').text
      time_utc = DateTime.strptime(x, "%d %b %Y %I:%M %p").to_time.utc
      @new_time = output = time_utc

      #table.each{|a| time << a.td(:class=> 'list', :index=>0).text }
      #time.each{ |x| converted << DateTime.strptime(x, "%d %b %Y %I:%M %p").to_time }
      puts "NEW TIME :: #{@new_time}"
      return output
    end

    def verify_query
      if @engine.text.include?('No Data Found') then
        output = 'No Data Found'
      else
        result = []
        table = @engine.tables(:style, /break-word/)
        doctype = @doctype.gsub('s', '')

        if @doctype != nil then
          table.each { |x|
            if (x.td(:class=> 'list', :index=>1).text =~ /#{doctype}/) then
              result << true
            else
              result << false
            end
          }
        end
        #Buyer Ref No => reference_no
        #table.each{|a| result << a.td(:class=> 'list', :index=>3).text.include?(@reference_no) } # Buyer Ref

        #supplier ID
        if @suppliername != nil then
          table.each { |x|
            if (x.td(:class=> 'list', :index=>6).text =~ /#{@suppliername}/) then
              result << true
            else
              result << false
            end
          }
        end
        #Supplier ID
        if @vessel != nil then
          table.each { |x|
            vessel_value = "#{@vessel}"[0..27]
            if (x.td(:class=> 'list', :index=>7).text =~ /#{vessel_value}/) then
              puts x.td(:class=> 'list', :index=>7).text
              puts "#{vessel_value}"
              result << true
            else
              result << false
            end
          }
        end
        if @status != nil then
          table.each { |x|
            if (x.td(:class=> 'list', :index=>8).text =~ /#{@status}/) then
              result << true
            else
              result << false
            end
          }
        end
        x = result.include? false ? false : true
        output = log_results(:action => 'SINGLE', :result => x, :notes_fail => "Search result/s have mismatch values")
      end
      return output
    end

    def resend_rfq
      result = []
      @engine.table(:style=> /break-word/, :text => /RFQ/).link(:href, /resend/).send_keys :enter
      until @engine.text.include? 'Resend Transaction Notification Email' do
        sleep 1
      end
      result << (@engine.text.include? 'Resend Transaction Notification Email')
      result << (@engine.text_field(:name => 'email1').value == @email1)
      result << (@engine.text_field(:name => 'email2').value == @email2)
      @engine.button(:value => 'Resend').fire_event('onclick')
      until @engine.text.include? 'successfully resent transaction' do
        sleep 1
      end
      result << (@engine.text.include? "successfully resent transaction notification email to supplier ID")
      puts result

      x = result.include? false ? false : true
      output = log_results(:action => 'SINGLE', :result => x, :notes_fail => "Error occurs when resending RFQ")
      return output
    end

    def resend_po
      result = []
      @engine.table(:style=> /break-word/, :text => /PO/).link(:href, /resend/).send_keys :enter
      until @engine.text.include? 'Resend Transaction Notification Email' do
        sleep 1
      end
      result << (@engine.text.include? 'Resend Transaction Notification Email')
      result << (@engine.text_field(:name => 'email1').value == @email1)
      result << (@engine.text_field(:name => 'email2').value == @email2)
      @engine.button(:value => 'Resend').fire_event('onclick')
      until @engine.text.include? 'successfully resent transaction' do
        sleep 1
      end
      result << (@engine.text.include? "successfully resent transaction notification email to supplier ID")
      puts result

      x = result.include? false ? false : true
      output = log_results(:action => 'SINGLE', :result => x, :notes_fail => "Error occurs when resending PO")
      return output
      #@engine.button(:value => 'Cancel').send_keys :enter
    end

    def fill_in_txn_monitor(params = {})
      #bybBranchCode
      @engine.select_list(:name => /supplier_type/).select_value(params[:supplier_type] || 'all')
      @engine.select_list(:name => /doc_type/).select_value(params[:doc_type] || 'all')
      @engine.select_list(:name => /status/).select_value(params[:status] || 'all')
      @engine.select_list(:name => /spb_branch_code/).select_value(params[:spb_branch_code] || '0')
      @engine.select_list(:name => /vessel/).select_value(params[:vessel] || 'all')
      @engine.text_field(:name => /reference_no/).set(params[:reference_no])
      @engine.radio(:value => /startswith/).set if params[:search_type] == 'startswith'
      @engine.radio(:value => /anywhere/).set if params[:search_type] == 'anywhere'
    end

    def select_buyer_company
      @engine.select_list(:name => /bybBranchCode/).select_value(params[:branch_code] || '10141')
      @engine.button(:name => /showTxnMonitorReport/).send_keys :enter

    end

    def verify_printable
      hash = Hash.new
      result = []
      url_link = @engine.table(:style=> /break-word/, :text => /#{@doctype}/).link(:href, /showPrintableView/).href
      doc_title = check_doc_alias(@doctype)
      hash = get_field_values()
      @engine.goto(url_link)
      @engine.wait
      sleep(3)
      if @engine.text.include? doc_title then
        %w{buyer_ref supplier_name vessel }.each { |x|
          result << (@engine.text.include?(hash[x.to_sym]) if hash.has_key? x.to_sym)
        }
        x = result.include? false ? false : true
        output = log_results(:action => 'SINGLE', :result => x, :notes_fail => "Data Incorrect for #{doc_title}")
      else
        x = false
        output = log_results(:action => 'SINGLE', :result => x, :notes_fail => "Incorrect Page Document - #{@engine.title}")
      end

      return output
    end

    def reset_query
      result = []
      @engine.button(:value, 'Reset').send_keys :enter
      puts "RESET Successful"
      result << (@engine.select_list(:name => /supplier_type/).value == 'all')
      result << (@engine.select_list(:name => /doc_type/).value == 'all')
      result << (@engine.select_list(:name => /status/).value == 'all')
      result << (@engine.select_list(:name => /spb_branch_code/).value == '0')
      result << (@engine.select_list(:name => /vessel/).value == 'all')
      result << (@engine.text_field(:name => /reference_no/).value == '')
      result << (@engine.radio(:value => /startswith/).set?)

      x = result.include?(false) ? false : true
      output = log_results(:action => 'SINGLE', :result => x, :notes_fail => "Reset Search Criteria Fail")
      return output
    end
  end
end