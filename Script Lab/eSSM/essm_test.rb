require 'rubygems'
require 'watir'
require './console'
require 'net/http'
require 'uri'

#check_page_status("#{address}/buyertxnhistory/admin/secured/showPrintableView.do?spb_branch_code=70455&doc_type=INV&internal_ref_no=5801&orgBranchCode=")

@engine = Watir::Browser.new 
address = "http://test.shipserv.com"
@engine.goto "#{address}/login.jsp"
@engine.text_field(:id => 'userid').when_present.set('b_king')
@engine.text_field(:id => 'password').when_present.set('shipserv')
@engine.link(:href=>'javascript:fnLogin()').send_keys :enter
puts @engine.url
sleep 5

Watir::Wait.until { (@engine.text.include?("Logout" || "Sign Out"))}
#puts ">>>>> Login Successful <<<<<" if (@engine.html.include? ("Logout" || "Sign Out"))
#@engine.link(:href => /buyertxnmonitor/).when_present.click
#Watir::Wait.until { @engine.url =~ /buyertxnmonitor/ }

def fill_in_txn_monitor(params = {})
  @engine.select_list(:name => /supplier_type/).select_value(params[:supplier_type] || 'all')
  @engine.select_list(:name => /doc_type/).select_value(params[:doc_type] || 'all')
  @engine.select_list(:name => /status/).select_value(params[:status] || 'all')
  @engine.select_list(:name => /spb_branch_code/).select_value(params[:spb_branch_code] || '0')
  @engine.select_list(:name => /vessel/).select_value(params[:vessel] || 'all')
  @engine.text_field(:name => /reference_no/).set(params[:reference_no])
  @engine.radio(:value => /startswith/).set if params[:search_type] == 'startswith'
  @engine.radio(:value => /anywhere/).set if params[:search_type] == 'anywhere'
end

def check_page_status(link)
  u = URI.parse link
  status_code = Net::HTTP.start(u.host,u.port){|http| http.head(u.request_uri).code }
  puts "STATUS CODE: #{status_code} LINK: #{link} "
  return status_code
end

    def reset_query
      result = []
      @engine.button(:value, 'Reset').send_keys :enter
      puts "RESET OK"
      result << (@engine.select_list(:name => /supplier_type/).value == 'all')
      result << (@engine.select_list(:name => /doc_type/).value == 'all')
      result << (@engine.select_list(:name => /status/).value == 'all')
      result << (@engine.select_list(:name => /spb_branch_code/).value == '0')
      result << (@engine.select_list(:name => /vessel/).value == 'all')
      result << (@engine.text_field(:name => /reference_no/).value == '')
      result << (@engine.radio(:value => /startswith/).set?)
      result << (@engine.select_list(:name => /week/).value == '0' if @engine.select_list(:name => /week/).exists?)
      #result << (@engine.text_field(:name => /fromdt_show/).value == '')
      #result << (@engine.text_field(:name => /todt_show/).value == '')
      #puts result
      if result.include? false then
        return 'Fail'
      end
    end


Console.start(binding)




