require 'essm/helpers'

module Essm
  class Authentications < Helpers
    def initialize
      @log_results = RallyUpdate.new
    end

    def logged_in
      login_user(@username, @password)

      x = @engine.html.include?("Logout") ? true : false
      output = log_results(action: 'SINGLE', result: x, notes_fail: "Login Failed for #{@username}")
    end

    def logged_out
      login_user(@username, @password)
      logout_user()

      x = !@engine.html.include?("Logout") ? true : false
      output = log_results(action: 'SINGLE', result: x, notes_fail: "Logout Failed for #{@username}")
    end
  end
end