require 'fit/column_fixture'
require 'helpers/rally_update'
require 'helpers/config'
require 'win32ole'
require 'net/http'
require 'uri'

class Helpers < Fit::ColumnFixture
  attr_accessor :env, :browser, :username, :password, :test_case
  include Setup
  include Rally

  def login_user(username, password)
    config = ConfigSetup.new
    @engine = config.browser(@browser)
    @address = config.web_environment(@env)
    #@engine.Cookies.clear
    @engine.goto @address
    check_user_session()
    @engine.goto "#{@address}/login.jsp"
    @engine.text_field(:id => 'userid').when_present.set(username)
    @engine.text_field(:id => 'password').when_present.set(password)
    @engine.link(:href=>'javascript:fnLogin()').send_keys :enter
    #if @engine.link(:href=>/LoginToSystem/).exists? then
    #  @engine.link(:href=>/LoginToSystem/).send_keys :enter
    #end
    #Watir::Wait.until { @engine.html.include? "Logout" }
    @engine.wait
    puts ">>>>> Login Successful <<<<<" if (@engine.html.include?("logout") || @engine.html.include?("signout"))
  end

  def login_user_via_netadmin(username, password)
    config = ConfigSetup.new
    @engine = config.browser(@browser)
    @address = config.web_environment(@env)
    @engine.clear_cookies
    @engine.goto "#{@address}/admin"
    check_user_session()
    @engine.goto "#{@address}/admin"
    @engine.text_field(:name => /login/).when_present.set(username)
    @engine.text_field(:name => /password/).when_present.set(password)
    @engine.button(:value =>'login').send_keys :enter
    Watir::Wait.until { @engine.html.include? "Logout" }
    puts ">>>>> Login Successful via Net Admin<<<<<" if @engine.html.include? "Logout"
  end

  def logout_user
    if @engine.url =~ /shipservadmin/
      @engine.link(:href, /logout/).click
      @engine.text.include?('You have been successfully LoggedOut')
    elsif @engine.html.include?("Logout")
      @engine.link(:text => "Logout").send_keys :enter
      autoit = WIN32OLE.new('AutoItX3.Control')
      autoit.WinWait('Message from webpage')
      autoit.WinActivate('Message from webpage')
      autoit.Send("{Enter}")
      #@engine.wait
      sleep(3)
    end
    puts ">>>>> Logout Successful <<<<<" if !@engine.html.include? "Logout"
    return 'ok'
  end

  #check the status code of the given link
  def check_page_status(link)
    u = URI.parse link.href
    status_code = Net::HTTP.start(u.host, u.port) { |http| http.head(u.request_uri).code }
    puts "STATUS CODE: #{status_code} LINK: #{link} "
    return status_code
  end

  #check if user has session
  def check_user_session
    logout_user if @engine.html.include?("Logout")
  end

  def clear_cookies
    @engine.clear_cookies
    return 'ok'
  end

  #close the browser
  def destroy
    @engine.close
    return 'ok'
  end

  def log_results(params ={})
    if params[:action] == 'SINGLE'
      if params[:result] == true
        verdict = 'pass'
        notes = params[:notes_pass] || 'Automation Update'
      else
        verdict = 'fail'
        notes = params[:notes_fail]
      end
      puts "FINAL RESULT :: #{verdict} => #{notes}"
      puts "TEST CASE: #{@test_case} ENV: #{@env}"
      #log_rally = RallyUpdate.new
      #log_rally.rally_update(:test_case => @test_case, :result => verdict, :notes => notes, :env => @env)
      #return "LOGGED SUCCESSFULLY"

      return verdict
    end
  end

  ##### Txn Monitor & Txn History #####

  #match doctype against complete name
  def check_doc_alias(doc_type)
    value = case doc_type.downcase
              when /req/ then
                'Requisition'
              when /rfq/ then
                'Request For Quotation'
              when /po/ then
                'Purchase Order'
              when /qot/ then
                'Quotation'
              when /poc/ then
                'Purchase Order Confirmation'
              when /inv/ then
                'Invoice'
              else
                'invalid doc type'
            end
    puts "\nDoc Type is #{value}"
    return value
  end

  #match the doctype value in the select list
  def check_doc_type(doc_type)
    value = case doc_type.downcase
              when 'req', 'reqs' then
                'reqs'
              when 'rfq', 'rfqs' then
                'rfqs'
              when 'po', 'pos' then
                'pos'
              when 'rfqs & pos' then
                'sent'
              when 'qot', 'qots' then
                'qots'
              when 'poc', 'pocs' then
                'pocs'
              when 'inv', 'invs' then
                'invs'
              when 'qots & pocs' then
                'recv'
              else
                'all'
            end
    puts "\nDoc Type is #{value}"
    return value
  end

  #match the status value in the select list
  def check_status(status)
    value = case status.downcase
              when 'accepted' then
                'ACC'
              when 'confirmed' then
                'CON'
              when 'declined' then
                'DEC'
              when 'not replied to - all' then
                'NREP'
              when 'opened' then
                'OPN'
              when 'sent' then
                'SENT'
              when 'unopened' then
                'NEW'
              when 'quoted' then
                'QUO'
              else
                'all'
            end
    puts "\nStatus is #{value}"
    return value
  end

  #match the supplier type value in the select list
  def check_supplier_type(supplier_type)
    value = case supplier_type.downcase
              when 'startsupplier' then
                'startsupplier'
              when 'startsupplierstandard' then
                'startsupplierSTD'
              when 'startsupplierplus' then
                'startsupplierplus'
              when 'tradenetsupplier' then
                'tradenetsupplier'
              else
                'all'
            end
    puts "\nSupplier Type is #{value}"
    return value
  end

  #match the given GMT value against the select list values
  def check_timezone(timezone)
    timezone = timezone.gsub(/\s/, '')
    tz = {'GMT-11' => 'Pacific/Apia',
          'GMT-8' => 'Pacific/Pitcairn',
          'GMT-6' => 'America/Belize',
          'GMT-5' => 'America/Cayman',
          'GMT' => 'Africa/Abidjan',
          #'GMT0' => 'Africa/Abidjan',
          'GMT+5' => 'Asia/Aqtobe',
          'GMT+8' => 'Asia/Chongqing',
          'GMT+9' => 'Asia/Pyongyang',
          'GMT+11.5' => 'Pacific/Norfolk',
          'GMT+14' => 'Pacific/Kiritimati'
    }
    value = (tz.has_key? timezone) ? tz[timezone] : tz.key(timezone)
    puts "\n#{timezone} ==> TimeZone is #{value}"
    return value
  end

  #Get the value of the result table
  def get_field_values
    table = @engine.tables(:style, /break-word/).first
    hash = {:doctype => table.td(:class=> 'list', :index=>1).text.rstrip,
            :buyer_ref => table.td(:class=> 'list', :index=>3).text.rstrip,
            :supplier_ref => table.td(:class=> 'list', :index=>4).text.rstrip,
            :supplier_name => (table.td(:class=> 'list', :index=>5).text.rstrip).slice(0..27),
            :supplier_id => table.td(:class=> 'list', :index=>6).text.rstrip,
            :vessel => (table.td(:class=> 'list', :index=>7).text.rstrip).slice(0..27),
            :status => table.td(:class=> 'list', :index=>8).text.rstrip}
    return hash
  end

  def goto_next
    @engine.button(:text => /Next/).send_keys :enter
    @engine.wait
  end

  def goto_prev
    @engine.button(:text => /Previous/).send_keys :enter
    @engine.wait
  end

  def goto_first
    @engine.button(:text => /First/).send_keys :enter
    @engine.wait
  end

  def goto_last
    @engine.button(:text => /Last/).send_keys :enter
    @engine.wait
  end
end