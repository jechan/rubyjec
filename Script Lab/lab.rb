#include libraries and gems
require 'fileutils'
require 'D:/Shipserv/RUBY WORKSHOP/STD/STD scripts/mymethods_v04.rb'

#Global Variable Declaration
@@v_int_type='DANEQUIP'
@@v_ext='.XML'
@@v_dir_template=Dir.pwd
Dir.chdir(@@v_dir_template)

#Replaced Variables in documents later to be XML
@@v_buyer='10414'
@@v_supplier='59758'
attach='0'

@@v_doc_type=4
#1=RFQ, 2=ORD, 3=QOT, 4=POA/POC, 5=INV

#the test file create is dependent on doc_type
integrated_doc=Intfile.new
doc_ref_seq=get_doc_seq(@@v_doc_type)
@@v_unique_ref=gen_uc_ref_no(doc_ref_seq)
if @@v_doc_type.to_i == 1 or @@v_doc_type.to_i==2 then
	doc_ref_seq[@@v_doc_type.to_i+5] = @@v_unique_ref
end

@@v_rfq_dat_for_qot=find_rfq_dat_for_qot(get_uc_ref_no(@@v_doc_type)) if @@v_doc_type.to_i==3
@@v_ord_dat_for_poac=find_ord_dat_for_poac(get_uc_ref_no(@@v_doc_type)) if @@v_doc_type.to_i==4

##need to put error handling for QOT and POC so not to continue the whole script

Dir.chdir(@@v_dir_template) #need to return to template directory since work dir has been changed

integrated_doc.gen_ord_resp if @@v_doc_type==4

#Reading of template file and replacing variables
@@w_content=File.read(integrated_doc.get_template)
@@w_content.gsub!("v_buyer", @@v_buyer)
@@w_content.gsub!("v_supplier", @@v_supplier)
@@w_content.gsub!("v_unique_ref", @@v_unique_ref)
@@w_content.gsub!("v_rfq_dat_for_qot_0", @@v_rfq_dat_for_qot[0]) if @@v_doc_type.to_i==3
@@w_content.gsub!("v_rfq_dat_for_qot_1", @@v_rfq_dat_for_qot[1]) if @@v_doc_type.to_i==3
@@w_content.gsub!("v_ord_dat_for_poac", @@v_ord_dat_for_poac) if @@v_doc_type.to_i==4


#write to a new file and move
integrated_doc.create_file
create_file_attachments(attach)

#write to _SEQ file
write_file= write_seq_refno(doc_ref_seq)

