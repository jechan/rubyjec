require 'rubygems'
require 'oci8'
require 'nokogiri'


directory='C:/Shipserv Backup/RUBY WORKSHOP/Script Lab/Evaluation Scripts/Man BW Quotes/52471-quotes'

tnsnames_uat = '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1530)) (CONNECT_DATA = (SID = SSTEST05)))'
tnsnames_dev = '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = 172.30.1.7)(PORT = 1521)) (CONNECT_DATA = (SID = ssdev06)))'
tnsnames_live = '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = localhost)(PORT = 1531)) (CONNECT_DATA = (SID = ssprod04)))'

db=OCI8.new('readonly','iship',tnsnames_live)
query=%Q{select mim_msg_no, mim_document_name||':'||mim_internal_no from mr_internal_mapping
where mim_internal_no not in (select QOT_RFQ_INTERNAL_REF_NO from quote)
and mim_branch_code=59758
and mim_document_name='RequestForQuote'
and to_char(mim_created_date, 'YYYY/MM/DD')='2012/07/05'
order by mim_created_date desc
}

rfq=[]
db.exec(query) do |row|
	rfq << row
end

files=Dir[directory+'/*.*']
rfq_qot_files=[]
i=0
while i < rfq.length do
	rfq[i] << files[i]
	rfq_qot_files << rfq[i]
	i+=1
end

puts rfq_qot_files.inspect
rfq_qot_files.each do |rq|
		#read file parse xml
		qot_src=Nokogiri::XML(File.read(rfq[2]))
		#replace message number
		quote=qot_src.at_xpath("//Quote")
		quote[:MessageNumber]=rq[0]
		#replace AGI
		agi=qot_src.at_xpath('//Reference[@Qualifier="AGI"]')
		agi[:ReferenceNumber]=rq[1]
		#replace buyer
		interchange=qot_src.at_xpath("//Interchange")
		interchange[:Recipient]='10414'
		#replace supplier
		interchange[:Sender]='59758'
		#replace emails
		all_em=qot_src.at_xpath('//CommunicationMethod[@Qualifier="EM"]')
		all_em[:Number]='jchan@shipserv.com' if all_em != nil
		#Save file
		Dir.chdir('C:/Shipserv Backup/RUBY WORKSHOP/Script Lab/Evaluation Scripts/Man BW Quotes/replaced')
		File.open(File.basename(rfq[2]),'w') do |file|
			file.puts qot_src
			end
	end

#~ puts File.directory?(directory)

#~ a={:value => 1, :another => 2}
#~ b={:value => 3, :another => 4}

#~ letter={:a => a, :b=> b}



#~ Dir.glob(directory+'/*') do |file|
	#~ a << File.basename(file)
	#~ end

#~ puts a.inspect


#~ a=[[1,2],[3,4]]
#~ b=['c','d']
#~ i=0
#~ c=[]

#~ while i < a.length do
	 #~ a[i] << b[i]
	 #~ c << a[i]
	 #~ i=i+1
#~ end
#~ puts c.inspect

#~ c.each do |test|
	#~ puts test[0]
	#~ puts test[1]
	#~ puts test[2]
	#~ end

#~ a.each do |var|
	#~ puts var << b[]
#~ end

#~ puts a[0]
#~ puts b[0]
#~ a[0] << b[0]
