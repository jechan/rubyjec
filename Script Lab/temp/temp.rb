require 'nokogiri'

data=%Q{<?xml version="1.0"?>

<Order OrderType="1">
  <Header>
    <Reference>3148594</Reference>
    <OrderReference/>
    <CustomerCode>10414</CustomerCode>
    <CustomerReference>ORD:121012174605254</CustomerReference>
    <Date>20121012</Date>
    <VesselName>T/h.e Flying DutcHMAN</VesselName>
    <IMONumber>JC2010030401</IMONumber>
    <DeliveryDate>20111113</DeliveryDate>
    <DeliveryTo>CHAN-JE, U.N. Avenue, Taft Avenue</DeliveryTo>
    <DeliveryTerms>FCA</DeliveryTerms>
    <TransportMode>_3</TransportMode>
    <FreightForwarder>Freight forwarder ord fw</FreightForwarder>
    <Company>ord TEST SHIPNET BUYER</Company>
    <Contact>Roque Andaya ord - Buyer</Contact>
    <Telephone/>
    <Email/>
    <InvoiceTo>
      <Name>IV ordAndaya Smart TN90003 Supplies</Name>
      <Address>IV IVAdd 1 - Invoice toAdd 2, Taft Avenue</Address>
      <City>IV of Manila</City>
      <ZipCode>1100</ZipCode>
      <Country>Roque BA ord cn</Country>
    </InvoiceTo>
    <PaymentTerms/>
    <Currency>USD</Currency>
    <LineItemCount>1</LineItemCount>
    <SubTotal>60</SubTotal>
    <Discount>0</Discount>
    <TotalAmount>50</TotalAmount>
    <Comments>
      <Comment>buyer comments  Port:CHAN-JE-Packing instructions</Comment>
    </Comments>
  </Header>
  <Lines>
    <Line>
      <LineNumber>1</LineNumber>
      <Description>[UOM:PCE] v_description</Description>
      <ItemId>20120423</ItemId>
      <Qty>24</Qty>
      <Unit>PCS</Unit>
      <UnitPrice> 1</UnitPrice>
      <LineTotal> 20.34</LineTotal>
      <Comments>
        <Comment>Comment's 2 For : For second section, Desc : Desc, Mfg : Man, Model : Model, Rating : Rating, S/N : Serial, Dwg : Dwg, Type : Type</Comment>
      </Comments>
    </Line>
  </Lines>
</Order>
}

    xml=Nokogiri::XML(data)
    int_ref=xml.at_xpath('//Reference').content
    puts int_ref
