require 'FileUtils'
require 'C:/Shipserv Backup/RUBY WORKSHOP/STD/STD scripts/mymethods_v03.rb'

#Global Variable Declaration
@@v_int_type='LAB'
@@v_ext='.XML'
@@v_dir_template='C:/Shipserv Backup/RUBY WORKSHOP/STD/LAB/'
Dir.chdir(@@v_dir_template)
@@v_pref='JC'

#Replaced Variables in documents later to be XML
@@v_buyer='10414'
@@v_supplier='59758'

@@v_doc_type=4

@@v_sample=%Q{<?xml version="1.0"?>
<MTML>
	<Interchange ControlReference="RequestForQuote:4361767" Identifier="UNOC" PreparationDate="2012-Apr-30" PreparationTime="18:27" Recipient="59758" RecipientCodeQualifier="ZEX" Sender="10414" SenderCodeQualifier="ZEX" VersionNumber="2">
		<RequestForQuote AssociationAssignedCode="MARL10" ControllingAgency="UN" CurrencyCode="USD" DeliveryTermsCode="EXW" FunctionCode="_9" LineItemCount="1" MessageNumber="90001489166" MessageReferenceNumber="RequestForQuote:4361767" Priority="High" ReleaseNumber="96A" TaxStatus="Exempt" TransportModeCode="_2" VersionNumber="D">
			<DateTimePeriod FormatQualifier="_203" Qualifier="_132" Value="200803240000"/>
			<DateTimePeriod FormatQualifier="_203" Qualifier="_133" Value="200803280000"/>
			<DateTimePeriod FormatQualifier="_203" Qualifier="_2" Value="201111130000"/>
			<DateTimePeriod FormatQualifier="_203" Qualifier="_175" Value="200803200000"/>
			<DateTimePeriod FormatQualifier="_203" Qualifier="_137" Value="201204301827"/>
			<Comments Qualifier="PUR">
				<Value> PLEASE QUOTE FOR PRICE AND DELIVERY FOR THE FOLLOWING ITEMS NO WORK IS TO BE UNDERSTAKEN OR GOOD SUPPLIED UNTIL AN APPROVED PURCHASE ORDER HAS BEEN RECEIVED. ******* PLEASE ADVISE HOW QUICKLY YOU CAN DESPATCH TO THE ABOVE AGENTS AND THE COST OF DELIVERY TO DENMARK **********             </Value>
			</Comments>
			<Comments Qualifier="ZTP">
				<Value>Payment terms's             </Value>
			</Comments>
			<Comments Qualifier="ZTC">
				<Value>'RFQ General terms''s and conditions             ' :AND: RFQ CUSTOMER DATA FROM 10414 --&gt; TERMS &amp; CONDITIONS :- 1.   PLEASE DO NOT CHANGE UOM (UNIT OF MEASUREMENT) AND CURRENCY AS AGREED ON OUR B+H SPECIFIED PURCHASING REQUIREMENT. 2.   ANY ADDITIONAL COSTS SUCH AS BUT NOT LIMITED TO PACKING AND / OR FREIGHT COSTS, MUST BE STIPULATED YOUR QUOTATION. 3.   COMPLETE ALL PAGES AND INDICATE AVAILABILITY / DELIVERY TIME 4.   REVIEW ALL QUANTITY, UOM AND PART NUMBER. 5.   MARK CHANGES, IF ANY, FAILURE TO COMPLY MAY RESULT IN DISQUALIFICATIONS. 6.   ALL SERVICES / ATTENDANCE TO THE VESSEL MUST COMPLY TO THE STANDARDS OF ISM CODE / ISO 9002 7.   ALL ELECTRONICS EQUIPMENT SUPPLIED MUST BE IN ACCORDANCE TO YEAR 2000 COMPLIANCE. 8.  PAYMENT TERMS : 90 days </Value>
			</Comments>
			<Comments Qualifier="ZAT">
				<Value>TEST RFQ MTML JCRFQ120430002's subject</Value>
			</Comments>
			<Reference Qualifier="UC" ReferenceNumber="UC.JCRFQ120430002s"/>
			<Reference Qualifier="AGI" ReferenceNumber="RequestForQuote:4361767"/>
			<Party Identification="1002510414" Name="MV Erwin MTML" Qualifier="UD">
				<PartyLocation Port="AL-DRZ" Qualifier="ZUC"/>
			</Party>
			<Party Name="RFQ Freight forwarder" Qualifier="FW"/>
			<Party Qualifier="CN">
				<StreetAddress> RFQ Deliver To's,  Details</StreetAddress>
			</Party>
			<Party Name="10414 DEV Baker Engineering UK Ltd (For Demos Only)" Qualifier="BY">
				<Contact FunctionCode="PD" Name="RFQ Erwin Chan - Purchaser's">
					<CommunicationMethod Number="RFQ +63 2 488-3850" Qualifier="TE"/>
					<CommunicationMethod Number="jcsserv#gmail.com" Qualifier="EM"/>
				</Contact>
			</Party>
			<Party City="RFQ Manila" CountryCode="RFQ PH" CountrySubEntityIdentification="RFQ Metro Manila" PostcodeIdentification="RFQ 1000" Qualifier="BA">
				<StreetAddress>RFQ UN Avenue corner's Taft Ave</StreetAddress>
			</Party>
			<LineItem Description="[for testing only] DOVER AND CALAIS TO ORF.N. AND SCHEV." Identification="RFQ181.01.01.001" MeasureUnitQualifier="PCE" Number="1" Quantity="1" TypeCode="VP">
				<Comments Qualifier="LIN">
					<Value>Comments 1</Value>
				</Comments>
				<Section DepartmentType="Type" Description="Desc" DrawingNumber="Dwg" Manufacturer="Man" ModelNumber="Model" Name="For first section" Rating="Rating" SerialNumber="Serial"/>
			</LineItem>
		</RequestForQuote>
	</Interchange>
</MTML>}

def get_attr_val(attr)
	attr_pattern = /#{attr}="(\S+)"/
	return @@v_sample.scan(attr_pattern).first
end

#~ node="MTML"
#~ def get_node_val(node)
	#~ node_pattern = /<#{node}>\s.*\b/
	#~ puts node_pattern
	#~ puts @@v_sample.scan(node_pattern)
#~ end

def get_node_attr(node)
end

puts get_attr_val("ControlReference")
puts get_attr_val("MessageNumber")
