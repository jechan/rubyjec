require 'rubygems'
require 'savon'
require 'base64'
require 'net/http'
require 'yaml'

class Ssws
  attr_reader :ssws_profile, :ssws_pwd, :ssws_src_file, :ssws_client, :ssws_dest_dir, :ssws_dest_file
  attr_reader :ssws_tnid, :ssws_user_id, :ssws_user_pwd
  attr_accessor :doc_type, :doc_id
  
  
  def initialize
    #@ssws_src_file=File.read("#{ENV['HOME']}/source_path.txt")
    @ssws_dest_dir="#{ENV['HOME']}/SS_FILES_DOWNLOADED/"
  end
  
  def set_ssws_environment_client(env)
    case env.upcase.to_sym
    when :DEV
      ssws_env='http://dev.shipserv.com/SSMTML/MTMLLink.asmx?wsdl'
    when :UAT1
      ssws_env= 'http://test.shipserv.com/SSMTML/MTMLLink.asmx?wsdl'
    when :UAT2
      ssws_env= 'http://test2.shipserv.com/ssmtml/MTMLLink.asmx?wsdl'
    when :LIVE
      ssws_env='https://www.shipserv.com/SSMTML/MTMLLink.asmx?wsdl'
    when :TNCDEV
      ssws_env='http://devmono1.shipserv.com/tradenet/MTMLLink.asmx?wsdl'
    when :TNCDEVC
      ssws_env='http://devmono1.shipserv.com/tradenetC/MTMLLink.asmx?wsdl'
    when :TNCDEVD
      ssws_env='http://devmono1.shipserv.com/tradenetD/MTMLLink.asmx?wsdl'
    when :TNCDEVF
      ssws_env='http://devmono1.shipserv.com/tradenetF/MTMLLink.asmx?wsdl'
    when :TNCUAT
      ssws_env='http://test.shipserv.com/tradenet/MTMLLink.asmx?wsdl'
    when :TNCUAT2
      ssws_env='http://test.shipserv.com/tradenet_UAT2/MTMLLink.asmx?wsdl'
    when :TNCUATB
      ssws_env='http://test.shipserv.com/tradenetB/MTMLLink.asmx?wsdl'
    when :TNCLIVE
      ssws_env='https://www.shipserv.com/tradenet/MTMLLink.asmx?wsdl'
    when :UKDEV
      ssws_env='http://ukdev.shipserv.com/tradenet/MTMLLink.asmx?wsdl'
    when :CUSTOM
      ssws_env='http://172.30.1.234/TradeNet/MTMLLink.asmx?wsdl'
    else
      raise 'The environment is not defined. Please check'
    end
    
    @ssws_client=Savon::Client.new do
       http.auth.ssl.verify_mode = :none
       wsdl.document=ssws_env      
       case env.upcase.to_sym
       when :UAT2
         wsdl.endpoint="http://test2.shipserv.com/ssmtml/MTMLLink.asmx"
       when :TNCUAT
         wsdl.endpoint="http://test.shipserv.com/tradenet/MTMLLink.asmx"
       when :TNCLIVE
         wsdl.endpoint="https://www.shipserv.com/tradenet/MTMLLink.asmx"
       when :TNCUATB
         wsdl.endpoint="http://test.shipserv.com/tradenetB/MTMLLink.asmx"
       when :TNCUAT2
         wsdl.endpoint="http://test.shipserv.com/tradenet_UAT2/MTMLLink.asmx"
       when :UKDEV
         wsdl.endpoint="http://ukdev.shipserv.com/tradenet/MTMLLink.asmx"         
       else
       end
    end
  end
  
  #setter values
  def set_ssws_tnid(tnid)
    @@ssws_tnid=tnid
  end
  
  def set_ssws_user_id(user)
    @@ssws_user_id=user
  end
  
  def set_ssws_user_pwd(pwd)
    @@ssws_user_pwd=pwd
  end
  
  def set_ssws_profile(profile='STD')
    @@ssws_profile=profile
  end
  
  def set_source_ssws_file(data=File.read("#{ENV['HOME']}/source_path.txt"))
    if File.file?(data) then
      @ssws_src_file=File.read(data)
    else
      @ssws_src_file=data
    end
  end
  
  def set_dest_ssws_file(file)
    @ssws_dest_file=file
  end

#soap_actions  
  def display_soap_action
    puts @ssws_client.wsdl.soap_actions
  end  
 
  #Send document via SSWS
  def submit_file_to_ssws
    ssws_file=@ssws_src_file
    ssws_filename="ssws_direct"
    ssws_file_base64=Base64.encode64(ssws_file)
    response = @ssws_client.request :send_encoded_document, :xmlns => "urn:shipserv.mtml" do      
      soap.body={
        "UserIntegrationDoc" => {
          "AppDetails" =>{
            "Name" => 'DirectWS',
            "Version"=> '1.0'
          },
          "IntegrationDetails" =>{
            "TradeNetID"=> @@ssws_tnid,
            "UserID"=>@@ssws_user_id,
            "UserPassword"=>@@ssws_user_pwd,
            "IntegrationCode"=>@@ssws_profile.upcase
          },
          "DocumentDetails"=>{
            "DocumentType"=>'string',
            "ClientFileName"=>ssws_filename,
            "FileContentsAsBytes"=>ssws_file_base64,
            "EncodingTypeCode"=>'UTF-8'
          }
        }        
      }    
    end
    puts '=====OUTPUT====='
    ul_response=response.to_hash[:send_encoded_document_response][:send_encoded_document_result]
    puts ul_response
    puts ul_response[:result_code]
    puts ul_response[:error_message] if ul_response[:result_code]='Err'
    #puts ul_response[:file_contents] #ICR
    return response
  end
  
  def download_file_from_ssws
    response=@ssws_client.request :get_encoded_document, :xmlns => "urn:shipserv.mtml" do
      soap.body={
        "UserIntegrationDoc" => {
          "AppDetails" =>{
            "Name" => 'DirectWS',
            "Version"=> '1.0'
          },
          "IntegrationDetails" =>{
            "TradeNetID"=>@@ssws_tnid,
            "UserID"=>@@ssws_user_id,
            "UserPassword"=>@@ssws_user_pwd,
            "IntegrationCode"=>@@ssws_profile.upcase,
            "InvoiceIntegrationCode"=>@@ssws_profile.upcase
          },
          "DocumentDetails"=>{
            "EncodingTypeCode"=>'ASCII'
          }
        }                
      }      
    end
    puts '=====OUTPUT====='
    dl_response=response.to_hash[:get_encoded_document_response][:get_encoded_document_result]
    puts dl_response
    puts dl_response[:result_code]
    puts dl_response[:document_type]
    @doc_type=dl_response[:document_type].to_s
    puts dl_response[:ack_id]
    @doc_id=dl_response[:ack_id].to_s
    puts dl_response[:error_message] if dl_response[:result_code]=='Err'
    acknowledge_document if dl_response[:result_code]=='Success'
    #puts dl_response[:server_file_name]
    #puts dl_response[:file_contents_as_bytes]
    dl_content=Base64.decode64(dl_response[:file_contents_as_bytes])
    #puts dl_content
    #server_file_name=dl_response[:server_file_name].match(/\d*_\d*_\d*_\d*/)
    server_file_name=Time.now.strftime('%y%m%d%H%M%S%L')
    #save downloaded file
    dl_file_path=@ssws_dest_dir+server_file_name.to_s+'.'+@@ssws_profile
    File.open(dl_file_path,'w+') do |f|
      f.puts dl_content
    end
    #save downloaded path
    File.open("#{ENV['HOME']}/dest_path.txt",'w+:UTF-8') do |f|
      f.print dl_file_path
    end
    #puts dl_response
    return dl_content
  end
  
  #Other functions of ssws
  
  def get_ping
    response = @ssws_client.request :ping, :xmlns => "urn:shipserv.mtml"
    puts response
  end
  
  def get_count_remaining
    ssws_file_tnid=@@ssws_tnid
    ssws_file_user_id=@@ssws_user_id
    ssws_file_user_pwd=@@ssws_user_pwd
    ssws_file_profile=@@ssws_profile    
    response = @ssws_client.request :get_count_remaining, :xmlns => "urn:shipserv.mtml" do
      soap.body = {
        "strTradeNetID"=>ssws_file_tnid,
        "strUserID"=>ssws_file_user_id,
        "strPassword"=>ssws_file_user_pwd,
        "strIntegration"=>ssws_file_profile
      }
    end
    puts '=====OUTPUT====='
    result = response.to_hash[:get_count_remaining_response]
    puts result[:get_count_remaining_result]
    return response
  end
  
  def get_integration_codes(data='NIL')
    response=@ssws_client.request :get_integration_codes, :xmlns => "urn:shipserv.mtml"
    result=response[:get_integration_codes_response][:get_integration_codes_result][:string]
    if data=='NIL' then
      puts result
    else
      puts result.include?(data.upcase)
    end
  end
  
  def acknowledge_document
    ssws_file_tnid=@@ssws_tnid
    ssws_file_user_id=@@ssws_user_id
    ssws_file_user_pwd=@@ssws_user_pwd
    ssws_file_profile=@@ssws_profile
    ssws_doc_type=@doc_type
    ssws_doc_id=@doc_id   
    response=@ssws_client.request :acknowledge_document, :xmlns => "urn:shipserv.mtml"     do
      soap.body= {
        "strTradeNetID"=>ssws_file_tnid,
        "strUserID"=>ssws_file_user_id,
        "strPassword"=>ssws_file_user_pwd,
        "strIntegration"=>ssws_file_profile,
        "strDocType" => ssws_doc_type,
        "strAckID" => ssws_doc_id          
      }
      puts response
     #result=response.to_hash[:acknowledge_document_response][:acknowledge_document_result]
     #puts result[:result_code]
    end
  end
  
end


=begin     
         soap.body=%Q{
      <UserIntegrationDoc>
        <AppDetails>
          <Name>DirectWS</Name>
          <Version>1.0</Version>
        </AppDetails>
        <IntegrationDetails>
          <TradeNetID>#{ssws_file_tnid}</TradeNetID>
          <UserID>#{ssws_file_user_id}</UserID>
          <UserPassword>#{ssws_file_user_pwd}</UserPassword>
          <IntegrationCode>#{ssws_file_profile}</IntegrationCode>
        </IntegrationDetails>
        <DocumentDetails>
          <DocumentType>string</DocumentType>
          <ClientFileName>#{ssws_filename}</ClientFileName>
          <FileContentsAsBytes>#{ssws_file_base64}</FileContentsAsBytes>
          <EncodingTypeCode>UTF-8</EncodingTypeCode>
        </DocumentDetails>
    </UserIntegrationDoc>
    }
=end
=begin      soap.body=%Q{
       <UserIntegrationDoc>
        <AppDetails>
          <Name>DirectWS</Name>
          <Version>1.0</Version>
        </AppDetails>
        <IntegrationDetails>
          <TradeNetID>#{ssws_file_tnid}</TradeNetID>
          <UserID>#{ssws_file_user_id}</UserID>
          <UserPassword>#{ssws_file_user_pwd}</UserPassword>
          <IntegrationCode>#{ssws_file_profile}</IntegrationCode>
          <InvoiceIntegrationCode>string</InvoiceIntegrationCode>
        </IntegrationDetails>
        <DocumentDetails>
          <EncodingTypeCode>string</EncodingTypeCode>
        </DocumentDetails>
      </UserIntegrationDoc>
      }
=end