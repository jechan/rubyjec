require 'rubygems'
require 'ssws'

describe Ssws, 'Functions of ssws' do
	it 'submits file to the server' do
		ss=Ssws.new
		ss.set_ssws_environment('DEV')
		ss.set_ssws_buyer_id('10414')
		ss.submit_file_to_ssws
	end
	
	it 'pings to the server' do
		ss=Ssws.new
		ss.set_ssws_environment('DEV')
		ss.set_ssws_client
		ss.get_ping
	end
end