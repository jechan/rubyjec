require 'savon'
require 'base64'
require 'net/http'
require 'yaml'

class Ssws
  attr_reader :ssws_profile, :ssws_pwd, :ssws_src_file, :ssws_client, :ssws_dest_dir, :ssws_dest_file
  attr_reader :ssws_tnid, :ssws_user_id, :ssws_user_pwd
  attr_accessor :doc_type, :doc_id
  
  
  def initialize
    #@ssws_src_file=File.read("#{ENV['HOME']}/source_path.txt")
    @ssws_dest_dir="#{ENV['HOME']}/SS_FILES_DOWNLOADED/"
  end
  
  def set_ssws_environment_client(env)
    case env.upcase.to_sym
    when :DEV
      ssws_env='https://dev.shipserv.com/tradenet/MTMLLink.asmx?wsdl'
    when :UAT1
      ssws_env= 'https://legacytest.shipserv.com/tradenet/MTMLLink.asmx?wsdl'
    when :UATJM
      ssws_env= 'https://test.shipserv.com/tradenetJM/MTMLLink.asmx?wsdl'      
    when :UATSTAGE
      ssws_env= 'https://test.shipserv.com/tradenetSTAGE/MTMLLink.asmx?wsdl'           
    when :UATC
      ssws_env= 'https://test.shipserv.com/tradenetC/MTMLLink.asmx?wsdl'
    when :UAT3
      ssws_env= 'https://test3.shipserv.com/tradenet/MTMLLink.asmx?wsdl'      
    when :LIVE
      ssws_env='https://www.shipserv.com/tradenet/MTMLLink.asmx?wsdl'
    when :PREPROD
      ssws_env='https://www.shipserv.com/pretradenet/MTMLLink.asmx?wsdl'
    when :TNCDEV
      ssws_env='https://dev.shipserv.com/tradenet/MTMLLink.asmx?wsdl'
    when :DEVB
      ssws_env='https://dev.shipserv.com/tradenetB/MTMLLink.asmx?wsdl'
    when :DEVVB
      ssws_env='http://devtradenet.myshipserv.com/tradenetB/MTMLLink.asmx?wsdl'      
    when :TNCDEVC
      ssws_env='https://dev.shipserv.com/tradenetC/MTMLLink.asmx?wsdl'
    when :TNCDEVD
      ssws_env='https://dev.shipserv.com/tradenetD/MTMLLink.asmx?wsdl'
    when :TNCDEVE
      ssws_env='https://dev.shipserv.com/tradenetE/MTMLLink.asmx?wsdl'      
    when :TNCDEVF
      ssws_env='https://dev.shipserv.com/tradenetF/MTMLLink.asmx?wsdl'
    when :TNCDEVH
      ssws_env='https://dev.shipserv.com/tradenetH/MTMLLink.asmx?wsdl'      
    when :TNCDEVG
      ssws_env='https://dev.shipserv.com/tradenetG/MTMLLink.asmx?wsdl'
    when :TNCDEVI
      ssws_env='https://dev.shipserv.com/tradenetI/MTMLLink.asmx?wsdl'
    when :TNCDEVR
      ssws_env='https://dev.shipserv.com/tradenetR/MTMLLink.asmx?wsdl'                            
    when :TNCUAT
      ssws_env='https://test.shipserv.com/tradenet/MTMLLink.asmx?wsdl'
    when :TNCUAT2
      ssws_env='https://test2.shipserv.com/tradenet/MTMLLink.asmx?wsdl'
    when :TNCUAT3
      ssws_env='https://test3.shipserv.com/tradenet/MTMLLink.asmx?wsdl'      
    when :UATB
      ssws_env='https://legacytest.shipserv.com/tradenetB/MTMLLink.asmx?wsdl'
    when :TNCUATC
      ssws_env='https://legacytest.shipserv.com/uatpre-tradenetC/MTMLLink.asmx?wsdl'      
    when :TNCUATD
      ssws_env='https://test.shipserv.com/tradenetD/MTMLLink.asmx?wsdl'      
    when :TNCUATF
      ssws_env='https://test.shipserv.com/tradenetF/MTMLLink.asmx?wsdl'
    when :TNCUATE
      ssws_env='https://test.shipserv.com/tradenetE/MTMLLink.asmx?wsdl'                       
    when :TNCUATG
      ssws_env='https://test.shipserv.com/tradenetG/MTMLLink.asmx?wsdl'          
    when :TNCUATH
      ssws_env='https://test.shipserv.com/tradenetH/MTMLLink.asmx?wsdl'
    when :TNCUATK
      ssws_env='https://test.shipserv.com/tradenetK/MTMLLink.asmx?wsdl'                
    when :TNCUATI
      ssws_env='https://test.shipserv.com/tradenetI/MTMLLink.asmx?wsdl'
    when :TNCUATJM
      ssws_env='https://test.shipserv.com/tradenetJM/MTMLLink.asmx?wsdl'                 
    when :TNCLIVE
      ssws_env='https://www.shipserv.com/tradenet/MTMLLink.asmx?wsdl'
    when :TNCUATCHE
      ssws_env='https://test.shipserv.com/tradenetCHE/MTMLLink.asmx?wsdl'      
    when :UKDEV
      ssws_env='https://dev.shipserv.com/tradenet/MTMLLink.asmx?wsdl'
    when :DEVA
      ssws_env='https://dev.shipserv.com/tradenetA/MTMLLink.asmx?wsdl'
    when :CUSTOM
      ssws_env='https://172.30.1.234/TradeNet/MTMLLink.asmx?wsdl'
    else
      raise 'The environment is not defined. Please check'
    end
    
    
    @ssws_client=Savon.client(ssl_verify_mode: :none) do
       wsdl ssws_env
       namespace "urn:shipserv.mtml"
       case env.upcase.to_sym
       when :UAT1
         endpoint "https://legacytest.shipserv.com/tradenet/MTMLLink.asmx"
       when :UATJM
         endpoint "https://test.shipserv.com/tradenetJM/MTMLLink.asmx"
       when :UATSTAGE
         endpoint "https://test.shipserv.com/tradenetSTAGE/MTMLLink.asmx"                                      
       when :UATC
         endpoint "https://test.shipserv.com/tradenetC/MTMLLink.asmx"
       when :TNCUAT
         endpoint "https://test.shipserv.com/tradenet/MTMLLink.asmx"
       when :TNCUAT2
         endpoint "https://test2.shipserv.com/tradenet/MTMLLink.asmx"
       when :TNCUAT3
         endpoint "https://test3.shipserv.com/tradenet/MTMLLink.asmx"       
       when :UAT3
         endpoint "https://test3.shipserv.com/tradenet/MTMLLink.asmx"                       
       when :LIVE
         endpoint "https://www.shipserv.com/tradenet/MTMLLink.asmx"
       when :PREPROD
       endpoint 'https://www.shipserv.com/pretradenet/MTMLLink.asmx'
       when :UATB
         endpoint "https://legacytest.shipserv.com/tradenetB/MTMLLink.asmx"
       when :TNCUATC
         endpoint "https://legacytest.shipserv.com/uatpre-tradenetC/MTMLLink.asmx"       
       when :TNCUATD
         endpoint "https://test.shipserv.com/tradenetD/MTMLLink.asmx"  
       when :TNCUATF
         endpoint "https://test.shipserv.com/tradenetF/MTMLLink.asmx"  
       when :TNCUATE
         endpoint "https://test.shipserv.com/tradenetE/MTMLLink.asmx"           
       when :TNCUATG
         endpoint "https://test.shipserv.com/tradenetG/MTMLLink.asmx" 
       when :TNCUATI
         endpoint "https://test.shipserv.com/tradenetI/MTMLLink.asmx"
       when :TNCUATH
         endpoint "https://test.shipserv.com/tradenetH/MTMLLink.asmx"
       when :TNCUATK
         endpoint "https://test.shipserv.com/tradenetK/MTMLLink.asmx"         
       when :TNCUATCHE
         endpoint "https://test.shipserv.com/tradenetCHE/MTMLLink.asmx"
       when :TNCUATJM
         endpoint "https://test.shipserv.com/tradenetJM/MTMLLink.asmx"                                                            
       when :TNCUAT2
         endpoint "https://test.shipserv.com/tradenet_UAT2/MTMLLink.asmx"
       when :UKDEV
         endpoint "https://dev.shipserv.com/tradenet/MTMLLink.asmx"
       when :DEVA 
         endpoint "https://dev.shipserv.com/tradenetA/MTMLLink.asmx"  
       when :DEVB 
         endpoint "https://dev.shipserv.com/tradenetB/MTMLLink.asmx" 
       when :DEVVB
         endpoint "http://devtradenet.myshipserv.com/tradenetB/MTMLLink.asmx"           
       else
       end
    end
  end
  
  #setter values
  def set_ssws_tnid(tnid)
    @@ssws_tnid=tnid
  end
  
  def set_ssws_user_id(user)
    @@ssws_user_id=user
  end
  
  def set_ssws_user_pwd(pwd)
    @@ssws_user_pwd=pwd
  end
  
  def set_ssws_profile(profile='STD')
    @@ssws_profile=profile
  end
  
  def set_source_ssws_file(data=File.read("#{ENV['HOME']}/source_path.txt"))
    if File.file?(data) then
      @ssws_src_file=File.read(data)
    else
      @ssws_src_file=data
    end
  end
  
  def set_dest_ssws_file(file)
    @ssws_dest_file=file
  end

#soap_actions  
  def display_soap_action
    puts @ssws_client.wsdl.soap_actions
  end  
 
  def forward_document(doc_id,supplier_tnid,doc_type='RFQ')
    match_buyer=11107
    body={
      "UserIntegrationDoc" => {
          "AppDetails" =>{
            "Name" => 'DirectWS',
            "Version"=> '1.0'
          },
          "IntegrationDetails" =>{
            "TradeNetID"=> 999999,
            "UserID"=>'autosuggest1',
            "UserPassword"=>'tit4tat',
            "IntegrationCode"=>'STD'
          },
          "DocumentDetails"=>{
            "DocumentType"=>doc_type,
            "DocumentID"=>doc_id,
            "ForwardToTradeNetID"=>supplier_tnid,
            #"DocumentSubject"=>"Testing only muna",
            "EncodingTypeCode"=>'UTF-8'
          }
        }        
      
    }
    response = @ssws_client.call(:forward_document, message: body)
    return response   
  end
  #Send document via SSWS
  def submit_file_to_ssws
    ssws_file=@ssws_src_file
    ssws_filename="ssws_direct"
    ssws_file_base64=Base64.encode64(ssws_file)
    file_id=Time.now.strftime('%y%m%d%H').to_i
    data_att = File.read('C:\Users\Public\Pictures\Sample Pictures\Chrysanthemum92201.jpg')
    encoded = Base64.encode64(data_att)
    
      att_img_file = File.open('C:\Users\Public\Pictures\Sample Pictures\Chrysanthemum92201.jpg', "rb") do |file|
        Base64.strict_encode64(file.read)
        end
        
    body={
        "UserIntegrationDoc" => {
          "AppDetails" =>{
            "Name" => 'DirectWS',
            "Version"=> '1.0'
          },
          "IntegrationDetails" =>{
            "TradeNetID"=> @@ssws_tnid,
            "UserID"=>@@ssws_user_id,
            "UserPassword"=>@@ssws_user_pwd,
            "IntegrationCode"=>@@ssws_profile.upcase
          },
          "DocumentDetails"=>{
            "DocumentType"=>'string',
            "ClientFileName"=>ssws_filename,
            "FileContentsAsBytes"=>ssws_file_base64,
            "EncodingTypeCode"=>'UTF-8'
          }
        }        
      }
    response = @ssws_client.call(:send_encoded_document, message: body)     
    puts '=====OUTPUT====='
    ul_response=response.to_hash[:send_encoded_document_response][:send_encoded_document_result]
    puts ul_response
    puts ul_response[:result_code]
    puts ul_response[:error_message] if ul_response[:result_code]='Err'
    #puts ul_response[:file_contents] #ICR
=begin
,
          "DocumentAttachments"=>{
            "DocumentAttachment"=>{
               "FileId"=> file_id,
               "FileName"=> "att-test.jpg",
               "FileContentsAsBytes"=>att_img_file
            }
          }           
=end    
    return response
  end

  def submit_file_to_ssws_customer_doc
    ssws_file=@ssws_src_file
    ssws_filename="ssws_direct"
    ssws_file_base64=Base64.encode64(ssws_file)    
    body={
      strTradeNetID: @@ssws_tnid,
      strUserID: @@ssws_user_id,
      strPassword: @@ssws_user_pwd,
      strIntegration: @@ssws_profile.upcase,
      strFileContents: ssws_file,
      byteFileContents: ssws_file_base64
      }
    response = @ssws_client.call(:send_customer_doc, message: body)     
    puts '=====OUTPUT====='
    ul_response=response.to_hash[:send_customer_doc_response][:send_customer_doc_result]
    puts ul_response
    return response
  end

  def submit_file_to_ssws_mtml_doc
    ssws_file=@ssws_src_file
    ssws_filename="ssws_direct"   
    body={
      strTradeNetID: @@ssws_tnid,
      strUserID: @@ssws_user_id,
      strPassword: @@ssws_user_pwd,
      strIntegration: @@ssws_profile.upcase,
      strMTML: ssws_file
      }
    response = @ssws_client.call(:send_mtml_doc, message: body)     
    puts '=====OUTPUT====='
    ul_response=response.to_hash[:send_mtml_doc_response][:send_mtml_doc_result]
    puts ul_response
    return response
  end

  def submit_file_to_ssws_send_doc
    ssws_file=@ssws_src_file
    ssws_filename="ssws_direct"
    ssws_file_base64=Base64.encode64(ssws_file)    
    body={
      strTradeNetID: @@ssws_tnid,
      strUserID: @@ssws_user_id,
      strPassword: @@ssws_user_pwd,
      strIntegration: @@ssws_profile.upcase,
      strFileContents: ssws_file,
      byteFileContents: ssws_file_base64,
      strClientFileName: "file.xml",
      strDocumentType: "string"
      }
    response = @ssws_client.call(:send_document, message: body)     
    puts '=====OUTPUT====='
    ul_response=response.to_hash[:send_document_response][:send_document_result]
    puts ul_response
    return response
  end  
  
  def download_file_from_ssws
      body={
        "UserIntegrationDoc" => {
          "AppDetails" =>{
            "Name" => 'DirectWS',
            "Version"=> '1.0'
          },
          "IntegrationDetails" =>{
            "TradeNetID"=>@@ssws_tnid,
            "UserID"=>@@ssws_user_id,
            "UserPassword"=>@@ssws_user_pwd,
            "IntegrationCode"=>@@ssws_profile.upcase,
            "InvoiceIntegrationCode"=>@@ssws_profile.upcase
          },
          "DocumentDetails"=>{
            "EncodingTypeCode"=>'UTF-8'
          }
        }                
      }
    response=@ssws_client.call(:get_encoded_document, message: body)      
    puts '=====OUTPUT====='
    dl_response=response.to_hash[:get_encoded_document_response][:get_encoded_document_result]
    puts dl_response
    puts dl_response[:result_code]
    puts dl_response[:document_type]
    @doc_type=dl_response[:document_type].to_s
    puts dl_response[:ack_id]
    @doc_id=dl_response[:ack_id].to_s
    puts dl_response[:error_message] if dl_response[:result_code]=='Err'
    acknowledge_document if dl_response[:result_code]=='Success'
    #puts dl_response[:server_file_name]
    #puts dl_response[:file_contents_as_bytes]
    dl_content=Base64.decode64(dl_response[:file_contents_as_bytes])
    #puts dl_content
    #server_file_name=dl_response[:server_file_name].match(/\d*_\d*_\d*_\d*/)
    server_file_name=Time.now.strftime('%y%m%d%H%M%S%L')
    #save downloaded file
    dl_file_path=@ssws_dest_dir+server_file_name.to_s+'.'+@@ssws_profile
    File.open(dl_file_path,'w+') do |f|
      f.puts dl_content
    end
    #save downloaded path
    File.open("#{ENV['HOME']}/dest_path.txt",'w+:UTF-8') do |f|
      f.print dl_file_path
    end
    #puts dl_response
    return dl_content
  end
  
  def download_file_from_ssws_get_doc
    body={
        strTradeNetID: @@ssws_tnid,
        strUserID: @@ssws_user_id,
        strPassword: @@ssws_user_pwd,
        strIntegration: @@ssws_profile.upcase
      }
    response=@ssws_client.call(:get_document, message: body)      
    puts '=====OUTPUT====='
    dl_response=response.to_hash[:get_document_response][:get_document_result][:file_contents]
    puts dl_response
    dl_response     
  end

  def download_file_from_ssws_mtml_doc
    body={
        strTradeNetID: @@ssws_tnid,
        strUserID: @@ssws_user_id,
        strPassword: @@ssws_user_pwd,
        strIntegration: @@ssws_profile.upcase
      }
    response=@ssws_client.call(:get_mtml_doc, message: body)      
    puts '=====OUTPUT====='
    dl_response=response.to_hash[:get_mtml_doc_response]
    puts dl_response
    puts dl_response[:get_mtml_doc_result]
    dl_response[:get_mtml_doc_result]         
  end

  def download_file_from_ssws_customer_doc
    body={
        strTradeNetID: @@ssws_tnid,
        strUserID: @@ssws_user_id,
        strPassword: @@ssws_user_pwd,
        strIntegration: @@ssws_profile.upcase
      }
    response=@ssws_client.call(:get_customer_doc, message: body)      
    puts '=====OUTPUT====='
    dl_response=response.to_hash[:get_customer_doc_response][:get_customer_doc_result]
    puts dl_response
    dl_response    
  end    
  #Other functions of ssws
  
  def get_ping
    response = @ssws_client.call(:ping, :xmlns )
    puts response
  end
  
  def get_count_remaining
    ssws_file_tnid=@@ssws_tnid
    ssws_file_user_id=@@ssws_user_id
    ssws_file_user_pwd=@@ssws_user_pwd
    ssws_file_profile=@@ssws_profile    
    body = {
        "strTradeNetID"=>ssws_file_tnid,
        "strUserID"=>ssws_file_user_id,
        "strPassword"=>ssws_file_user_pwd,
        "strIntegration"=>ssws_file_profile
      }
    response = @ssws_client.call(:get_count_remaining, message: body)
    puts '=====OUTPUT====='
    result = response.to_hash[:get_count_remaining_response]
    puts result[:get_count_remaining_result]
    return response
  end
  
  def get_integration_codes(data='NIL')
    response=@ssws_client.call(:get_integration_codes)
    result=response[:get_integration_codes_response][:get_integration_codes_result][:string]
    if data=='NIL' then
      puts result
    else
      puts result.include?(data.upcase)
    end
  end
  
  def acknowledge_document
    ssws_file_tnid=@@ssws_tnid
    ssws_file_user_id=@@ssws_user_id
    ssws_file_user_pwd=@@ssws_user_pwd
    ssws_file_profile=@@ssws_profile
    ssws_doc_type=@doc_type
    ssws_doc_id=@doc_id   
    body= {
        "strTradeNetID"=>ssws_file_tnid,
        "strUserID"=>ssws_file_user_id,
        "strPassword"=>ssws_file_user_pwd,
        "strIntegration"=>ssws_file_profile,
        "strDocType" => ssws_doc_type,
        "strAckID" => ssws_doc_id          
      }
      response=@ssws_client.call(:acknowledge_document, message: body)
      puts response
     #result=response.to_hash[:acknowledge_document_response][:acknowledge_document_result]
     #puts result[:result_code]
  end
  
end