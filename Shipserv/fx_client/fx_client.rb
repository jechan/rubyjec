require 'win32ole'

class FxClient
  attr_reader :fx_tnid, :fx_user_id, :fx_password, :fx_profile, :fx_env, :fx_server
  
  def set_fx_environment(env)
    case env.upcase.to_sym
    when :DEV
      @fx_env='http://dev.shipserv.com/SSMTML'
    when :UAT1
      ''
    when :UAT2
      ''
    when :LIVE
      ''
    when :TN2DEV
      ''
    when :TNCDEVH
      'http://dev.shipserv.com/tradenetH'      
    else
      raise "The Environment is not defined"
    end
  end
  
  #setter values
  def set_fx_server(server)
    @fx_server=server
  end
  
  def set_fx_tnid(tnid)
    @fx_tnid=tnid
  end
  
  def set_fx_user_id(user)
    @fx_user_id=user
  end
  
  def set_fx_password(pwd)
    @fx_password=pwd
  end
  
  def set_fx_profile(profile)
    @fx_profile=profile
  end
  
  
end
