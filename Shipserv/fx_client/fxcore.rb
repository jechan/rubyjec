require 'win32ole'
require 'win32/service'
require 'io/console'
require 'au3'
require_relative 'fxtab/fx_log.rb'
#Dir.glob(File.expand_path(Dir.pwd)+'/fxtab/*.rb') {|rf| require rf}

class Fxcore
  include FxLog, Win32
  attr_reader :window, :fx_hwnd, :initialize_log, :log_msg, :env, :title
  attr_reader :file_upload, :file_download, :fx_tnid
  #attr_reader :lb_log, :edit_log, :btn_clear
  
  def initialize
    @title="MTMLLink FX Client"
    @env='http://dev.shipserv.com/SSMTML'
    @profile='STD'
    @fx_dir="#{ENV['ProgramFiles']}/Shipserv/MTMLlink-FX/Service"
  end
  
  def set_fx_environment(en)
    @env=case en.upcase.to_sym
      when :DEV
        'https://dev.shipserv.com/SSMTML'
      when :UAT1
        'https://test.shipserv.com/SSMTML'
      when :UAT2
        'https://test2.shipserv.com/ssmtml'
      when :UATJM
        'https://test.shipserv.com/tradenetJM'
      when :UATSTAGE
        'https://test.shipserv.com/tradenetSTAGE'                    
      when :LIVE
        'https://www.shipserv.com/SSMTML'
      when :TNCDEV
        'http://dev.shipserv.com/tradenet'
      when :TNCDEVH
        'http://dev.shipserv.com/tradenetH'
      when :TNCDEVE
        'http://dev.shipserv.com/tradenetE'
      when :TNCDEVB
        'http://dev.shipserv.com/tradenetB'
      when :TNCDEVR
        'http://dev.shipserv.com/tradenetR'                          
      when :TNCUAT
        'http://test.shipserv.com/tradenet'
      when :TNCUATB
        'http://test.shipserv.com/tradenetB'
      when :TNCUATF
        'http://test.shipserv.com/tradenetF'
      when :TNCUATG
        'http://test.shipserv.com/tradenetG'       
      when :TNCUATH
        'http://test.shipserv.com/tradenetH'  
      when :TNCUATI
        'http://test.shipserv.com/tradenetI'                          
      when :TNCLIVE
        'http://www.shipserv.com/tradenet'
      when :UKDEV
        'http://ukdev.shipserv.com/tradenet'
      when :CUSTOM
        'http://172.30.2.200/SSMTML'
      else
        raise "The Environment is not defined"
      end
  end
  
  def open_fx_client
    if not AutoItX3::Window.exists?(@title) then
      AutoItX3.run("#{ENV['ProgramFiles']}/ShipServ/MTMLlink-FX/Client/MTMLLinkFXClient.exe") 
      sleep(5)
    end
    AutoItX3::Window.wait(@title)
    @window=AutoItX3::Window.new(@title)
    @window.activate if not @window.active?
    initialize_controls_purchasing if @btn_exchange_now.nil?
    return @window.exists?
  end
  
  def access_fx_client
    @window=AutoItX3::Window.new(@title)
    @window.activate if @window.exists? and not @window.active?
    initialize_controls_purchasing if @btn_exchange_now.nil?
  end
  
  def initialize_controls_purchasing
    #buttons
    @btn_exchange_now=AutoItX3::Control.new(@title,"","Exchange Now!")
    @btn_ping=AutoItX3::Control.new(@title,"","Ping Server")
    @btn_edit=AutoItX3::Control.new(@title,"","WindowsForms10.BUTTON.app.0.378734a7")
    @btn_save=AutoItX3::Control.new(@title,"","WindowsForms10.BUTTON.app.0.378734a2")
    
    #Server settings
    @edit_server=AutoItX3::Control.new(@title,"","Edit2")
    @cb_server=AutoItX3::ComboBox.new(@title,"","WindowsForms10.COMBOBOX.app.0.378734a2")
    @edit_tnid=AutoItX3::Edit.new(@title,"","Edit1")
    @cb_tnid=AutoItX3::ComboBox.new(@title,"","WindowsForms10.COMBOBOX.app.0.378734a1")
    
    #TradeNet Account Settings
    @edit_server_url=AutoItX3::Edit.new(@title,"","WindowsForms10.EDIT.app.0.378734a8")
    @edit_user_id=AutoItX3::Edit.new(@title,"","WindowsForms10.EDIT.app.0.378734a7")
    @edit_password=AutoItX3::Edit.new(@title,"","WindowsForms10.EDIT.app.0.378734a6")
    
    #Folder Settings
    @edit_outbox=AutoItX3::Edit.new(@title,"","WindowsForms10.EDIT.app.0.378734a13")
    @edit_inbox=AutoItX3::Edit.new(@title,"","WindowsForms10.EDIT.app.0.378734a14")
    @edit_icr=AutoItX3::Edit.new(@title,"","WindowsForms10.EDIT.app.0.378734a8")
    
    @cb_integration=AutoItX3::ComboBox.new(@title,"","WindowsForms10.COMBOBOX.app.0.378734a6")
    
    #fx_tab
    @tab=AutoItX3::TabBook.new(@title,"","WindowsForms10.SysTabControl32.app.0.378734a1")
  end
    
  def exchange_now
    @window.activate
    sleep(5)
    @btn_exchange_now.click if @btn_exchange_now.enabled?
    navigate_fx_tab('log')
    wait_for_force_interchange
  end
  
  def set_server_name(svr="SCRIPTBOX")
    @window.activate
    #sleep(1)
    if @edit_server.text != svr then
      @edit_server.click
      @edit_server.send_keys("{DEL}")
      @edit_server.send_keys(svr+"{DOWN}")
      sleep(2)
    end
  end
  
  def set_tradenet_account(tnid='10414', profile='STD')
    @window.activate
    @fx_tnid=tnid
    #sleep(1)
    if @edit_tnid.text != tnid then
      @edit_tnid.click
      @edit_tnid.send_keys("{DEL}")
      @edit_tnid.send_keys(tnid+"{DOWN}")
      while not @edit_server_url.text=~/http.*/ do
        sleep(1)
      end
      #log=1
    end
    
    if @edit_server_url.text != @env or @cb_integration.text != profile.upcase or @cb_integration.text.nil? then
      edit_settings
      @edit_server_url.click
      if @edit_server_url.text != @env then
        @edit_server_url.text=@env
      end
      
      if @cb_integration.text.upcase != profile.upcase then
        @cb_integration.click
        @cb_integration.send_keys("{DEL}")
        sleep(1)
        @cb_integration.send_keys(profile.upcase)
        @cb_integration.send_keys("{DOWN}")
        sleep(1)
      end
      save_settings
      ping_server
      navigate_fx_tab('log')
      clear_log
      navigate_fx_tab('tradenet')  
    end
    
  end
  
  def ping_server
    @window.activate
    sleep(1)
    if @btn_ping.visible? then
      ping_title="MTMLLinkFXClient"
      @btn_ping.click
      AutoItX3::Window.wait(ping_title)
      raise 'Server is down' if AutoItX3::Control.new(ping_title,"","Static1").text.to_s != "Server Ping Result : OK"
      AutoItX3::Control.new(ping_title,"","OK").click          
    else
      return 'Ping is not available' 
    end
    sleep(1)  
  end
  
  def edit_settings
    @window.activate
    if @btn_edit.enabled? and @btn_edit.visible? then
      @btn_edit.click
      edit_title="Enter Access Code"
      AutoItX3::Window.wait(edit_title)
      edit_access_code=AutoItX3::Edit.new(edit_title,"","WindowsForms10.EDIT.app.0.378734a1")
      edit_access_code.text='SHIPSERV'
      AutoItX3::Control.new(edit_title,"","WindowsForms10.BUTTON.app.0.378734a1").click
      until @edit_server.enabled? and @cb_integration.enabled? do
        sleep(1)
      end
    else
      'Edit is not available'
    end   
  end
  
  def save_settings
    @window.activate
    sleep(1)
    @btn_save.click
    sleep(1)
    until not @btn_save.enabled? do
      sleep(1)
    end
  end
  
  def navigate_fx_tab(tab='TradeNet')
    case tab.upcase.to_sym
    when :TRADENET
      mv_tabno='1'
    when :PURCHASING
      mv_tabno='2'
    when :LOGISTICS
      mv_tabno='3'
    when :LOG
      mv_tabno='4'
    end
    cur_tabno=@tab.current.to_i
    move=(mv_tabno.to_i-cur_tabno).abs
    if move > 0 then
      @window.activate
      if mv_tabno.to_i > cur_tabno.to_i then
        move.to_i.times {@tab.right}
      else
        move.to_i.times {@tab.left}
      end
      case mv_tabno.to_i
      when 4
        initialize_controls_logs if @initialize_log != 1
        sleep(1)
        @log_msg=@edit_log.text
      end     
    end
  end
  
  def restart_service
    #restart service
    service_name='MTMLLink-FX'
    Service.stop(service_name) if Service.status(service_name).current_state=='running'
    until Service.status(service_name).current_state=='stopped' do
      sleep(1)
      puts Service.status(service_name).current_state
    end
    Service.start(service_name) if Service.status(service_name).current_state == 'stopped'

    until Service.status(service_name).current_state=='running' do
      sleep(1)
      puts Service.status(service_name).current_state
    end
    sleep(3)
  end
  
end

