require 'win32ole'
require 'au3'

module FxLog
  def initialize_controls_logs
    @lb_log=AutoItX3::ListBox.new(@title,"","WindowsForms10.LISTBOX.app.0.378734a1")
    #@edit_log=AutoItX3::Edit.new(@title,"","WindowsForms10.EDIT.app.0.378734a32") if @window.title=~/5.2.3/
    @edit_log=AutoItX3::Edit.new(@title,"","WindowsForms10.EDIT.app.0.378734a33") #if @window.title=~/5.6/
    @btn_clear=AutoItX3::Edit.new(@title,"","WindowsForms10.BUTTON.app.0.378734a37")
    @initialize_log=1
  end
  
  def clear_log
    sleep(1)
    @window.activate
    @btn_clear.click
    @log_msg=@edit_log.text
    until @log_msg=~/Log Truncated/ do
      sleep(1)
      @log_msg=@edit_log.text
    end
  end
  
  def wait_for_file_upload_download
    i=0
    until (@log_msg =~ /Uploaded->\(OK\)/)  or (@log_msg =~ /Downloaded/) or (@log_msg =~ /Ack/)  do
      @log_msg=@edit_log.text
      #puts @log_msg
      i+=1
      break if i==10000
    end     
    sleep(12)#wait time for fx client since it is still processing
    log_file="#{@fx_dir}/SSHLinkLog.#{@fx_tnid.gsub('-','.')}"
    puts log_file
    ul=[]
    dl=[]
    File.open(log_file, 'r') do |f|
      f.each do |l| 
        dl << l if l=~/Downloaded/
        ul << l if l=~/Uploaded/
      end
    end
    puts "UPLOAD array"
    puts ul.inspect
    puts "DOWNLOAD array"
    puts dl.inspect
    puts dl.length
    #get filename
    if @log_msg=~/Uploaded->\(OK\)/ then
      sleep(5)
      upload=@log_msg.match(/Uploaded->\(OK\) (.*)/).to_s
      file_upload=ul.last.match(/\w*\.\w{3}/).to_s
    elsif @log_msg=~/Downloaded/ or @log_msg=~/Ack/ then
      sleep(10)
      download=@log_msg.match(/Downloaded-\((.*)\) ./).to_s
      file_download=dl.last.match(/\w*\.\w{3}\.?\w{0,3}/).to_s
    end
  end
  
  def wait_for_force_interchange
    i=0
    until log_msg=~/Forced Interchange/ do
      log_msg=@edit_log.text
      i+=1
      break if i==200
    end
  end
  sleep(1)
end