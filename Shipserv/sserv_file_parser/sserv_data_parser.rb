require 'rubygems'
require 'nokogiri'

class SservDataParser
  
  attr_reader :src_file, :file_profile, :file_data
  
    #Setters
  def ship_parse(file, profile='STD')
    @fle_data=case profile.upcase.to_sym
    when :STD
      'STD Parser to return hasd or yml'
    when :APMM
    else
      raise 'profile not yet defined'
    end
  end      
    
    
    #This will get the full path of the file that we need to check
  def set_source_file(template_dir)
    @src_file=File.read(template_dir+'download_filepath.txt')
    raise 'File not valid' unless File.file?(@src_file)
    rescue => e
      puts 'set_source_file --> ' + e
      exit
  end
  
  def set_file_profile(profile)
    @file_profile=profile
    case profile.to_sym
    when :STD
      @file_data=Nokogiri::XML(File.read(@src_file)).at_xpath('/')
    else
      raise 'Profile not yet supported for this library'
    end
    rescue => e
      puts 'set file profile--> '+ e
      exit
  end

  
  #Data Parsers
  #Comments
  def header_comments
    case @file_profile.to_sym
    when :STD
      hc=@file_data.at_xpath('//Comments[@Qualifier="PUR"]/Value')
      if hc == nil then
        puts 'no data found'
      else
        puts hc.text
        return hc.text
      end
    else
      raise 'Profile not yet supported for header_comments'
    end
    rescue => e
      puts 'header_comments error'
      puts e
      exit    
  end
  
  
  #Monetary Amounts
  def freight_cost
    case @file_profile.to_sym
    when :STD
      fc=@file_data.at_xpath('//MonetaryAmount[@Qualifier="_64"]')
      puts fc['Value']
      return fc['Value']
    else
      raise 'Profile not yet supported for freight'
    end
    rescue => e
      puts 'freight_cost error --> ' + e
      exit    
  end
  
  
  def packing_cost
    case @file_profile.to_sym
    when :STD
      pc=@file_data.at_xpath('//MonetaryAmount[@Qualifier="_106"]')
      puts pc['Value']
      return pc['Value']
    else
      raise 'Profile not yet supported for packing cost'
    end
    rescue => e
      puts 'packing_cost error'
      puts e
      exit        
  end

end


if __FILE__ == $0
  require 'rubygems'
  require "robot_remote_server"
  #RobotRemoteServer.new(SservFileParser.new, host='localhost', port='8282')
end
