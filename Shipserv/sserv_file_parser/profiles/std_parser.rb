require 'nokogiri'
require 'yaml'

module StdParser
  def getdata(file)
    std_data=File.read(file)
    std_doc=Nokogiri::XML(std_data)
    
    purchasing_data=%w{
      documentType
      controlRefNo
      sender
      recipient
      deliveryAddress
      billingAddress
      lineItemCount
    }
    
    #header interchange
    
    #header document
    
    #Datetime periods
    
    #comments
    
    #references
    
    #line items         
  end
end