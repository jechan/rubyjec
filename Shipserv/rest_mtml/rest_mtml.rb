require 'rubygems'
require 'net/http'
require 'nokogiri'
require 'fileutils'

class RestMtml
  
  attr_reader :rest_tnid, :rest_env, :rest_src_file, :rest_dest_dir, :rest_dest_file
  
  def initialize
    #src_path=File.read("#{ENV['HOME']}/source_path.txt")
    #@rest_src_file=File.read(src_path)
    @rest_dest_dir="#{ENV['HOME']}/SS_FILES_DOWNLOADED/"
  end
  
  #Setter methods
  def set_rest_environment(env)
    case env.upcase.to_sym
    when :DEV
      @res_env='http://xentomcat2.myshipserv.com:8780'
    when :UAT1
      @res_env='http://localhost:1541' #THIS must be pre set up in putty
    when :UAT2
      @res_env='http://localhost:1537' #This must be pre set up in putty
    when :LIVE
      @res_env='http://localhost:1543' #This must be pre set up in putty
    else
      raise 'The environment is not defined. Please check'
    end
  end
  
  def set_rest_tnid(tnid)
    @rest_tnid=tnid
  end
  
  def set_source_mtml_file(file)
     if File.file?(file) then
       @rest_src_file=File.read(file)
     else
       @rest_src_file=file
     end
  end
  
  def set_dest_mtml_file(file)
    @rest_dest_file=file
  end

  ##Application Methods
  def get_count_remaining
    uri=URI(@res_env+'/mtmlws/docs/'+@rest_tnid+'/inbox/count')
    response=Net::HTTP.get(uri)
    response_mtml=Nokogiri::XML(response).at_xpath('//MtmlResponse')
    raise 'Error get count' unless response_mtml['Success'] == 'true'
    puts "You have #{response_mtml['DocCount']} files to get"
    return response_mtml['DocCount'].to_s
  end
  
  def acknowledge_receipt_file(control_reference)
    uri=URI(@res_env+'/mtmlws/docs/'+@rest_tnid+'/inbox/'+control_reference)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Delete.new(uri.request_uri)
    response=http.request(request)
    response_mtml=Nokogiri::XML(response.body).at_xpath('//MtmlResponse')
    return puts 'Success' if response_mtml['Success']=='true'
  end
  
  def submit_ssmtml_file_to_server
    uri=URI(@res_env+'/mtmlws/docs/'+@rest_tnid+'/outbox')
    http = Net::HTTP.new(uri.host, uri.port)
    request=Net::HTTP::Post.new(uri.request_uri)
    request.body = @rest_src_file
    response=http.request(request)
    response_mtml=Nokogiri::XML(response.body).at_xpath('//MtmlResponse')
    puts response_mtml 
    #raise 'File submission failed' unless response_mtml['Success']=='true'
    #puts "File was submitted: #{response_mtml['ControlReference']}" 
    return response_mtml
  end
  
  def download_ssmtml_file_from_server
    raise 'No file to download' if get_count_remaining=='0'
    uri = URI(@res_env+'/mtmlws/docs/'+@rest_tnid+'/inbox/next')
    response = Net::HTTP.get(uri)
    response_mtml = Nokogiri::XML(response).at_xpath('//MtmlResponse')
    puts response_mtml
    raise 'download failed' unless response_mtml['Success']=='true'
    
    control_reference=response_mtml['ControlReference']
    puts "File Downloaded: #{control_reference}"
    puts 'Acknowledging File'
    acknowledge_receipt_file(control_reference)
        
    #Get MTML file and place in a soft copy
    download_dir="#{ENV['HOME']}/SS_FILES_DOWNLOADED/"
    mtml_data =  response_mtml.at_xpath('//Document')
    puts mtml_data.content
    control_reference.gsub!(':','-')
    @rest_dest_file=@rest_dest_file||download_dir+control_reference+'.xml'
    mtml_file = File.open(@rest_dest_file,'w+:UTF-8')
    mtml_file.puts mtml_data.content
    mtml_file.close
    
    #destination path of downloaded file
    download_file=File.open("#{ENV['HOME']}/dest_path.txt",'w')
    download_file.print @rest_dest_file
    download_file.close
    return mtml_data.content
  end
  
end

=begin
if __FILE__ == $0
  require 'rubygems'
  require "robot_remote_server"
  #RobotRemoteServer.new(RestMtml.new, host='localhost', port='8281')
end
=end

                          