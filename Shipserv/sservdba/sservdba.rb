#require 'rubygems'
require 'oci8'
require 'net/ssh/gateway'

class Sservdba
  
  attr_reader :database, :environment, :db_conditions, :db_client, :single_last_data, :gateway, :portal
  
 
  def set_db_environment(db)
    case db.upcase.to_sym
    when :DEV
      environment = { :db_usr => 'sservdba', :db_pw => 'devmla', :db_host => '172.30.1.80', :db_port => '1521', :db_sid => 'ssdev08'}       
    when :UAT1  
      environment = { :db_usr => 'sservdba', :db_pw => 'robin', :db_host => 'localhost', :db_port => '1530', :db_sid => 'SSTEST05'}
      terminal={:host=>'sweep.shipserv.com', :user=>'jechan',:pwd=>'chenge.', :db_server => 'testdb.myshipserv.com'}
    when :UKDEV
      puts 'open putty for this in jonah as of the moment SSERVDBA'      
      environment = { :db_usr => 'sservdba', :db_pw => 'jason', :db_host => 'localhost', :db_port => '1601', :db_sid => 'SSUK06'}
      terminal={:host=>'helen.shipserv.com', :user=>'jechan',:pwd=>'chenge.56', :db_server => 'joseph.myshipserv.com'}
    when :LIVE
      puts 'open putty for this in sweep as of the moment SSERVDBA'      
      environment = { :db_usr => 'readonly', :db_pw => 'iship', :db_host => 'localhost', :db_port => '1531', :db_sid => 'SSPROD04'}
      terminal={:host=>'sweep.shipserv.com', :user=>'jechan',:pwd=>'chenge.', :db_server => 'livedb.myshipserv.com'}
    else
      raise 'Not valid db name'    
    end
    
    #if db.upcase != 'DEV'
     #@gateway = Net::SSH::Gateway.new(terminal[:host], terminal[:user], :password => terminal[:pwd])
     #@portal = @gateway.open(terminal[:db_server], '1521', environment[:db_port])
    #end

    #tnsnames="(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = #{environment[:db_host]})(PORT = #{environment[:db_port]})) (CONNECT_DATA = (SID = #{environment[:db_sid]})))"
    @db_client=OCI8.new(environment[:db_usr], environment[:db_pw],"//#{environment[:db_host]}:#{environment[:db_port]}/#{environment[:db_sid]}")
    puts @db_client
  end
  
  def set_db_condition(column,value)
    puts Time.now
    @db_conditions=%Q{where #{column} = '#{value}'}
  end
  
  def query_record(table, column="*")
    sql_query=%Q{select #{column} from #{table} #@db_conditions}
    puts sql_query
    @single_last_data=[]
    #print column + " is " 
    @db_client.exec(sql_query) do |record|
      @single_last_data << record
    end
    #@gateway.shutdown! if not @gateway.nil?
    puts Time.now
    puts @single_last_data[0][0].to_s
    @single_last_data[0][0].to_s
  end
  
  def update_record(table, column, value)
    sql_update=%Q{begin update #{table} set #{column}='#{value}'; commit; end;}
    puts sql_update
    @db_client.exec(sql_update)
    #@gateway.shutdown! if not @gateway.nil?
  end
#=begin  
  def select(query)
    @single_last_data=[]
    @db_client.exec(query) do |record|
      @single_last_data << record
    end
    #@gateway.shutdown! if not @gateway.nil?    
    if @single_last_data[0].nil? then
      puts 'no rows returned'
      'DBNOROW'  
    else
       puts @single_last_data[0][0].to_s
       @single_last_data[0][0].to_s
    end

  end
#=end


  def select_multi(query)
    @single_last_data=[]
    @db_client.exec(query) do |record|
      @single_last_data << record
    end
    #@gateway.shutdown! if not @gateway.nil?    
    @single_last_data[0]
  end

  def execute_sql(sql)
    sql_stm=%Q{begin #{sql} commit; end;}
    puts sql_stm
    @db_client.exec(sql_stm)
    #@gateway.shutdown! if not @gateway.nil?
  end
  def shutdown
    puts 'SHUTTING DOWN DATABASE'
    @gateway.shutdown! if not @gateway.nil?
  end  
end

