require 'rubygems'
require 'C:\Shipserv Backup\RUBY WORKSHOP\Shipserv\sservdba\sservdba.rb'
require 'rspec'

describe Sservdba, "Access to different database" do
	it "can connect to the DEV environment and query a data" do
		sservdba=Sservdba.new
		sservdba.set_db_environment('DEV')
		sservdba.set_db_condition('CCF_BRANCH_CODE','10414')
		sservdba.query_record('CUSTOMER_CONFIG','CCF_MOVE_NEG_COSTS_TO_DISC')	
	end
	
	it "can connect to the DEV environment and update a data" do
		sservdba=Sservdba.new
		sservdba.set_db_environment('DEV')
		sservdba.set_db_condition('CCF_BRANCH_CODE','10414')
		sservdba.update_record('CUSTOMER_CONFIG','CCF_MOVE_NEG_COSTS_TO_DISC','0')	
	end
end