require 'rubygems'
require 'nokogiri'
require 'fileutils'
require 'pp'

module Std
  
  @template_folder="#{ENV['HOME']}/SSTEMPLATES/"
  @sub='std'
  
  def self.create_file(hdr, li, doc_type)
    @header=hdr
    @line_items=li
    @template=File.read(@template_folder+@sub+'/'+doc_type.upcase.to_s+'.SRC')
    case doc_type.upcase.to_sym
    when :RFQ
      gen_data_mtml15('RequestForQuote')
    when :QOT
      qot_data
    when :ORD
      ord_data
    when :ORP
      orp_data
    when :POC
      poc_data
    when :INV
      inv_data
    end
  end
      
  def self.gen_data_mtml15(type)
    #string replacements first
    @template.gsub!('v_unique_ref', @sub+@header[:Reference])
    
    #parse the type of file
    parser=Nokogiri::XML(@template)

    #set interchange values  
    interchange=parser.at_xpath('//Interchange')
    interchange[:Sender] = @header[:Buyer]
    interchange[:Recipient]= @header[:Supplier]
        
    #set rfq headers
    header=parser.at_xpath("//#{type}")
    header[:CurrencyCode]=@header[:CurrencyCode] if not @header[:CurrencyCode].nil?
    header[:LineItemCount]=@header[:LineItemCount] if not @header[:LineItemCount].nil?
    header[:DeliveryTermsCode]=@header[:DeliveryTermsCode] if not @header[:DeliveryTermsCode].nil?
    header[:TransportModeCode]=@header[:TransportModeCode] if not @header[:TransportModeCode].nil?
    header[:Priority]=@header[:Priority] if not @header[:Priority].nil?
    header[:TaxStatus]=@header[:TaxStatus] if not @header[:TaxStatus].nil?
    header[:FunctionCode]=@header[:FunctionCode] if not @header[:FunctionCode].nil?
    header=update_attribute(header)
    
    #set comments
    comments=parser.xpath("//#{type}/Comments")
    comments.each do |c|
      case c[:Qualifier].to_sym
      when :PUR
        c.children.each {|v| v.content=@header[:PUR] if v.element? and not @header[:PUR].nil?}
      when :ZTC
        c.children.each {|v| v.content=@header[:ZTC] if v.element? and not @header[:ZTC].nil?}
      when :ZTP
        c.children.each {|v| v.content=@header[:ZTP] if v.element? and not @header[:ZTP].nil?}
      when :ZAT
        c.children.each {|v| v.content=@header[:ZAT] if v.element? and not @header[:ZAT].nil?}
      end
    end
    comments=update_element_value_node(comments)
    #References
    reference=parser.xpath("//#{type}/Reference")
    #Party Contacts
    reference.each do |ref|
      case ref[:Qualifier].to_sym
      when :AGI
        ref[:ReferenceNumber]=@header[:AGI] if not @header[:AGI].nil?
      when :UC
        ref[:ReferenceNumber]=@header[:UC] if not @header[:UC].nil?
      when :ACW
        ref[:ReferenceNumber]=@header[:ACW] if not @header[:ACW].nil?
      when :ON
        ref[:ReferenceNumber]=@header[:ON] if not @header[:ON].nil?
      when :AAG
        ref[:ReferenceNumber]=@header[:AAG] if not @header[:AAG].nil?
      end
      ref=update_attribute(ref)
    end
    partycontact=parser.xpath("//#{type}/Party")
    #MonetaryAmounts
    monetaryamount=parser.xpath("//#{type}/MonetaryAmount")
    monetaryamount.each do |mon|
      case mon[:Qualifier].to_sym
      when :_79
        mon[:Value]=@header[:_79] if not @header[:_79].nil? 
      when :_204
        mon[:Value]=@header[:_204] if not @header[:_204].nil? 
      when :_106
        mon[:Value]=@header[:_106] if not @header[:_106].nil?
      when :_64
        mon[:Value]=@header[:_64] if not @header[:_64].nil?
      when :_259
        mon[:Value]=@header[:_259] if not @header[:_259].nil?
      end
      mon=update_attribute(mon)
    end
    #Line Items
    lineitem=parser.xpath("//#{type}/LineItem")
    return parser
  end
  
  def self.inv_data
    #string replacement first
    @template.gsub!('v_unique_ref', @sub+@header[:Reference])
    parser=Nokogiri::XML(@template)
    #MTML 2.0
    interchange=parser.at_xpath('//Interchange')
    interchange[:Sender] = @header[:Supplier]
    interchange[:Recipient]= @header[:Buyer]  
    return parser
  end
  
  def self.update_attribute(node)
    node.each do |attr, value|
      if value=='NIL' then
        node[attr.to_sym]="" 
      elsif
        value=='DEL' then
        node.remove_attribute(attr.to_s)
      end
    end
    node
  end
  
  def self.update_element_value_node(nodeset)
    nodeset.each do |pnode|
      pnode.children.each do |cnode|
        pnode.remove if cnode.content=="DEL" and cnode.element?
        cnode.content="" if cnode.content=="NIL" and cnode.element?
      end
    end
    nodeset
  end

end