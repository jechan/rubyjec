#require MTML
#require SSWS
#require TN2
#require FXClient

class Ssinterface
  attr_accessor :source_file, :dest_file
  
  #get absolute path of files used by MTML,SSWS,TN2
  @source_file=File.read("#{ENV['HOME']}/source_path.txt")
  @dest_file=File.read("#{ENV['HOME']}/dest_path.txt")
   
  def get_fx_in_folder
    #will return the IN folder of fx client
  end
  
  def get_fx_out_folder
    #will return the OUT folder
  end
  
  def submit_source_file(tradenet)
    #will submit @source_file to tradenet
    case tradenet.to_sym
    when :MTML
    when :SSWS
    when :TN2
    when :FXClient
      #copy the source file to the in folder
    end    
  end
  
  def submit_to_shipdata_parser(tradenet)
    #will submit @dest_file to data parser
    case tradenet.to_sym
    when :MTML
    when :SSWS
    when :TN2
    when :FXClient
      #get the downloaded file from the fxclient
    end        
  end
  
end