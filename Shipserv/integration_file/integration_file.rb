require 'rubygems'
require 'fileutils'
require 'nokogiri'

class IntegrationFile
  
  attr_reader :doc_type, :doc_profile, :doc_buyer_id, :doc_supplier_id, :doc_control_reference, :doc_freight_cost, :doc_packing_cost
  
  def check_valid_template(file)
    raise 'Invalid Template' if not File.file?(file)
    puts 'Template File is valid'
  end
  
 #Setter Values 
  def generate_control_reference
    sleep(1)
    @doc_control_reference=Time.now.strftime('%y%m%d%H%M%S')
    puts @doc_control_reference 
  end
  
  def set_doc_buyer_id(buyer)
    @doc_buyer_id=buyer
  end
  
  def set_doc_supplier_id(supplier)
    @doc_supplier_id=supplier
  end
  
  def set_doc_freight_cost(fc)
    @doc_freight_cost=fc
  end
  
  def set_doc_packing_cost(pc)
    @doc_packing_cost=pc
  end
  
 #Integration File Creation
  def save_ssmtml_order(file)
    template_file=File.read(file)
    xml_parse=Nokogiri::XML(template_file)
    root = xml_parse.xpath('/')
    
    #INTERCHANGE HEADERS
    mtml=root.at_xpath('//MTML')
    interchange=mtml.at_xpath('//Interchange')
    raise 'no interchange' unless interchange.element?
    interchange['Sender']=@doc_buyer_id
    interchange['Recipient']=@doc_supplier_id
    interchange['ControlReference']='C.'+@doc_control_reference
    
    #ORDER HEADERS
    order=interchange.at_xpath('//Order')
    raise 'Not valid PO MTML' unless order.element?
    order['MessageReferenceNumber']='MRN.'+@doc_control_reference
    order['MessageNumber']='MN.'+@doc_control_reference
    
    
    #References
    client_reference = order.at_xpath('//Reference[@Qualifier="UC"]')
    client_reference['ReferenceNumber'] = 'Ord Ref '+@doc_control_reference
    
    #MONETARY AMOUNTS
    freight_cost=order.at_xpath('//MonetaryAmount[@Qualifier="_64"]')
    raise 'No Freight Node' unless freight_cost.element?
    freight_cost['Value']=@doc_freight_cost
    
    packing_cost=order.at_xpath('//MonetaryAmount[@Qualifier="_106"]')
    raise 'No packing cost node' unless packing_cost.element?
    packing_cost['Value']=@doc_packing_cost
    
    ######this code is to be removed..written for S4962 to compute for Total Cost
    line_item_total=order.at_xpath('//MonetaryAmount[@Qualifier="_79"]')
    line_item_total['Value']='150'
    
    header_discount=order.at_xpath('//MonetaryAmount[@Qualifier="_204"]')
    header_discount['Value']='25'
    
    
    cost_total=order.at_xpath('//MonetaryAmount[@Qualifier="_259"]')
    cost_total['Value']=((line_item_total['Value'].to_i)-(header_discount['Value'].to_i)+(freight_cost['Value'].to_i)+(packing_cost['Value'].to_i)).to_s
    ######
    
    #Save file in File Directory Template
    Dir.chdir(File.dirname(file))
    ssmtml_filename=@doc_control_reference+'_order.xml'
    ssmtml_order=File.open(ssmtml_filename, "w:UTF-8") 
    ssmtml_order.puts(root)
    ssmtml_order.close
    
    source_filepath=Dir.pwd+'/'+ssmtml_filename
    
    if File.file?(ssmtml_filename) then
        file=File.open('source_filepath.txt', 'w')
        file.print source_filepath
        file.close
    else
      raise RuntimeError, 'File Not Generated'
    end
    
    return source_filepath    
  end
end

if __FILE__ == $0
  require 'rubygems'
  require "robot_remote_server"
  RobotRemoteServer.new(IntegrationFile.new, host='localhost', port='8280')
end



