require 'rubygems'
require 'nokogiri'
require 'fileutils'
require 'csv'
Dir.glob(Dir.pwd+'/profile/*.rb') {|rf| require rf}

class FileGenerator
  
  #full
  include Std
  
  #buyer
  
  #supplier
  attr_reader :fg_profile, :fg_doc_type, :fg_data_header, :fg_data_line_items
  attr_reader :buyer_tnid, :supplier_tnid
  
  def set_buyer_tnid(byb='10414')
    @buyer_tnid=byb
  end
  
  def set_supplier_tnid(spb='59758')
    @supplier_tnid=spb
  end
  
  def set_filegen_profile(profile)
    @fg_profile=profile
  end
  
  def set_filegen_data(header='none',line_item='one')
    File.file?(header) ? content = CSV.read(header) : content=['N','D']
    #Convert header and value into single multidimensional array
    a_content = []
    a_content_header = content[0]
    a_content_value = content[1]
    i=0
    while i < a_content_header.length do
      a_content[i]=[]
      a_content[i] << a_content_header[i] << a_content_value[i] 
      i+=1
    end 
    #convert to hash
    h_content={}
    a_content.map do |key, value|
      h_content[key.to_sym]=value
    end
    h_content[:Reference]=Time.now.strftime('%y%m%d%H%M%S%L')
    h_content[:Buyer]=@buyer_tnid
    h_content[:Supplier]=@supplier_tnid
    @fg_data_header=h_content
  end
  
  def set_filegen_doctype(doctype)
    type=['RFQ','QOT','ORD','POR','POC','INV']
    if type.include?doctype then
      @fg_doc_type=doctype
    else
      raise 'Document Type is not valid'
    end
  end
  
#This will need to generate two files: the actual src file and file containing absolute path of src file
  def generate_file
    case @fg_profile.to_sym
    when :STD
      file=Std::create_file(@fg_data_header, @fg_data_line_items='one', @fg_doc_type)
    when :NS51
      file='NS51'
    end
    
    doc_filename=ENV['HOME']+'/SS_FILES_GENERATED/'+@fg_profile.to_s+@fg_doc_type.to_s+@fg_data_header[:Reference]+'.src'
    #the source file creation
    File.open(doc_filename, 'w+:UTF-8') do |f|
      f.print file
    end
    #the absolute path of file
    File.open("#{ENV['HOME']}/source_path.txt", 'w+:UTF-8') do |f|
      f.print doc_filename
    end   
    File.file?(doc_filename)
  end

end

