require 'nokogiri'
require 'ffaker'

module AutoDataGen
  
  def define_data_mtml(mtml_template)
    @mtml_doc=Nokogiri::XML(mtml_template)
    #clear the line items for auto generation
    @mtml_doc.css('LineItem').remove
    @mtml_doc.css('MonetaryAmount').remove
    @mtml_doc.css('Party').remove
    @mtml_doc.css('Comments').remove
    @mtml_doc.css('PackagingInstructions').remove
    @mtml_doc.css('DateTimePeriod[Qualifier="_2"]').remove
  end
  
  def parse_for_modify
    @mtml_node=Nokogiri::XML::DocumentFragment.parse(@mtml_doc)
  end
  
  def gen_line_item_count
    rand(1..3)
  end
  
  def gen_line_item_description
      FFaker::Product.product
  end
  
  def gen_comments
    FFaker::HealthcareIpsum.paragraph
  end
  
  def gen_contact_person
    {
       :name=>FFaker::Name.name,
       :phone=>FFaker::PhoneNumber.phone_number,
       :email=>FFaker::Internet.email
    }
  end
  
  def gen_address
    # Comp Name, Street Add, City, Post Code, Country Code
    {
       :company_name=>FFaker::Company.name,
       :street_add1=>FFaker::AddressUK.street_address,
       :city=>FFaker::AddressUK.city,
       :state=>FFaker::AddressAU.state,
       :zip=>FFaker::AddressUS.zip_code,
       :country_code=>['UK','HK','SG','DK'].sample
    }
  end
  
  def reconstruct_data_line_items
    @total_lines=gen_line_item_count
    @doc_node['LineItemCount']=@total_lines
    section_grouping = 1
    section_number= @total_lines.to_i / section_grouping.to_i
    n=1
    while n <= @total_lines do
      line=gen_line_item_mtml_node
      line['Description']=gen_line_item_description + "\n (COOL WHITE) \n" + gen_line_item_description + "\n PRODUCT ID NUMBER: 2937 \n" + " Secondary \n NEW Line ****RETROFITTED **** \n" + gen_line_item_description + "\n" + gen_comments.slice(0,300)
      line['Number']=n
      line['Identification']=rand(100000..999999)
      line['MeasureUnitQualifier']=['PCE','DZN','PA'].sample
      #line['Quantity']=rand(5..15)
      line['Quantity']=rand(1.5..4.5).round(2)
      #line['SupplierPartNumber']='SPN-' +rand(7777777..8888888).to_s
      line['TypeCode']=['VP','BP','ZIM','UP','ZMA','ZIS'].sample
      section=line.at_css('Section')
      if n > section_grouping then
        i=1
        while i <= section_number  do
          if n > (i*section_grouping) and n <= ((i+1)*section_grouping) then
            section['Description']="Sector #{i} Diagram"
          end
          i+=1
        end
      end
      price=line.at_css('PriceDetails')
      #puts @doc_type
      @doc_type.upcase != 'RFQ' ? price['Value']=rand(20.5..50.7).round(2) :  price['Value']=0
      @doc_node.add_child line
      n+=1
    end
    @mtml_node.to_xml
  end
  
  def reconstruct_delivery_date
    cur=Date.today.next_day(15)
    dt=cur.strftime('%Y%m%d')
    val='<DateTimePeriod FormatQualifier="_102" Qualifier="_2" Value="20221016"/>'.gsub('20221016', dt)
    delivery_date_node = Nokogiri::XML::DocumentFragment.parse(val)
    @doc_node.add_child delivery_date_node
    @mtml_node.to_xml
  end    
  
  def reconstruct_comments(qualifier)
    comment_node=Nokogiri::XML::DocumentFragment.parse('<Comments Qualifier="ZAT">
        <Value>Subject for PO testing v_unique_ref</Value>
      </Comments>').first_element_child
    comment_node['Qualifier']=qualifier
    comment_node.at_css('Value').content = qualifier != 'ZAT' ? gen_comments : FFaker::Product.product_name
    @doc_node.add_child comment_node
   #puts  @mtml_node.to_xml
    @mtml_node.to_xml
  end
  
  def reconstruct_party_address(qualifier)
    party=gen_party_mtml_node
    company_data=gen_address
    contact_data=gen_contact_person
    party['Qualifier']=qualifier
    party['Name']=company_data[:company_name]
    party['City']=company_data[:city]
    party['CountrySubEntityIdentification']=company_data[:state]
    party['PostcodeIdentification']=company_data[:zip]
    party['CountryCode']=company_data[:country_code]
    address=party.at_css('StreetAddress')
    address.content=company_data[:street_add1]
    contact=party.at_css('Contact')
    contact['Name']=contact_data[:name]
    contact.css('CommunicationMethod').each do |comms|
       comms['Number']=contact_data[:phone] if comms['Qualifier']=='TE'
       comms['Number']=contact_data[:email] if comms['Qualifier']=='EM'
      end
    @doc_node.add_child party
    #puts @mtml_node.to_xml
    @mtml_node.to_xml
  end
  
  def reconstruct_vessel
    vessel_port_node=Nokogiri::XML::DocumentFragment.parse('<Party Qualifier="UD" Identification="2839388" Name="MANDORRI">
        <PartyLocation Port="GB-GUW" Qualifier="ZUC" />
      </Party>').first_element_child
      vessel_port_node['Identification']=rand(1000000..9999999)
      vessel_port_node['Name']=FFaker::NameIT.last_name
      port=vessel_port_node.at_css('PartyLocation')
      port['Port']=['FR-CHM','FR-GLO','FR-NOI'].sample
    @doc_node.add_child vessel_port_node
    #puts  @mtml_node.to_xml
    @mtml_node.to_xml      
  end
  
  def reconstruct_packaging
    packaging=Nokogiri::XML::DocumentFragment.parse('<PackagingInstructions RelatedInformationCode="34">
        <Value> Testing the packaging instruction</Value>
      </PackagingInstructions>').first_element_child
    instructions=packaging.at_css('Value')
    instructions.content=gen_comments
    @doc_node.add_child packaging
    #puts  @mtml_node.to_xml
    @mtml_node.to_xml          
  end
  
  def reconstruct_total_price
    #puts @doc_node.to_xml
    total_price_node=Nokogiri::XML::DocumentFragment.parse('<MonetaryAmount Qualifier="_259" Value="125" />').at_css('MonetaryAmount')
    line_subtotal_node=Nokogiri::XML::DocumentFragment.parse('<MonetaryAmount Qualifier="_79" Value="125" />').at_css('MonetaryAmount')
    total_price_node['Value']=calculate_total_price
    line_subtotal_node['Value']=total_price_node['Value']
    @doc_node.add_child total_price_node
    @doc_node.add_child line_subtotal_node
    #puts @mtml_node.to_xml
    @mtml_node.to_xml
  end
  
  def gen_line_item_mtml_node
    cmt=gen_comments
    cmt=cmt.slice(0,500)
    #cmt=""
    line_node=Nokogiri::XML::DocumentFragment.parse('<LineItem Number="3" TypeCode="BP" Identification="123" MeasureUnitQualifier="PCE" Quantity="1" Description="L.O. COOLER">
        <PriceDetails Qualifier="CAL" Value="30" TypeCode="QT" TypeQualifier="GRP" />
        <CustomsCode>Customs Code B</CustomsCode>
        <Section Description="Additional Item Information: Part No.: 81400962; Order No.: 465/02765/2021" DepartmentType="Standard Goods\59 Pneumatic and Electrical Tools\5935 High Pressure Cleaners\"/>
        <Comments Qualifier="LIN">
        <Value>'+cmt+'</Value>
      </Comments>
      </LineItem>')
      #'+cmt+'
    line_node=line_node.first_element_child
  end
  
  def gen_party_mtml_node
    party_node=Nokogiri::XML::DocumentFragment.parse('<Party Qualifier="BA" Identification="52636" Name="HMS Far East Pte Ltd" CountrySubEntityIdentification="Rosario" PostcodeIdentification="2450" CountryCode="SG" City="Bill Address">
        <StreetAddress>Billing Address</StreetAddress>
        <Contact FunctionCode="PD" Name="Michael Jordan III">
          <CommunicationMethod Qualifier="TE" Number="+65 (-6276) 7890" />
          <CommunicationMethod Qualifier="EM" Number="jchanwin@lycos.com" />
        </Contact>  
     </Party>')
    party_node=party_node.first_element_child
  end
  
  def get_doc_type_name
    @doc_typ=@mtml_node.first_element_child.first_element_child.first_element_child.name
  end   
  
  def doc_detail_fragment
    @doc_node=@mtml_node.at_css @doc_typ
  end
  
def calculate_total_price
    line=@mtml_node.css('LineItem')
    @total_price=0
    line.each do |item|
      #get quanity
      @quantity=item['Quantity']
      #get unit cost
         #find through grp qualifier
      item.css('PriceDetails').each do |price|
        @unit_cost=price['Value'] if price['TypeQualifier']=='GRP'
        #for coding consider discount
      end
      #calculate total cost
      @line_cost=@quantity.to_f*@unit_cost.to_f
      @total_price+=@line_cost
    end
    #puts @total_price
    @total_price
  end
  
end
