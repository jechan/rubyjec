require_relative 'profile/lab.rb'
require_relative 'profile/std.rb'
require_relative 'profile/spectec.rb'
require_relative 'profile/danaos.rb'
require_relative 'profile/bass.rb'
require_relative 'profile/prophet21.rb'
require_relative 'profile/ns5.rb'
require_relative 'profile/ns51.rb'
require_relative 'profile/apmm.rb'
require_relative 'profile/aalborg.rb'
require_relative 'profile/wallemtps.rb'
require_relative 'profile/amosintl.rb'
require_relative 'profile/one4al.rb'
require_relative 'profile/fastenal.rb'
require_relative 'profile/yuantong.rb'
require_relative 'profile/mtml15.rb'
require_relative 'profile/scandia.rb'
require_relative 'profile/iss.rb'
require_relative 'profile/drew.rb'
require_relative 'profile/saifee.rb'
require_relative 'profile/viking.rb'
require_relative 'profile/seaway.rb'
require_relative 'profile/sinwawindsor.rb'
require_relative 'profile/willwell.rb'
require_relative 'profile/sperre1.rb'
require_relative 'profile/nippon.rb'
require 'C:/Users/Tested/bitbucket/rubyjec/Shipserv/fx_client/fxcore.rb'
require 'C:/Users/Tested/bitbucket/rubyjec/Shipserv/rest_mtml/rest_mtml.rb'
require 'C:/Users/Tested/bitbucket/rubyjec/Shipserv//ssws/ssws.rb'
#require 'C:\fitnesse\fixtures\lib\integrations\client_gui.rb'

module Selector
  def use_fx_client(id, env='DEV', prof='STD')
    prof='STD' if prof.upcase.to_sym==:LAB
    prof='MTML1.5' if prof.upcase.to_sym==:MTML15
    fx=Fxcore.new
    fx.set_fx_environment(env)
    fx.open_fx_client
    fx.navigate_fx_tab('tradenet')
    fx.set_server_name
    fx.set_tradenet_account(id, prof)
    fx.exchange_now
    fx.wait_for_file_upload_download
  end
  
  def use_mtml(id, env='DEV', flag=1, data='NONE')
    rmtml=RestMtml.new
    rmtml.set_rest_environment(env)
    rmtml.set_rest_tnid(id)
    if flag==0 then
      rmtml.set_source_mtml_file(data) 
      rmtml.submit_ssmtml_file_to_server
    elsif flag==1
      rmtml.download_ssmtml_file_from_server
    end
  end
  
  def use_web_service(id, user, pwd, flag=1, env='DEV', prof='STD', data='NONE', method=NIL)  
    prof='STD' if prof.upcase.to_sym==:LAB
    prof='MTML1.5' if prof.upcase.to_sym==:MTML15
    ss=Ssws.new
    ss.set_ssws_environment_client(env)    
    ss.set_ssws_tnid(id)
    ss.set_ssws_user_id(user)
    ss.set_ssws_user_pwd(pwd)
    ss.set_ssws_profile(prof)
    ss.set_source_ssws_file(data)
    #flag==0 ? ss.submit_file_to_ssws : ss.download_file_from_ssws
    if flag==0 then
      case method
      when "customer_doc"
        ss.submit_file_to_ssws_customer_doc
      when "mtml_doc"
        ss.submit_file_to_ssws_mtml_doc
      when "send_doc"
        ss.submit_file_to_ssws_send_doc
      else
        ss.submit_file_to_ssws
      end
    else
      case method
      when "customer_doc"
        ss.download_file_from_ssws_customer_doc
      when "mtml_doc"
        ss.download_file_from_ssws_mtml_doc  
      when "get_doc"
        ss.download_file_from_ssws_get_doc        
      else
        ss.download_file_from_ssws
      end
    end
  end
  
  def use_client_gui(tnid, flag=1, upload=nil,env='DEV', type='magic')
    gui=ClientGui.new
    upload.nil? ? gui.fill_in(tnid.to_s,'213.86.57.8','11876') : gui.fill_in(tnid.to_s,'213.86.57.8','11876',upload) #to be changed host and port
    sleep(1)
    flag == 0 ? gui.send_document : gui.get_document
    sleep(10)
    gui.shutdown
  end
  
  def parse_download_rfq_data(prof, data)

    prof='MTML15' if prof.upcase=='MTML1.5'

    case prof.upcase.to_sym
    when :LAB, :SHELL
      Std::get_rfq_reference(data)
    when :STD
      Std::get_rfq_reference(data)
    when :NAUTISK
      Std::get_rfq_reference(data)
    when :WEILBACH
      Std::get_rfq_reference(data)
    when :FURUNO
      Std::get_rfq_reference(data)                        
    when :PROPHET21
      Prophet21::get_rfq_reference(data)
    when :AMOSINTL
      Amosintl::get_rfq_reference(data)
    when :ONE4AL
      Std::get_rfq_reference(data)
    when :FASTENAL
      Std::get_rfq_reference(data)      
    when :YUANTONG
      Std::get_rfq_reference(data) 
    when :MTML15
      Mtml15::get_rfq_reference(data)
    when :SCANDIA
      Scandia::get_rfq_reference(data)
    when :AALBORG
      Aalborg::get_rfq_reference(data)
    when :ISS
      Iss::get_rfq_reference(data)
    when :DREW
      Drew::get_rfq_reference(data)
    when :SAIFEE
      Saifee::get_rfq_reference(data) 
    when :VIKING
      Viking::get_rfq_reference(data)
    when :SEAWAY
      Seaway::get_rfq_reference(data)          
    when :SINWA_WINDSOR
      Sinwawindsor::get_rfq_reference(data)        
    when :WILLWELL
      Willwell::get_rfq_reference(data)
    when :SPERRE1
      Sperre1::get_rfq_reference(data)
    when :NIPPON
      Nippon::get_rfq_reference(data)                                                                                                 
    else
      'Not yet implemented'
    end
  end
  
  def parse_download_qot_data(prof, data)
    case prof.upcase.to_sym
    when :LAB, :SHELL
      Std::get_qot_reference(data)
    when :STD
      Std::get_qot_reference(data)
    when :NAUTISK
      Std::get_qot_reference(data)
    when :FURUNO
      Std::get_qot_reference(data)
    when :WEILBACH
      Std::get_qot_reference(data)                  
    when :SPECTEC
      Spectec::get_qot_reference(data)
    when :DANAOS
      Danaos::get_qot_reference(data)
    when :BASS
      Bass::get_qot_reference(data)
    when :NS5
      Ns5::get_qot_reference(data)
    when :NS51, :NS51_AET, :NS51_APL_SG
      Ns51::get_qot_reference(data)
    when :APMM
      Apmm::get_qot_reference(data)
    when :WALLEMTPS
      Wallemtps::get_qot_reference(data)
    else
      'Not yet implemented'
    end
  end
  
  def parse_download_ord_data(prof, data)
    prof='MTML15' if prof.upcase=='MTML1.5'
    case prof.upcase.to_sym
    when :LAB, :SHELL
      Std::get_ord_reference(data)
    when :STD
      Std::get_ord_reference(data)
    when :SPERRE1
      Sperre1::get_ord_reference(data)      
    when :FURUNO
      Std::get_ord_reference(data)
    when :WEILBACH
      Std::get_ord_reference(data)
    when :NAUTISK
      Std::get_ord_reference(data)                  
    when :MTML15
      Mtml15::get_ord_reference(data)      
    when :PROPHET21
      Prophet21::get_ord_reference(data)
    when :AALBORG
      Aalborg::get_ord_reference(data)
    when :AMOSINTL
      Amosintl::get_ord_reference(data)
    when :ONE4AL
      One4al::get_ord_reference(data)
    when :FASTENAL
      Std::get_ord_reference(data)
    when :YUANTONG
      Std::get_ord_reference(data)
    when :SCANDIA
      Scandia::get_ord_reference(data)
    when :VIKING
      Viking::get_ord_reference(data)       
    when :SAIFEE
      Saifee::get_ord_reference(data)                                             
    when :SEAWAY
      Seaway::get_ord_reference(data)   
    when :ISS
      Iss::get_ord_reference(data)        
    when :WILLWELL
      Willwell::get_ord_reference(data)        
    when :DREW
      Drew::get_ord_reference(data)
    when :NIPPON
      Nippon::get_ord_reference(data)        
    else      
      'Not yet implemented'
    end
  end    
    
  def parse_download_poc_data(prof, data)
    case prof.upcase.to_sym
    when :LAB, :SHELL
      Std::get_poc_reference(data)
    when :STD
      Std::get_poc_reference(data)
    else      
      'Not yet implemented'
    end
  end    

  
end