require 'nokogiri'

module Aalborg
  
  def self.get_rfq_reference(data)
    xml=Nokogiri::XML(data)
    int_ref=xml.at_css('Reference').content
    sender=xml.at_css('CustomerCode').content
    buyer_name=xml.at_css("Company").content
    #hum_ref=xml.at_xpath('//Reference[@Qualifier="UC"]')
    #rfq_head=xml.at_xpath('//RequestForQuote')
    #puts hum_ref[:ReferenceNumber]+' '+int_ref[:ReferenceNumber] +' '+rfq_head[:MessageNumber]
    {:rfq_internal_ref=>int_ref,:rfq_msg_no=> NIL ,:rfq_buyer=> sender,:rfq_buyer_name=>buyer_name}
  end
  
  def self.get_ord_reference(data)
    xml=Nokogiri::XML(data)
    int_ref=xml.at_css('Reference').content
        sender=xml.at_css('CustomerCode').content
    buyer_name=xml.at_css("Company").content
    {:ord_internal_ref=>int_ref,:ord_buyer=>sender,:ord_buyer_name=>buyer_name}
  end
  

end