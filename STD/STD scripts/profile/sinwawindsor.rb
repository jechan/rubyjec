require 'nokogiri'

#Supplier
module Sinwawindsor
  
  def self.get_rfq_reference(data)
    lines=data.split("\n")
    #doc_id=lines[0].match(/RID:(\d*)-/)[1]
    doc_id=data.match(/RID:(\d*)-/)[1]
    #buyer=lines[0].match(/TNID\[(.*)\]\s-/)[1]
    buyer=data.match(/RID:\d*-(\d*)/)[1]
    msg=NIL
    #buyer_name=lines[0].match(/TNID\[.*\]\s-\s(.*),/)[1].strip
    {:rfq_internal_ref=>doc_id,:rfq_msg_no=>msg,:rfq_buyer=>buyer}
  end
  
  def self.get_ord_reference(data)
    txt=prophet21_parser(data)
    int_ref=txt[:hdr][1]
    {:ord_internal_ref=>int_ref}
  end
  


end