require 'nokogiri'

#Supplier
module Seaway
  
  def self.get_rfq_reference(data)
    lines=data.split("\n")
    doc_id=lines[0].match(/RequestForQuote:.*/).to_s.slice(0,24).strip
    buyer=lines[0].match(/RFQHEA.*/).to_s.slice(17,5).strip
    msg=lines[0].match(/RequestForQuote:.*/).to_s.slice(24,11).strip
    buyer_name=lines[0].match(/RFQHEA.*/).to_s.slice(102,44).strip
    {:rfq_internal_ref=>doc_id,:rfq_msg_no=>msg,:rfq_buyer=>buyer,:rfq_buyer_name=>buyer_name}
  end
  
  def self.get_ord_reference(data)
    doc_id=NIL
    buyer=NIL
    buyer_name=NIL
    {:ord_internal_ref=>doc_id,:ord_buyer=>buyer,:ord_buyer_name=>buyer_name}
  end
  


end