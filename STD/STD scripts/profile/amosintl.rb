require 'nokogiri'
require 'csv'

module Amosintl
  
  def self.get_rfq_reference(data)
    csv=CSV.parse(data)
    int_ref=csv[1][2].to_s
    msg_no=csv[1][9].to_s
    #puts hum_ref[:ReferenceNumber]+' '+int_ref[:ReferenceNumber] +' '+rfq_head[:MessageNumber]
    {:rfq_internal_ref=>int_ref,:rfq_msg_no=>msg_no}
  end
  
  def self.get_ord_reference(data)
    csv=CSV.parse(data)
    int_ref=csv[1][2].to_s
    {:ord_internal_ref=>int_ref}
  end
  
end