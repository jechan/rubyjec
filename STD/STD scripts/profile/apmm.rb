require 'nokogiri'

module Apmm
    
  def self.get_qot_reference(data)
    int_ref=data.match(/RFF00501AAG(.*\w)/)
    {:qot_internal_ref=>int_ref[1]}
  end
  

end