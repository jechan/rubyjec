require 'nokogiri'

module Viking
  
  def self.get_rfq_reference(data)
    vik=Nokogiri::XML(data)
    buyer=NIL
    buyer_name=vik.at_xpath("//E1EDKA1/NAME1").text
    msg=vik.xpath("//ZSHIPSERVH01/IDENTIFIER")[1].text
    doc_id=vik.xpath("//ZSHIPSERVH01/IDENTIFIER")[0].text
    {:rfq_internal_ref=>doc_id,:rfq_msg_no=>msg,:rfq_buyer=>buyer,:rfq_buyer_name=>buyer_name}
  end
  
  def self.get_ord_reference(data)
    vik=Nokogiri::XML(data)
    doc_id=vik.xpath("//ZSHIPSERVH01/IDENTIFIER")[0].text
    buyer=NIL
    buyer_name=vik.at_xpath("//E1EDKA1/NAME1").text
    {:ord_internal_ref=>doc_id, :ord_buyer=>buyer, :ord_buyer_name=>buyer_name}
  end
  
  def self.get_qot_reference(data)
    {:qot_internal_ref=>'QOT REF'}
  end
  
end

