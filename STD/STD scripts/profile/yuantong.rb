require 'nokogiri'

module Yuantong
  
  def self.get_rfq_reference(data)
    xml=Nokogiri::XML(data)
    int_ref=xml.at_xpath('//Reference[@Qualifier="AGI"]')
    hum_ref=xml.at_xpath('//Reference[@Qualifier="UC"]')
    rfq_head=xml.at_xpath('//RequestForQuote')
    interchange=xml.at_xpath('//Interchange')
    #puts hum_ref[:ReferenceNumber]+' '+int_ref[:ReferenceNumber] +' '+rfq_head[:MessageNumber]
    {:rfq_internal_ref=>int_ref[:ReferenceNumber],:rfq_msg_no=>rfq_head[:MessageNumber],:rfq_buyer=>interchange[:Sender]}
  end
  
  def self.get_ord_reference(data)
    xml=Nokogiri::XML(data)
    int_ref=xml.at_xpath('//Reference[@Qualifier="ON"]')
    hum_ref=xml.at_xpath('//Reference[@Qualifier="UC"]')
    interchange=xml.at_xpath('//Interchange')
    {:ord_internal_ref=>int_ref[:ReferenceNumber],:ord_buyer=>interchange[:Sender]}
  end
  
  def self.get_qot_reference(data)
    {:qot_internal_ref=>'QOT REF'}
  end
  
end

