require 'nokogiri'


module Danaos
  
  def self.get_qot_reference(data)
    xml=Nokogiri::XML(data)
    int_ref=xml.at_xpath('//Reference[@Qualifier="AAG"]')
    #hum_ref=xml.at_xpath('//Reference[@Qualifier="UC"]')
    {:qot_internal_ref=>int_ref[:ReferenceNumber]}
  end
  
end