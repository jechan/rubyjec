require 'nokogiri'

module Std
  
  def self.get_rfq_reference(data)
    xml=Nokogiri::XML(data)
    int_ref=xml.at_xpath('//Reference[@Qualifier="AGI"]')
    hum_ref=xml.at_xpath('//Reference[@Qualifier="UC"]')
    rfq_head=xml.at_xpath('//RequestForQuote')
    interchange=xml.at_xpath('//Interchange')
    buyer_name=xml.at_xpath("//Party[@Qualifier='BY']")['Name']
    #puts hum_ref[:ReferenceNumber]+' '+int_ref[:ReferenceNumber] +' '+rfq_head[:MessageNumber]
    {:rfq_internal_ref=>int_ref[:ReferenceNumber].gsub(/\D/,''), :rfq_msg_no=>rfq_head[:MessageNumber],:rfq_buyer=>interchange[:Sender],:rfq_buyer_name=>buyer_name}
  end
  
  def self.get_ord_reference(data)
    xml=Nokogiri::XML(data)
    int_ref=xml.at_xpath('//Reference[@Qualifier="ON"]')
    hum_ref=xml.at_xpath('//Reference[@Qualifier="UC"]')
    interchange=xml.at_xpath('//Interchange')
    buyer_name=xml.at_xpath("//Party[@Qualifier='BY']")['Name']
    po_head=xml.at_xpath('//Order')
    {:ord_internal_ref=>int_ref[:ReferenceNumber].gsub(/\D/,''), :ord_buyer=>interchange[:Sender],:ord_buyer_name=>buyer_name, :ord_msg_no=>po_head[:MessageNumber]}
  end
  
  def self.get_qot_reference(data)
    xml=Nokogiri::XML(data)
    int_ref=xml.at_xpath('//Reference[@Qualifier="AAG"]') || 'NONE'
    hum_ref=xml.at_xpath('//Reference[@Qualifier="UC"]') || 'NONE'
    {:qot_internal_ref=>int_ref[:ReferenceNumber], :uc_ref => hum_ref[:ReferenceNumber]}
  end

  def self.get_poc_reference(data)
    xml=Nokogiri::XML(data)
    interchange=xml.at_xpath('//Interchange')
    {:poc_internal_ref=>interchange[:ControlReference]}
  end
  
end