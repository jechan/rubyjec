require 'nokogiri'

module Iss
  
  def self.get_rfq_reference(data)
    crdata=data.match(/CR\[.*\]/).to_s
    buyer=crdata.match(/TNID\[(.*)\]/)[1]
    msg=crdata.match(/MR\[(\d*)\]/)[1]
    doc_id=crdata.match(/REQUESTFORQUOTE:(\d*)\]/)[1]
    buyer_name=data.match(/TNID\[\w*\]\s-\s(.{1,56})/)[1].strip
    {:rfq_internal_ref=>doc_id,:rfq_msg_no=>msg,:rfq_buyer=>buyer,:rfq_buyer_name=>buyer_name}
  end
  
  def self.get_ord_reference(data)
    crdata=data.match(/CR\[.*\]/).to_s
    buyer=crdata.match(/TNID\[(.*)\]/)[1]
    doc_id=crdata.match(/ORDER:(\d*)\]/)[1]
    buyer_name=NIL
    {:ord_internal_ref=>doc_id,:ord_buyer=>buyer,:ord_buyer_name=>buyer_name}
  end
  
  def self.get_qot_reference(data)
    {:qot_internal_ref=>'QOT REF'}
  end
  
end

