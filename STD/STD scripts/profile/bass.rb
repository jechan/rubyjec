require 'nokogiri'

module Bass
  
  def self.get_rfq_reference(data)
    xml=Nokogiri::XML(data)
    int_ref=xml.at_xpath('//Reference[@Qualifier="AGI"]')
    hum_ref=xml.at_xpath('//Reference[@Qualifier="UC"]')
    rfq_head=xml.at_xpath('//RequestForQuote')
    #puts hum_ref[:ReferenceNumber]+' '+int_ref[:ReferenceNumber] +' '+rfq_head[:MessageNumber]
    {:rfq_internal_ref=>int_ref[:ReferenceNumber],:rfq_msg_no=>rfq_head[:MessageNumber]}
  end
  
  def self.get_ord_reference(data)
    xml=Nokogiri::XML(data)
    int_ref=xml.at_xpath('//Reference[@Qualifier="ON"]')
    hum_ref=xml.at_xpath('//Reference[@Qualifier="UC"]')
    {:ord_internal_ref=>int_ref[:ReferenceNumber]}
  end
  
  def self.get_qot_reference(data)
    xml=Nokogiri::XML(data)
    int_ref=xml.at_xpath('//Reference[@Qualifier="AAG"]')
    hum_ref=xml.at_xpath('//Reference[@Qualifier="UC"]')
    {:qot_internal_ref=>int_ref[:ReferenceNumber]}
  end
  
end