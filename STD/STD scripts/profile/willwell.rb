require 'nokogiri'

#Supplier
module Willwell
  
  def self.get_rfq_reference(data)
    lines=data.split("\n")
    doc_id=lines[0].split('~')[3]
    buyer=lines[0].split('~')[1]
    msg=NIL
    buyer_name=NIL
    {:rfq_internal_ref=>doc_id,:rfq_msg_no=>msg,:rfq_buyer=>buyer,:rfq_buyer_name=>buyer_name}
  end
  
  def self.get_ord_reference(data)
    dat=data.gsub("\n","").split("~")
    buyer=dat[1]
    doc_id=dat[3]
    buyer_name=NIL
    {:ord_internal_ref=>doc_id, :ord_buyer=>buyer, :ord_buyer_name=>buyer_name}
  end
  


end