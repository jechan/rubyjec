require 'nokogiri'

module Drew
  
  def self.get_rfq_reference(data)
    buyer=data.split("\n")[1].slice(0,16).strip
    msg=data.match(/09MSG(\d*)/)[1]
    doc_id=data.match(/RequestForQuote:(\d*)/)[1]
    buyer_name=data.match(/.*BY.*ZEX(.*)\b/)[1]
    {:rfq_internal_ref=>doc_id,:rfq_msg_no=>msg,:rfq_buyer=>buyer,:rfq_buyer_name=>buyer_name}
  end
  
  def self.get_ord_reference(data)
    doc_id=data.match(/Order:(\d*)/)[1]
    buyer=data.split("\n")[1].match(/\A\w*/)[0]
    buyer_name=data.match(/10BY\s*ZEX(.*)/)[1].strip
    {:ord_internal_ref=>doc_id,:ord_buyer=>buyer.to_s,:ord_buyer_name=>buyer_name}
  end
  
  def self.get_qot_reference(data)
    {:qot_internal_ref=>'QOT REF'}
  end
  
end

