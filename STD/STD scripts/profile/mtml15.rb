require 'nokogiri'

module Mtml15
  
  def self.get_rfq_reference(data)
    xml=Nokogiri::XML(data)
    int_ref=xml.at_xpath('//Reference[@Qualifier="AGI"]')
    hum_ref=xml.at_xpath('//Reference[@Qualifier="UC"]')
    rfq_head=xml.at_xpath('//RequestForQuote')
    interchange=xml.at_xpath('//Interchange')
    buyer_name=xml.at_xpath("//Party[@Qualifier='BY']")['Name']
    #puts hum_ref[:ReferenceNumber]+' '+int_ref[:ReferenceNumber] +' '+rfq_head[:MessageNumber]
    {:rfq_internal_ref=>int_ref[:ReferenceNumber],:rfq_msg_no=>rfq_head[:MessageNumber],:rfq_buyer=>interchange[:Sender],:rfq_buyer_name=>buyer_name}
  end
  
  def self.get_ord_reference(data)
    xml=Nokogiri::XML(data)
    int_ref=xml.at_css('Order')
    hum_ref=xml.at_xpath('//Reference[@Qualifier="UC"]')
    buyer_name=xml.at_xpath("//Party[@Qualifier='BY']")['Name']
    interchange=xml.at_xpath('//Interchange')
    {:ord_internal_ref=>int_ref[:MessageReferenceNumber], :ord_buyer=>interchange[:Sender],:ord_buyer_name=>buyer_name}
  end
  
  def self.get_qot_reference(data)
    xml=Nokogiri::XML(data)
    int_ref=xml.at_xpath('//Reference[@Qualifier="AAG"]')
    hum_ref=xml.at_xpath('//Reference[@Qualifier="UC"]')
    {:qot_internal_ref=>int_ref[:ReferenceNumber]}
  end
  
end