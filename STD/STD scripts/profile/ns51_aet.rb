require 'nokogiri'
#Buyer Integratopm

module Ns51aet
  
    def self.get_qot_reference(data)
    xml=Nokogiri::XML(data)
    int_ref=xml.at_xpath('//VendorRefNo').content #this is actually the Human Readable Reference for ABS/NS5 users
    {:qot_internal_ref=>int_ref}
  end
  
end