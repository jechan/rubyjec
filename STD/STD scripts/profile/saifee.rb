require 'nokogiri'

module Saifee
  
  def self.get_rfq_reference(data)
    buyer=NIL
    buyer_name=data.split("\n")[1].split("|")[1]
    msg=NIL
    doc_id=data.match(/File=RFQHdr(\d*)/)[1]
    {:rfq_internal_ref=>doc_id,:rfq_msg_no=>msg,:rfq_buyer=>buyer,:rfq_buyer_name=>buyer_name}
  end
  
  def self.get_ord_reference(data)
    #BUYER TNID AND BUYER NAME NOT IN DOWNLOADED PO AS OF 20150828 using LIVE version
    doc_id=data.match(/File=POHdr(\d*)/)[1]
    {:ord_internal_ref=>doc_id,:ord_buyer=>NIL,:ord_buyer_name=>NIL}
  end
  
  def self.get_qot_reference(data)
    {:qot_internal_ref=>'QOT REF'}
  end
  
end

