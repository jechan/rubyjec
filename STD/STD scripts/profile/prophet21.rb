require 'nokogiri'

#Supplier
module Prophet21
  
  def self.get_rfq_reference(data)
    lines=data.split("\n")
    doc_id=lines[0].split("\t")[1]
    buyer=lines[0].split("\t")[2].strip
    buyer_tkr=lines[0].split("\t")[9].strip
    buyer_name=lines[0].split("\t")[3].strip
    msg=NIL
    {:rfq_internal_ref=>doc_id,:rfq_msg_no=>msg,:rfq_buyer=>buyer,:rfq_buyer_name=>buyer_name, :rfq_buyer_tkr=>buyer_tkr}
  end
  
  def self.get_ord_reference(data)
    lines=data.split("\n")
    doc_id=lines[0].split("\t")[1]
    buyer=lines[0].split("\t")[2].strip
    buyer_tkr=lines[0].split("\t")[9].strip
    buyer_name=lines[0].split("\t")[3].strip
    {:ord_internal_ref=>doc_id,:ord_buyer=>buyer,:ord_buyer_name=>buyer_name}
  end
  


end