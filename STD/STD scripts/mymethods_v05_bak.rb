require 'fileutils'
require 'nokogiri'
require 'au3'
require 'win32/service'
require_relative 'selector'
require 'yaml'
#require 'D:/Shipserv/RUBY WORKSHOP/Shipserv/fx_client/fxtab/fx_log.rb'
#require 'D:/Shipserv/RUBY WORKSHOP/Shipserv/fx_client/fxcore.rb'
#require_relative 'profile/lab.rb'

class Bpdtest
  
  include Selector, Win32
  
  attr_accessor :buyer, :supplier, :doc_type, :attachment, :buyer_profile, :supplier_profile, :file_template, :system
  attr_accessor :buyer_user, :buyer_password, :supplier_user, :supplier_password, :unique_reference, :interface
  attr_accessor :use_last_rfq, :replacement
  attr_reader :send_wdir, :receive_wdir, :ext, :data, :download_ord_data, :download_rfq_data, :download_qot_data, :last_rfq_id
  LAST_RFQ_DATA="#{ENV['HOME']}/last_download_rfq.dat"
  
  def initialize(fx=0)
    @attachment='N'
    @doc_type='NOT'
    @buyer_profile='lab'
    @supplier_profile='LAB'
    @system='UAT1'
    @use_last_rfq=0
    @replacement=0
    #@unique_reference=Time.now.strftime('%y%m%d%H%M%S%L')
    @unique_reference=Time.now.strftime('%y%m%d')+"-"+Time.now.strftime('%H%M')+"-"+Time.now.strftime('%S%L')
    restart_fx_service if fx==1
    @no_poa=%w{
      YUANTONG
      SCANDIA
      AALBORG
      PROPHET21
      SPERRE
      SPERRE1
      SBS
      DREW
      NAUTISK
      }    
  end
 
  def create_document(multi=0,template="default")
    raise 'Buyer/Supplier must be entered' if @buyer.nil? or @supplier.nil?
    
    if multi==1 then
      @unique_reference=Time.now.strftime('%y%m%d')+"-"+Time.now.strftime('%H%M')+"-"+Time.now.strftime('%S%L')
    end
    
    puts @unique_reference
    
    profile_dir='C:/Users/Tested/Documents/Aptana Studio 3 Workspace//RUBY WORKSHOP/STD/STD scripts'
    if [:REQ, :RFQ, :ORD].include?(@doc_type.upcase.to_sym) then
      @send_wdir=profile_dir+"/profile/#{@buyer_profile.downcase}" 
      profile=@buyer_profile.upcase
    else
      @send_wdir=profile_dir+"/profile/#{@supplier_profile.downcase}"
      profile=@supplier_profile.upcase
    end
    
    #extension names
    csv=%w{
      MITSUBISHI
      SINWA_WINDSOR
      AMOSINTL
      }
    txt=%w{
      PROPHET21
      APMM
      WILLWELL
      SPERRE
      SINWA_WINDSOR
      DREW
    }
    if csv.include?(profile.upcase) then
      @ext='csv'
    elsif txt.include?(profile.upcase) then
      @ext='txt'
    else
      @ext='XML'
    end
    
    if template=="default" then
      @file_template="#@send_wdir/#{profile}_#{@doc_type.upcase}.#@ext" 
    else
      @file_template=template
    end

    raise "NOT VALID FILE --> #@file_template" if not File.file?(@file_template)
    
    #@data=File.read(@file_template)
    File.open(@file_template, "r+") do |f|
      s=nil
      f.each do |line|
        if s.nil? then
          s=line.to_s
        else
          s.to_s << line.to_s
         end
      end
      @data=s
    end      
    @data.gsub!('v_unique_ref', @unique_reference)
    @data.gsub!('v_supplier',@supplier)

    @data=modify_data_for_replacement(@doc_type, @data) if @replacement == 1
    puts @data
    case @doc_type.upcase.to_sym
    when :QOT
      use_last_rfq_for_qot if @use_last_rfq==1
      if not @download_rfq_data.nil? then
        @data.gsub!("v_rfq_number",@download_rfq_data[:rfq_internal_ref])
        @data.gsub!("v_message_number",@download_rfq_data[:rfq_msg_no]) if not @download_rfq_data[:rfq_msg_no].nil?
#revert this back     
       @data.gsub!('v_buyer',@download_rfq_data[:rfq_buyer]) if not @download_rfq_data[:rfq_buyer].nil?
#for match proxy testing
#      @data.gsub!('v_buyer', @buyer) if not @download_rfq_data[:rfq_buyer].nil?
      end
    when :RDC
      use_last_rfq_for_qot if @use_last_rfq==1
      if not @download_rfq_data.nil? then
        @data.gsub!("v_rfq_number",@download_rfq_data[:rfq_internal_ref])
        @data.gsub!("v_message_number",@download_rfq_data[:rfq_msg_no]) if not @download_rfq_data[:rfq_msg_no].nil?
        @data.gsub!('v_buyer',@download_rfq_data[:rfq_buyer]) if not @download_rfq_data[:rfq_buyer].nil?
      end      
    when :ORD
      if not @download_qot_data.nil? then
        @data.gsub!("v_quote_number", @download_qot_data[:qot_internal_ref]) 
      else
        #agi_match=/<.*Qualifier="AGI".*\/>/
        #@data.gsub!(@data.match(agi_match).to_s,'')
        #aag_match=/<.*Qualifier="AAG".*\/>/
        #@data.gsub!(@data.match(aag_match).to_s,'')
        @data.gsub!("v_quote_number",'')
        #@data.gsub!('<Reference Qualifier="AGI" ReferenceNumber="v_quote_number"/>','')
      end
    when :ORP
      @data.gsub!('v_order_number', @download_ord_data[:ord_internal_ref]) if not @download_ord_data.nil?
      @data.gsub!('v_buyer',@download_ord_data[:ord_buyer]) if not @download_ord_data[:ord_buyer].nil?
      #puts @data
    when :POC
      
      if not @no_poa.include?(profile.upcase) then
        @ord_response=File.read("#@send_wdir/#{profile}_ORP.#@ext")
        @ord_response.gsub!('v_unique_ref', @unique_reference)
        @ord_response.gsub!('v_supplier',@supplier)
        @ord_response.gsub!('v_order_number', @download_ord_data[:ord_internal_ref]) if not @download_ord_data.nil?
        @ord_response.gsub!('v_buyer',@download_ord_data[:ord_buyer]) if not @download_ord_data.nil?
        @ord_response.gsub!('v_buyer',@buyer)
      end
      
      @data.gsub!('v_order_number', @download_ord_data[:ord_internal_ref]) if not @download_ord_data.nil?
      @data.gsub!('v_buyer',@download_ord_data[:ord_buyer]) if not @download_ord_data.nil?
    end
    @data.gsub!('v_buyer',@buyer)
  end
  
  def send_document(platform='FX', nosend=0)
    @interface=platform.upcase
    if [:REQ, :RFQ, :ORD].include?(@doc_type.upcase.to_sym) then 
      out_folder="C:/MTML/#@buyer/OUT/"
      tnid=@buyer
      profile=@buyer_profile
      user=@buyer_user
      pwd=@buyer_password
    else 
      out_folder="C:/MTML/#@supplier/OUT/"
      tnid=@supplier
      profile=@supplier_profile
      user=@supplier_user
      pwd=@supplier_password      
    end
  
    case platform.upcase.to_sym
    when :GUI 
      std_mtml_profile=['STD','LAB']        
      if std_mtml_profile.include?(@buyer_profile.upcase) || std_mtml_profile.include?(@supplier_profile.upcase) then
          if @doc_type.upcase=='POC' then
            puts "*********ORP UPLOADED for POC**********"
            puts @ord_response        
            File.open('C:\MTML\upload_orp.xml','w+') do |f|
              f.puts @ord_response
            end
            sleep(2)
            use_client_gui(tnid.to_s,0,'C:\MTML\upload_orp.xml')            
          end
          File.open('C:\MTML\upload.xml','w+') do |f|
          f.print @data
        end
      end
      puts "*********#{@doc_type.upcase} UPLOADED**********"
      puts @data
      puts 'manual click of send document'
      sent_file=use_client_gui(tnid.to_s,0)
    when :FX
      raise 'invalid out directory' if not File.directory?(out_folder)
      if @doc_type.upcase=='POC' and not @no_poa.include?(profile.upcase) then
        puts "*********ORP UPLOADED for POC**********"
        puts @ord_response        
        File.open(out_folder+@unique_reference+"_ORP."+@ext,'w+') do |f|
          f.puts @ord_response
        end
      end
      
      puts "*********#{@doc_type.upcase} UPLOADED**********"
      puts @data
      
      upload_file=out_folder+@unique_reference+"_#@doc_type."+@ext
      
      if @supplier_profile != 'SBS' then
        File.open(upload_file,'w+') do |f|
          f.puts @data
        end     
      end
      if nosend==0
        #need to modify fx core codes
        fx_tn_acct=tnid.to_s+'-'+@supplier_user
        puts fx_tn_acct
        sent_file=use_fx_client(fx_tn_acct, @system, profile) 
      else
        att_folder=out_folder+@unique_reference+"_#@doc_type."+@ext+"_ATTACHMENT"
        FileUtils.mkdir(att_folder)
        2.times do
          FileUtils.cp('C:/Users/Public/Pictures/Sample Pictures/CEF_1B_eo.jpg', att_folder+"/#{Time.now.strftime('%y%m%d%H%M%S%L')}.jpg")
        end
        #need to modify fx core codes
        #sent_file=use_fx_client(tnid, @system, profile)
      end
    when :MTML
      raise 'MTML Server can only be used for STD format' if (profile.upcase !='LAB' and profile.upcase !='STD')
      if @doc_type.upcase=='POC' then
            puts "*********ORP UPLOADED for POC**********"
            puts @ord_response
            use_mtml(tnid, @system, 0, @ord_response) 
      end
      puts "*********#{@doc_type.upcase} UPLOADED**********"
      puts @data
      sent_file=use_mtml(tnid, @system, 0, @data)
    when :WS
      if @doc_type.upcase=='POC' and not @no_poa.include?(profile.upcase) then
        puts "*********ORP UPLOADED for POC**********"
        puts @ord_response
        use_web_service(tnid, user, pwd, 0, @system, profile, @ord_response) 
      end
      puts "*********#{@doc_type.upcase} UPLOADED**********"
      puts @data
      sent_file=use_web_service(tnid, user, pwd, 0, @system, profile, @data)
    when :WS2
      if @doc_type.upcase=='POC' and not @no_poa.include?(profile.upcase) then
        puts "*********ORP UPLOADED for POC**********"
        puts @ord_response
        use_web_service(tnid, user, pwd, 0, @system, profile, @ord_response,"customer_doc") 
      end
      puts "*********#{@doc_type.upcase} UPLOADED**********"
      puts @data
      sent_file=use_web_service(tnid, user, pwd, 0, @system, profile, @data,"customer_doc")
    when :WS3
      if @doc_type.upcase=='POC' and not @no_poa.include?(profile.upcase) then
        puts "*********ORP UPLOADED for POC**********"
        puts @ord_response
        use_web_service(tnid, user, pwd, 0, @system, profile, @ord_response,"mtml_doc") 
      end
      puts "*********#{@doc_type.upcase} UPLOADED**********"
      puts @data
      sent_file=use_web_service(tnid, user, pwd, 0, @system, profile, @data,"mtml_doc")
    when :WS4
      if @doc_type.upcase=='POC' and not @no_poa.include?(profile.upcase) then
        puts "*********ORP UPLOADED for POC**********"
        puts @ord_response
        use_web_service(tnid, user, pwd, 0, @system, profile, @ord_response,"send_doc") 
      end
      puts "*********#{@doc_type.upcase} UPLOADED**********"
      puts @data
      sent_file=use_web_service(tnid, user, pwd, 0, @system, profile, @data,"send_doc")                          
    end
    @replacement=0
  end
  
  def receive_document(platform='FX')
    @interface=platform.upcase
    if [:RFQ, :ORD, :SDOC].include?(@doc_type.upcase.to_sym) then
      @receive_wdir=Dir.pwd+"/profile/#{@supplier_profile.downcase}"
      in_folder="C:/MTML/#@supplier/IN/"
      tnid=@supplier
      profile=@supplier_profile
      user=@supplier_user
      pwd=@supplier_password    
    else
      @receive_wdir=Dir.pwd+"/profile/#{@buyer_profile.downcase}"
      in_folder="C:/MTML/#@buyer/IN/"
      tnid=@buyer
      profile=@buyer_profile
      user=@buyer_user
      pwd=@buyer_password
    end
    
    case platform.upcase.to_sym
    when :GUI
      puts 'manual download hehe'
      use_client_gui(tnid.to_s,1) if @doc_type.upcase=='POC'
      receive_file=use_client_gui(tnid.to_s,1)
      receive_data=File.read('C:\MTML\download.xml')
    when :FX
      fx_tn_acct=tnid.to_s+'-'+@supplier_user
      receive_file=use_fx_client(fx_tn_acct, @system, profile)
      puts "RECEIVE FILE"
      puts receive_file
      @doc_type.upcase != 'ORP' ? doc_type=@doc_type : doc_type='POC'
      #receive_file_loc=in_folder+doc_type.upcase+'/'+receive_file #need to revisit
      receive_file_loc=in_folder+receive_file
      puts receive_file_loc
      receive_data=File.read(receive_file_loc)||'NO DATA READ'  
      #testing of file open      
      File.open(receive_file_loc, "r+:UTF-8") do |f|
        r=nil
        f.each do |line|
          if r.nil? then
            r=line.to_s
          else
            r.to_s << line.to_s
          end
        end
        @rdata=r
      end      
    when :MTML
      use_mtml(tnid, @system, 1) if @doc_type.upcase=='POC'
      receive_data=use_mtml(tnid, @system, 1)
    when :WS
      use_web_service(tnid, user, pwd, 1, @system, profile) if @doc_type.upcase=='POC'
      receive_data=use_web_service(tnid, user, pwd, 1, @system, profile)      
    when :WS2
      use_web_service(tnid, user, pwd, 1, @system, profile, "data","customer_doc") if @doc_type.upcase=='POC'
      receive_data=use_web_service(tnid, user, pwd, 1, @system, profile, "data","customer_doc")    
    when :WS3
      use_web_service(tnid, user, pwd, 1, @system, profile, "data","mtml_doc") if @doc_type.upcase=='POC'
      receive_data=use_web_service(tnid, user, pwd, 1, @system, profile, "data","mtml_doc")     
    when :WS4
      use_web_service(tnid, user, pwd, 1, @system, profile, "data","get_doc") if @doc_type.upcase=='POC'
      receive_data=use_web_service(tnid, user, pwd, 1, @system, profile, "data","get_doc")                  
    end
    
    puts "*********#{@doc_type.upcase} DOWNLOADED**********"
    #puts @rdata
    puts receive_data
    
    puts "********* #{@doc_type.upcase} DOWNLOAD DATA**********"
    case @doc_type.upcase.to_sym
    when :RFQ
      #@download_rfq_data=Lab::get_rfq_reference(receive_data)
      @download_rfq_data=parse_download_rfq_data(profile, receive_data)
      puts @download_rfq_data
      puts Dir.pwd
      File.open(LAST_RFQ_DATA,'w') {|fp| fp.print(@download_rfq_data[:rfq_internal_ref].to_s+','+@download_rfq_data[:rfq_msg_no].to_s+','+@download_rfq_data[:rfq_buyer].to_s)}
      save_info_downloaded_txn(@unique_reference, @doc_type, @download_rfq_data)
      @download_rfq_data
    when :QOT
      #@download_qot_data=Lab::get_qot_reference(receive_data)
      @download_qot_data=parse_download_qot_data(profile, receive_data)
      puts @download_qot_data
      save_info_downloaded_txn(@unique_reference, @doc_type, @download_qot_data)
      @download_qot_data
      # get AGI and place in file_seq
    when :ORD
      #@download_ord_data=Lab::get_ord_reference(receive_data)
      @download_ord_data=parse_download_ord_data(profile, receive_data)
      puts @download_ord_data
      save_info_downloaded_txn(@unique_reference, @doc_type, @download_ord_data)
      @download_ord_data
      #get AGI and place in file_seq
    when :POC
      @download_poc_data=parse_download_poc_data(profile, receive_data)
      puts @download_poc_data
      save_info_downloaded_txn(@unique_reference, @doc_type, @download_poc_data)
      @download_poc_data
    else
      'no parsing yet'
    end
  end
  
  def restart_fx_service
    #restart service
    service_name='MTMLLink-FX'
    Service.stop(service_name) if Service.status(service_name).current_state=='running'
    until Service.status(service_name).current_state=='stopped' do
      sleep(1)
      puts Service.status(service_name).current_state
    end
    Service.start(service_name) if Service.status(service_name).current_state == 'stopped'

    until Service.status(service_name).current_state=='running' do
      sleep(1)
      puts Service.status(service_name).current_state
    end
    sleep(3)
  end  
  
  def use_last_rfq_for_qot
    puts ">>>>> use_last_rfq_for_qot"
    last_rfq_data=File.read(LAST_RFQ_DATA).split(',')
    puts last_rfq_data.inspect
    @unique_reference=Time.now.strftime('%y%m%d')+"-"+Time.now.strftime('%H%M')+"-"+Time.now.strftime('%S%L')
    @download_rfq_data={:rfq_internal_ref=>last_rfq_data[0],:rfq_msg_no=>last_rfq_data[1],:rfq_buyer=>last_rfq_data[2]}
    @last_rfq_id=@download_rfq_data[:rfq_internal_ref].to_s.gsub('RequestForQuote:','')
  end
  
  def get_reference_data(resp_doc, reference, buyer, supplier)    
    source_doc=get_source_document(resp_doc)
    txn_data=eval(File.read("#{ENV['Home']}/#{source_doc}_#{reference}_#{buyer}_#{supplier}.dat"))
    get_agi_reference(source_doc, txn_data)
  end
  
  private
  
  def save_info_downloaded_txn(file_name, doc_type, data)
    upper=doc_type.upcase
    File.open("#{ENV['HOME']}/#{upper}_#{file_name}_#{@buyer}_#{@supplier}.dat",'w') {|fp| fp.puts(data)}
  end
  
  def get_source_document(doc_type)
    case doc_type.upcase.to_sym
      when :QOT then 'RFQ'
      when :ORD then 'QOT'
      when :POC then 'ORD'  
    end
    
  end
  
  def get_agi_reference(doc_type, data)
    case doc_type.upcase.to_sym
      when :RFQ then @download_rfq_data=data
      when :QOT then @download_qot_data=data
      when :ORD then @download_ord_data=data     
    end
  end
  
  def modify_data_for_replacement(doc_type, data)
    xml=Nokogiri::XML(data)
    ##modify control reference
    xml.at_css(:Interchange)[:ControlReference]="#{@unique_reference}:Rev1"
    if ['RFQ','ORD'].include?(doc_type.upcase) then
      ##modify function code
      xml.at_css(:RequestForQuote)[:FunctionCode]="_5" if doc_type.upcase=='RFQ'
      xml.at_css(:Order)[:FunctionCode]="_5" if doc_type.upcase=='ORD'
      ##include ACW
      xml=Nokogiri::XML::DocumentFragment.parse(xml.at_css(:MTML))
      reference=xml.at_css(:Reference)
      reference.add_next_sibling("<Reference Qualifier=\"ACW\" ReferenceNumber=\"C.#{@unique_reference}\" />")     
    end
    xml.to_s
  end  
    
end

