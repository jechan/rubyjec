require 'au3'
PROG_TITLE="MTML Client Tester"


class Trial
	7.times do |num|
		num+=1
		define_method "edit#{num}" do
			AutoItX3::Control.new(PROG_TITLE,"","Edit#{num}")
		end
	end
	
	11.times do |num|
		num+=1
		define_method "button#{num}" do
			AutoItX3::Control.new(PROG_TITLE,"","Button#{num}")
		end
	end
	
	@@line_item=[{'Identification' => 'what', 'Description'=>'when'}]

	['Identification','Description'].each do |attr|
		define_method attr.downcase do |argument|
			if argument.nil? then
				@@line_item[0][attr]
			else
				argument = argument == 'NIL' ? '' : argument.to_s
			end
		end
	end
end


t=Trial.new
puts t.identification('check')