module OutboundDoc
  class RetrieveStdAttribute
    attr_reader :xml_doc

    def initialize(xml_doc)
      @doc = xml_doc
    end

    # Interchange Node
    def control_reference
      control_reference = @doc.xpath('//Interchange/@ControlReference').text
      control_reference.empty? ? "nil" : control_reference
    end

    def sender
      sender = @doc.xpath('//Interchange/@Sender').text
      sender.empty? ? "nil" : sender.to_i
    end

    def recipient
      recipient = @doc.xpath('//Interchange/@Recipient').text
      recipient.empty? ? "nil" : recipient.to_i
    end

    def preparation_date
      preparation_date = @doc.xpath('//Interchange/@PreparationDate').text
      preparation_date.empty? ? "nil" : preparation_date
    end

    def preparation_time
      preparation_time = @doc.xpath('//Interchange/@PreparationTime').text
      preparation_time.empty? ? "nil" : preparation_time
    end

    def msg_ref_number
      msg_ref_number = @doc.xpath('//@MessageReferenceNumber').text
      msg_ref_number.empty? ? "nil" : msg_ref_number
    end

    def message_number
      message_number = @doc.xpath('//@MessageNumber').text
      message_number.empty? ? "nil" : message_number
    end

    def function_code
      function_code = @doc.xpath('(//@FunctionCode)[1]').text
      function_code.empty? ? "nil" : function_code
    end

    # DocType Node
    def currency
      currency = @doc.xpath('//@CurrencyCode').text
      currency.empty? ? "nil" : currency
    end

    def delivery_terms
      delivery_terms = @doc.xpath('//@DeliveryTermsCode').text
      delivery_terms.empty? ? "nil" : delivery_terms
    end

    def line_item_count
      line_item_count = @doc.xpath('//@LineItemCount').text
      line_item_count.empty? ? "nil" : line_item_count
    end

    def priority
      priority = @doc.xpath('(//@Priority)[1]').text
      priority.empty? ? "nil" : priority
    end

    def tax_status
      tax_status = @doc.xpath('//@TaxStatus').text
      tax_status.empty? ? "nil" : tax_status
    end

    def mode_of_transport
      mode_of_transport = @doc.xpath('//@TransportModeCode').text
      mode_of_transport.empty? ? "nil" : mode_of_transport
    end

    # DateTimePeriod
    def eta
      eta = @doc.xpath('//DateTimePeriod[@Qualifier="_132"]/@Value').text
      eta.empty? ? "nil" : eta
    end

    def etd
      etd = @doc.xpath('//DateTimePeriod[@Qualifier="_133"]/@Value').text
      etd.empty? ? "nil" : etd
    end

    def delivery_date
      delivery_date = @doc.xpath('//DateTimePeriod[@Qualifier="_2"]/@Value').text
      delivery_date.empty? ? "nil" : delivery_date
    end

    def advise_date
      advise_date = @doc.xpath('//DateTimePeriod[@Qualifier="_175"]/@Value').text
      advise_date.empty? ? "nil" : advise_date
    end

    def doc_submitted_date
      doc_submitted_date = @doc.xpath('//DateTimePeriod[@Qualifier="_137"]/@Value').text
      doc_submitted_date.empty? ? "nil" : doc_submitted_date
    end

    def quote_validity_date
      quote_validity_date = @doc.xpath('//DateTimePeriod[@Qualifier="_36"]/@Value').text
      quote_validity_date.empty? ? "nil" : quote_validity_date
    end

    def delivery_lead_time
      delivery_lead_time = @doc.xpath('//DateTimePeriod[@Qualifier="_69"]/@Value').text
      delivery_lead_time.empty? ? "nil" : delivery_lead_time
    end

    # Comments Qualifier
    def buyer_comments
      buyer_comments = @doc.xpath('//Comments[@Qualifier="PUR"]/Value').text
      buyer_comments.empty? ? "nil" : buyer_comments
    end

    def supplier_comments
      supplier_comments = @doc.xpath('//Comments[@Qualifier="SUR"]/Value').text
      supplier_comments.empty? ? "nil" : supplier_comments
    end

    def terms_of_payment
      terms_of_payment = @doc.xpath('//Comments[@Qualifier="ZTP"]/Value').text
      terms_of_payment.empty? ? "nil" : terms_of_payment
    end

    def general_tc
      general_tc = @doc.xpath('//Comments[@Qualifier="ZTC"]/Value').text
      general_tc.empty? ? "nil" : general_tc
    end

    def subject
      subject = @doc.xpath('//Comments[@Qualifier="ZAT"]/Value').text
      subject.empty? ? "nil" : subject
    end

    # Party Contact Vessel Info
    def vessel_name
      vessel_name = @doc.xpath('//Party[@Qualifier="UD"]/@Name').text
      vessel_name.empty? ? "nil" : vessel_name
    end

    def vessel_imo_number
      vessel_imo_number = @doc.xpath('//Party[@Qualifier="UD"]/@Identification').text
      vessel_imo_number.empty? ? "nil" : vessel_imo_number
    end

    # Reference Numbers
    def aag_ref_no
      aag_ref_no = @doc.xpath('//Reference[@Qualifier="AAG"]/@ReferenceNumber').text
      aag_ref_no.empty? ? "nil" : aag_ref_no
    end

    def agi_ref_no
      agi_ref_no = @doc.xpath('//Reference[@Qualifier="AGI"]/@ReferenceNumber').text
      agi_ref_no.empty? ? "nil" : agi_ref_no
    end

    def on_ref_no
      on_ref_no = @doc.xpath('//Reference[@Qualifier="ON"]/@ReferenceNumber').text
      on_ref_no.empty? ? "nil" : on_ref_no
    end

    def uc_ref_no
      uc_ref_no = @doc.xpath('//Reference[@Qualifier="UC"]/@ReferenceNumber').text
      uc_ref_no.empty? ? "nil" : uc_ref_no
    end

    # Line Items
    def li_description(index = 1)
      li_description = @doc.xpath("(//LineItem/@Description)[#{index}]").text
      li_description.empty? ? "nil" : li_description
    end

    def li_part_number(index = 1)
      li_part_number = @doc.xpath("(//LineItem/@Identification)[#{index}]").text
      li_part_number.empty? ? "nil" : li_part_number
    end

    def li_uom(index = 1)
      li_uom = @doc.xpath("(//LineItem/@MeasureUnitQualifier)[#{index}]").text
      li_uom.empty? ? "nil" : li_uom
    end

    def li_number(index = 1)
      li_number = @doc.xpath("(//LineItem/@Number)[#{index}]").text
      li_number.empty? ? "nil" : li_number.to_i
    end

    def li_priority(index = 1)
      li_priority = @doc.xpath("(//LineItem/@Priority)[#{index}]").text
      li_priority.empty? ? "nil" : li_priority
    end

    def li_quality(index = 1)
      li_quality = @doc.xpath("(//LineItem/@Quality)[#{index}]").text
      li_quality.empty? ? "nil" : li_quality
    end

    def li_quantity(index = 1)
      li_quantity = @doc.xpath("(//LineItem/@Quantity)[#{index}]").text
      li_quantity.empty? ? "nil" : li_quantity.to_f
    end

    def li_part_code_type(index = 1)
      li_part_code_type = @doc.xpath("(//LineItem/@TypeCode)[#{index}]").text
      li_part_code_type.empty? ? "nil" : li_part_code_type
    end

    def li_comments(index = 1)
      li_comments = @doc.xpath("(//Comments[@Qualifier=\"LIN\"]/Value)[#{index}]").text
      li_comments.empty? ? "nil" : li_comments
    end

    # Equipment Details
    def eqp_details_account_number(index = 1)
      eqp_details_account_number = @doc.xpath("(//LineItem/Section/@AccountNumber)[#{index}]").text
      eqp_details_account_number.empty? ? "nil" : eqp_details_account_number
    end

    def eqp_details_department_type(index = 1)
      eqp_details_department_type = @doc.xpath("(//LineItem/Section/@DepartmentType)[#{index}]").text
      eqp_details_department_type.empty? ? "nil" : eqp_details_department_type
    end

    def eqp_details_description(index = 1)
      eqp_details_description = @doc.xpath("(//LineItem/Section/@Description)[#{index}]").text
      eqp_details_description.empty? ? "nil" : eqp_details_description
    end

    def eqp_details_drawing_number(index = 1)
      eqp_details_drawing_number = @doc.xpath("(//LineItem/Section/@DrawingNumber)[#{index}]").text
      eqp_details_drawing_number.empty? ? "nil" : eqp_details_drawing_number
    end

    def eqp_details_manufacturer(index = 1)
      eqp_details_manufacturer = @doc.xpath("(//LineItem/Section/@Manufacturer)[#{index}]").text
      eqp_details_manufacturer.empty? ? "nil" : eqp_details_manufacturer
    end

    def eqp_details_model_number(index = 1)
      eqp_details_model_number = @doc.xpath("(//LineItem/Section/@ModelNumber)[#{index}]").text
      eqp_details_model_number.empty? ? "nil" : eqp_details_model_number
    end

    def eqp_details_name(index = 1)
      eqp_details_name = @doc.xpath("(//LineItem/Section/@Name)[#{index}]").text
      eqp_details_name.empty? ? "nil" : eqp_details_name
    end

    def eqp_details_rating(index = 1)
      eqp_details_rating = @doc.xpath("(//LineItem/Section/@Rating)[#{index}]").text
      eqp_details_rating.empty? ? "nil" : eqp_details_rating
    end

    def eqp_details_serial_number(index = 1)
      eqp_details_serial_number = @doc.xpath("(//LineItem/Section/@SerialNumber)[#{index}]").text
      eqp_details_serial_number.empty? ? "nil" : eqp_details_serial_number
    end

    # Line Item Unit Cost & Discount
    def li_unit_cost(index = 1)
      li_unit_cost = @doc.xpath("(//LineItem/PriceDetails[@TypeQualifier=\"GRP\"]/@Value)[#{index}]").text
      li_unit_cost.empty? ? "nil" : li_unit_cost.to_f
    end

    def li_unit_cost_index(index = 1)
      li_unit_cost = @doc.xpath("(//LineItem/PriceDetails[@TypeQualifier=\"GRP\"]/@Value)[#{index}]").text
      li_unit_cost.empty? ? "nil" : li_unit_cost.to_f
    end

    def li_unit_discount_cost(index = 1)
      li_unit_discount_cost = @doc.xpath("(//LineItem/PriceDetails[@TypeQualifier=\"DPR\"]/@Value)[#{index}]").text
      li_unit_discount_cost.empty? ? "nil" : li_unit_discount_cost.to_f
    end

    def li_unit_discount_cost_index(index = 1)
      li_unit_discount_cost = @doc.xpath("(//LineItem/PriceDetails[@TypeQualifier=\"DPR\"]/@Value)[#{index}]").text
      li_unit_discount_cost.empty? ? "nil" : li_unit_discount_cost.to_f
    end

    #TODO: Check if via Line Item Number is needed.
    #li1_unit_cost @doc.xpath('//LineItem[@Number="1"]/PriceDetails[@TypeQualifier="GRP"]/@Value')
    #li1_disc_amt @doc.xpath('//LineItem[@Number="1"]/PriceDetails[@TypeQualifier="DPR"]/@Value')

    # Monetary Amounts
    def packing_cost
      packing_cost = @doc.xpath('//MonetaryAmount[@Qualifier="_106"]/@Value').text
      packing_cost.empty? ? "nil" : packing_cost.to_f
    end

    def total_cost
      total_cost = @doc.xpath('//MonetaryAmount[@Qualifier="_259"]/@Value').text
      total_cost.empty? ? "nil" : total_cost.to_f
    end

    def freight_cost
      freight_cost = @doc.xpath('//MonetaryAmount[@Qualifier="_64"]/@Value').text
      freight_cost.empty? ? "nil" : freight_cost.to_f
    end

    def line_item_total
      line_item_total = @doc.xpath('//MonetaryAmount[@Qualifier="_79"]/@Value').text
      line_item_total.empty? ? "nil" : line_item_total.to_f
    end

    def header_discount
      header_discount = @doc.xpath('//MonetaryAmount[@Qualifier="_204"]/@Value').text
      header_discount.empty? ? "nil" : header_discount.to_f
    end
  end
end