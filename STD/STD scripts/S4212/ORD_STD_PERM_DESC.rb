#include libraries and gems
require 'FileUtils'

string='door mat'
string_array=string.split(' ')
a_string_array=string_array.permutation.to_a

i=0
while i < 1

#Global Variable Declaration
v_buyer='10414'
v_supplier='59758'
v_out_folder="C:/MTML/#{v_buyer}/OUT/"
v_work_dir=''
v_dir_template='C:/Shipserv Backup/RUBY WORKSHOP/STD/STD MTML TEMPLATE/'
v_template='STD_ORD.XML'
v_seq_file='ORD_SEQ.txt'

v_description=a_string_array[rand(a_string_array.length)].join(' ')
i+=1

#go to dir template
Dir.chdir(v_dir_template)

#Reference Variables
cur_dt=Time.new
v_dt_seq=cur_dt.strftime('%Y%m%d')
#get sequence number
w_seq=File.read(v_seq_file).split(',')
if w_seq[0] == v_dt_seq then
	v_seq=w_seq[1]
	incr=w_seq[1].to_i+1
	File.open(v_seq_file,'w') do|fp|
		fp.print(v_dt_seq+','+incr.to_s)
	end
else
	v_seq='0'
	File.open(v_seq_file,'w') do|fp|
		fp.print(v_dt_seq+','+v_seq)
	end
end
	
v_unique_ref='JCORD'+v_dt_seq+v_seq

#Reading of template file and replacing variables
w_content=File.read(v_template)
w_content.gsub!("v_buyer", v_buyer)
w_content.gsub!("v_supplier", v_supplier)
w_content.gsub!("v_unique_ref", v_unique_ref)
w_content.gsub!("v_description", v_description)
#write to a new file
rfq_file_name='STD_ORD_'+v_unique_ref+'.XML'
File.open(rfq_file_name, 'w') do |fp|
	fp.puts(w_content)
end

#Moving of created files to FX out folder
FileUtils.mv(rfq_file_name, v_out_folder)

end