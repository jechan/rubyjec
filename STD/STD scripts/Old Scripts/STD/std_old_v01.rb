#include libraries and gems
require 'FileUtils'
require 'C:/Shipserv Backup/RUBY WORKSHOP/STD/STD scripts/mymethods.rb'

#Global Variable Declaration
v_int_type='STD'
v_ext='.XML'
=begin
1=RFQ, 2=ORD, 3=QOT, 4=POA/POC, 5=INV
=end
v_doc_type='1'

@@v_buyer='10414'
@@v_supplier='59758'

v_template=v_int_type+'_'+get_doc_type(v_doc_type)+v_ext
@@v_dir_template='C:/Shipserv Backup/RUBY WORKSHOP/STD/STD MTML TEMPLATE/'
@@v_seq_file=v_int_type+'_seq.txt'

#go to dir template and working directory
Dir.chdir(@@v_dir_template)

#Reference Variables
v_unique_ref='JC'+get_doc_seq(v_doc_type)

#Reading of template file and replacing variables
w_content=File.read(v_template)
w_content.gsub!("v_buyer", @@v_buyer)
w_content.gsub!("v_supplier", @@v_supplier)
w_content.gsub!("v_unique_ref", v_unique_ref)

#write to a new file
v_file_name=v_int_type+'_'+get_doc_type(v_doc_type)+'_'+v_unique_ref+v_ext
File.open(v_file_name, 'w') do |fp|
	fp.puts(w_content)
end

#Moving of created files to FX out folder
FileUtils.mv(v_file_name, get_out_folder(v_doc_type))
