#include libraries and gems
require 'FileUtils'
require 'C:/Shipserv Backup/RUBY WORKSHOP/STD/STD scripts/mymethods.rb'

#Global Variable Declaration
@@v_int_type='STD'
@@v_ext='.XML'
@@v_dir_template='C:/Shipserv Backup/RUBY WORKSHOP/STD/STD MTML TEMPLATE/'
Dir.chdir(@@v_dir_template)
@@v_pref='JC'

#Replaced Variables in documents later to be XML
@@v_buyer='10414'
@@v_supplier='59758'

@@v_doc_type=1
#1=RFQ, 2=ORD, 3=QOT, 4=POA/POC, 5=INV

#the test file create is dependent on doc_type
integrated_doc=Intfile.new
@@v_unique_ref=get_doc_seq(@@v_doc_type)

#Reading of template file and replacing variables
@@w_content=File.read(integrated_doc.get_template)
@@w_content.gsub!("v_buyer", @@v_buyer)
@@w_content.gsub!("v_supplier", @@v_supplier)
@@w_content.gsub!("v_unique_ref", @@v_unique_ref)

#write to a new file and move
integrated_doc.create_file
