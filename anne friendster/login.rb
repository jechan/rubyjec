# $Id: login.rb,v 1.10 2008/12/05 06:26:28 aking Exp $
# Author:: Aleli Lopez
# Last Modified by: $Author: aking $
#
# Author:: Anne Kristine King
# Last Modified by: $Author: aking $ $Date: 2008/12/05 06:26:28 $ 
require 'taf/page_handlers/default'

module AutomationFramework
  class LoginPageHandler < BasicPageHandler
	def self.can_handle?(page)
		super(page, 'login.php')
	end
	
	def initialize(page, logger = LogFacade.new)
		super(page, logger)
	  
		form :login, @page.form(:name, 'login_form')
		text_field :email, @page.text_field(:name, 'email')
		text_field :password, @page.text_field(:name, 'password')
		button :submit, login_form.links[1]
	  
		checkbox :remember_me, @page.checkbox(:name,'remembermyemail')
		link :forgot_password, @page.link(:href,/forgotpassword\.php/)
	  
		div :error_message_text, @page.div(:class,'grey-text')
	end
	
	def login_as(user)
		login(user.email, user.password)
	end
	
	def login(email, password)
		if logged_in?
			@logger.warn('Cannot log in - user session already exists')
			nil
		else
			email_text_field.set(email)
			password_text_field.set(password)
			submit_button.click
			if login_form.exists?
				@logger.warn([ "Could not log in as #{email}.", error_message_text_div.text ].join)
			end
		end
	end
  end
end
