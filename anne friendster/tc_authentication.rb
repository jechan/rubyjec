# $Id: tc_authentication.rb,v 1.34 2009/09/28 08:24:28 maustria Exp $ 
#
# Author::Yuriy Goldshtrakh
# Last Modified by: $Author: maustria $

require 'taf/smoke_test'
require 'taf/actions'
require 'net/http'
require 'watir/testcase'
require 'fit/column_fixture'

# This suite is testing the authentication functionality of friendster
# Manual Test plan can be found : http://wiki2.hq.friendster.com/eng/index.cgi?login_authentication_and_authorization
# The following test cases are included:
# Login as a valid user
# Login as invalid user
# Log Outs
# Forgot Password for valid user
# Forgot Password for invalid user
# Return to Session
# Blank Email
# Blank Password
# Blank Email and Password
# Login using invalid email format
# Login using unverified account
# Login using non existing email
# Login using uppercase of password (tester - TESTER)
# Login using lowercase of password (TESTER - tester)
# Login using camelized password (tester-Tester)

# Need to Add Support For:
# TODO:
#Remeber Me
#Inactive Users
#Browser Restart
#Login suspended user
#Login cancelled account
#Tests for field boundaries
class TestAuthentication < Watir::TestCase

# Specify the test ID for this smoke test.This value is used for locating
# the test configuration files.
	TEST_ID = 'tc_authentication'
	include AutomationFramework
	include SmokeTestCase
	include Actions

	def initialize(test_case_name)
		@config = TestConfiguration.new(TEST_ID)
		super(test_case_name, TEST_ID)
	end

	def test_login_valid_user
    p this
		@engine.login_as @config.tc_authentication_user_valid
		assert(@engine.logged_in?, "Fail, Unable to login valid user")
	end

	def test_login_invalid_user
    p this
		@engine.login_as @config.tc_authentication_user_invalid
		assert(@engine.logged_in? == false, "Fail, Invalid User logged in successfully.")
	end

	def test_logout
    p this
		@engine.login_as @config.tc_authentication_user_valid
		@engine.home
		@engine.logout
		assert(@engine.logged_in? == false, "Fail, Logout User still logged in.")
	end

	def test_forgot_password_page_verification
    p this
		assert(@engine.forgotpassword_link.exists?, 'Fail, missing forgot password link')
		assert(TafUtils.is_live?(@engine.forgotpassword_link.href), "Fail, Forgot password page not found")
		@engine.forgotpassword_link.click
		assert(@engine.forgot_password_help_link.exists?, 'FAIL - missing forgot password')
		assert(@engine.email_text_field.exists?, 'Fail, missing email text field')
		assert(@engine.get_password_button.exists?, 'Fail, missing get password button')
		assert(@engine.cancel_button.exists?, 'Fail, missing cancel button')
	end

	def test_forgot_password_valid_email
    p this
		@engine.forgotpassword_link.click
		@engine.email_text_field.set(@config.tc_authentication_user_valid.email)
		@engine.get_password_button.click
		assert(@engine.status_success_div.exists?, "Fail, retrieve valid forgot password => #{@engine.status_message_div.text}")
		assert(@engine.login_link.exists?, 'Fail, missing login link')
		assert(TafUtils.is_live?(@engine.login_link.href), "Login page not found")
	end

	def test_forgot_password_invalid_email
    p this
		invalid_emails = [
			'', # blank email
			@config.tc_authentication_user_valid.email.gsub('@','@@'),# existent, but invalid format
			@config.tc_authentication_user_invalid.email, # valid format, but non-existent
			'X' * 101 , # exceeding allowable number of input character in e-mail address
			'<?php phpinfo(); ?>',# PHP code injection
			"email@test.com' OR '1'='1",# SQL code injection
			"email@test.com; system('/bin/echo uh-oh')",# SHELL code injection
			]
    @engine.forgotpassword_link.click
		invalid_emails.each do |invalid_email|
			puts "setting the email field: #{invalid_email}"
			@engine.email_text_field.set(invalid_email)
			@engine.get_password_button.click
			assert(@engine.errorbox_div.exists?, "Fail, should show error message")
		end
	end

	def test_forgot_password_cancel
    p this
		emails = [
			'', # blank
			@config.tc_authentication_user_valid.email, # valid
			@config.tc_authentication_user_invalid.email, # invalid
			]
    @engine.forgotpassword_link.click
		emails.each do |email|
			puts "setting the email field: #{email}"
			@engine.email_text_field.set(email)
			@engine.cancel_button.click
			assert(@engine.logged_in? == false, 'Fail, user should not be logged in')
      @engine.forgotpassword_link.click
    end
	end

#~ #test forgot password for cancelled user

	def test_return_to_session
    p this
    @engine.login_as @config.tc_authentication_user_valid
		uri = URI.parse(@engine.url)
		host = uri.host
		@engine.goto('http://www.google.com')
		@engine.goto("#{host}")
		assert @engine.logged_in?
	end

	def test_blank_email
    p this
		@engine.login("", @config.tc_authentication_user_valid.password)
		assert(@engine.email_text_field.exists?, "Fail, successful login with blank email.")
	end

	def test_blank_password
    p this
		@engine.login(@config.tc_authentication_user_valid.email, "")
		assert(@engine.email_text_field.exists?, "Fail, successful login with blank password.")
	end

	def test_blank_email_and_password
    p this
		@engine.login("", "")
		assert(@engine.email_text_field.exists?, "Fail, successful login with blank email and password.")
	end

	def test_invalid_email_format
    p this
		@engine.login(Time.new.to_i.to_s, "password")
		assert(@engine.email_text_field.exists?, "Fail, successful login with invalid email format.")
	end

	def test_non_existing_valid_email
    p this
		@engine.login("mia.austria@gmail.com", "tester")
		assert(@engine.email_text_field.exists?, "Fail, successful login with non existing email.")
	end

	def test_uppercase_password
    p this
		@engine.login(@config.tc_authentication_user_valid.email, @config.tc_authentication_user_valid.password.upcase)
		assert(@engine.email_text_field.exists?, "Fail, successful login using password in uppercase.")
	end

	def test_lowercase_password
    p this
		@engine.login(@config.tc_allcaps_password.email, @config.tc_allcaps_password.password.downcase)
		assert(@engine.email_text_field.exists?, "Fail, successful login using password in lowercase.")
	end

	def test_camelize_password
    p this
		@engine.login(@config.tc_authentication_user_valid.email, @config.tc_authentication_user_valid.password.camelize)
		assert(@engine.email_text_field.exists?, "Fail, successful login using password in camel form.")
	end
end